'use strict';

angular.module('core').factory('funcService',
    function() {

        // clone object
        function objectClone (sObj) {
            if (typeof sObj !== "object") {
                return sObj;
            }

            var s = {};
            if (sObj.constructor == Array) {
                s = [];
            }

            for (var i in sObj) {
                s[i] = objectClone(sObj[i]);
            }

            return s;
        }

        // get coordinate vector
        function getCoordinateVector(node) {
            if (node.hasOwnProperty('X') && node.hasOwnProperty('Y')) {
                return [node.X, node.Y];
            }
            else if (node.hasOwnProperty('x') && node.hasOwnProperty('y')) {
                return [node.x, node.y];
            }
            else if (node instanceof Array && node instanceof Array) {
                return node;
            }

            return node;
        }

        // get euclidean distance
        function getDistance(p, q) {
            var dis = 0;
            p = getCoordinateVector(p);
            q = getCoordinateVector(q);

            if (p instanceof Array && q instanceof Array) {
                dis = Math.sqrt(Math.pow(p[0] - q[0], 2) + Math.pow(p[1] - q[1], 2));
            }

            return dis;
        }

        function getDirectionRadius(source, dest) {
            source = getCoordinateVector(source);
            dest = getCoordinateVector(dest);

            var thita = Math.atan2(source[1]-dest[1], source[0]-dest[0]);

            return thita;
        }

        // swap two variables
        function swap(a, b) {
            var c = a;
            a = b;
            b = c;
        }

        // generate polygon path
        function polygon(d) {
            if (d.length == 0) {
                return "";
            }
            return 'M' + d.join('L') + 'Z';
        }

        // generate line path
        function line(d) {
            return 'M' + d.join('L');
        }

        // get center point
        function getCenterPoint(p1, p2) {
            return [(p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2];
        }

        // is include in the range
        function isInRangeInclude(value, min, max) {
            return (value >= min) && (value <= max);
        }

        // calculate area of polygon
        function calculateArea(polygon) {
            var area = 0;
            var n = polygon.length;

            for (var i = 0; i < polygon.length; ++i) {
                var i2 = i;
                var i3 = i + 1;
                if (i3 >= n) {
                    i3 = i3 - n;
                }
                var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
                var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

                var _area = p2[0] * p3[1] - p3[0] * p2[1];
                _area = _area * 0.5;

                area = area + _area;
            }

            return area;
        }

        // is polygon concave
        function isConcave(polygon) {

            var n = polygon.length;

            var flag = false;

            for (var i = 0; i < n; ++i) {
                var i1 = i - 1;
                if (i1 < 0) {
                    i1 = i1 + n;
                }
                var i2 = i;
                var i3 = i + 1;
                if (i3 >= n) {
                    i3 = i3 - n;
                }
                var p1 = [polygon[i1][0], -1 * polygon[i1][1]];
                var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
                var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

                var result = crossMulti(p1, p2, p3);
                if (result < 0) {
                    flag = true;
                }
            }

            return flag;
        }

        // cross multiply
        function crossMulti(p1, p2, p3) {
            return (p1[0]*p2[1]+p2[0]*p3[1]+p3[0]*p1[1]) - (p1[0]*p3[1]+p2[0]*p1[1]+p3[0]*p2[1]);
        }

        // get number of properties in one object
        function getPropertyNum(obj) {
            var count = 0;
            for(var i in obj) {
                ++count;
            }

            return count;
        }

        return {
            objectClone: objectClone,
            getCoordinateVector: getCoordinateVector,
            getDistance: getDistance,
            getDirectionRadius: getDirectionRadius,
            swap: swap,
            polygon: polygon,
            line: line,
            getCenterPoint: getCenterPoint,
            isInRangeInclude: isInRangeInclude,
            calculateArea: calculateArea,
            isConcave: isConcave,
            crossMulti: crossMulti,
            getPropertyNum: getPropertyNum
        }
    }
);
