'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = mongoose.Schema.Types.Mixed;

// matrix data schema
var matrixDataSchema = new Schema({
    date: String,
    max_n: Number,
    max_m: Number,
    matrix: Mixed
});

// matrix hours data schema
var matrixHourDataSchema = new Schema({
    date: String,
    m_n: String,
    hour: Number,
    value: Mixed
});

mongoose.model('matrixData', matrixDataSchema);
mongoose.model('matrixHourData', matrixHourDataSchema);