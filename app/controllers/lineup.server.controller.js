'use strict';

var mongoose = require('mongoose');
var _ = require('lodash');
var func = require('./func.server.controller.js');

exports.getLineUpDataFiltered = getLineUpDataFiltered;
exports.getLineUpDataFilteredWithCallback = getLineUpDataFilteredWithCallback;

// get filtered lineup data depends on selected source and destination nodes
function getLineUpDataFiltered(req, res) {
    var paras = req.body;

    var dateList = paras.dateList;
    var m_n = paras.m_n;
    var selectedSource = paras.selectedSource;
    var selectedDest = paras.selectedDest;

    getLineUpDataFilteredWithCallback(dateList, m_n, selectedSource, selectedDest, true, function(filteredData) {
        res.send(filteredData);
        res.end();
    });
}

function getLineUpDataFilteredWithCallback(dateList, m_n, selectedSource, selectedDest, isRemoveRepetition, callback) {
    var sourceSize = selectedSource.length;
    var destSize = selectedDest.length;

    selectedSource = _.indexBy(selectedSource, function(obj) {
        return obj;
    });

    selectedDest = _.indexBy(selectedDest, function(obj) {
        return obj;
    });

    var mongoParas = generateMongoParas();

    func.getDataListMongo(mongoParas, function(dataList) {
        var filteredData = [];
        for(var i = 0; i < dataList.length; ++i) {
            var _filteredData = dataList[i].filter(function(v) {
                var mList = v.value.m;
                var nList = v.value.n;

                var sourceCount = 0;
                if(sourceSize > 0) {
                    for (var j in mList) {
                        if (selectedSource.hasOwnProperty(mList[j].id)) {
                            ++sourceCount;
                        }
                    }
                }

                var destCount = 0;
                if(destSize > 0) {
                    for (var j in nList) {
                        if (selectedDest.hasOwnProperty(nList[j].id)) {
                            ++destCount;
                        }
                    }
                }

                if(sourceCount == sourceSize && destCount == destSize) {
                    return true;
                }

                return false;
            });

            filteredData = filteredData.concat(_filteredData);
        }

        if(isRemoveRepetition) {
            filteredData = removeRepetition(filteredData);
        }

        // sorting depends on persons_sum
        filteredData.sort(function(va, vb) {
            return vb.value.info.persons_sum - va.value.info.persons_sum;
        });

        callback(filteredData);
    });

    // generate Mongo parameters
    function generateMongoParas() {
        var modelNames = [];
        var collectionNames = [];
        var findParas = [];

        for(var i = 0; i < dateList.length; ++i) {
            modelNames.push('lineUpData');
            collectionNames.push('lineup');
            findParas.push({
                date: dateList[i],
                m_n: m_n
            });
        }

        return {
            modelNames: modelNames,
            collectionNames: collectionNames,
            findParas: findParas
        };
    }

    // remove repetition combinations of different dates
    function removeRepetition(_data) {
        for(var i = 0; i < _data.length; ++i) {
            var value = _data[i].value;

            value.m.sort(sortByID);
            value.n.sort(sortByID);
        }

        var data = [];
        var dataMap = {};

        for(var i = 0; i < _data.length; ++i) {
            var value = _data[i].value;
            var name = getName(value);

            if(dataMap.hasOwnProperty(name)) {
                combineLinUpRecords(dataMap[name], _data[i]);
            } else {
                data.push(_data[i]);
                dataMap[name] = _data[i];
            }
        }

        for(var i = 0; i < data.length; ++i) {
            var value = data[i].value;

            value.m.sort(sortByDayPersons);
            value.n.sort(sortByDayPersons);
        }

        return data;

        function getName(value) {
            var mIDList = value.m.map(function(d) {
                return d.id;
            });
            var nIDList = value.n.map(function(d) {
                return d.id;
            });

            return mIDList.join(',') + '_' + nIDList.join(',');
        }

        function sortByID(va, vb) {
            return va.id > vb.id;
        }

        function sortByDayPersons(va, vb) {
            return vb.day_persons - va.day_persons;
        }

        function combineLinUpRecords(ra, rb) {
            var valueA = ra.value;
            var valueB = rb.value;

            valueA.info.persons_sum += valueB.info.persons_sum;
            valueA.info.time_slice_sum += valueB.info.time_slice_sum;

            for(var i = 0; i < valueA.m; ++i) {
                combineTwoItemsInMNList(valueA.m[i], valueB.m[i]);
            }

            for(var i = 0; i < valueA.n; ++i) {
                combineTwoItemsInMNList(valueA.n[i], valueB.n[i]);
            }

            function combineTwoItemsInMNList(itemA, itemB) {
                itemA.day_persons += itemB.day_persons;

                var timeSliceA = itemA.time_slice_persons;
                var timeSliceB = itemB.time_slice_persons;
                for(var j in timeSliceB) {
                    if(!timeSliceA.hasOwnProperty(j)) {
                        timeSliceA[j] = timeSliceB[j];
                    } else {
                        timeSliceA[j] += timeSliceB[j];
                    }
                }
            }
        }
    }
}