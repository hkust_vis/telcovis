'use strict';

angular.module('flowMap').directive('flowMapDirective', ["flowMapService", 'dataService', 'funcService', 'pipService', 'mapService', function(flowMapService, dataService, funcService, pipService, mapService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {

            setFlowMap();

            function setFlowMap() {
                setHourRangeSlider();

                pipService.onDateListChange(scope, function() {
                    flowMapService.clearMainGraph();
                });

                pipService.onHoursChange(scope, function(hours) {
                    $("#flow_map_slider-range").slider({
                        values: [hours[0], hours[hours.length-1]]
                    });

                    if(flowMapService.setHours) {
                        flowMapService.setHours(hours);
                    }
                });

                // init hour range slider
                function setHourRangeSlider() {
                    $(function () {
                        $("#flow_map_slider-range").slider({
                            range: true,
                            min: 0,
                            max: 23,
                            values: [ 0, 23 ],
                            step: 1,
                            slide: function (event, ui) {
                                var start = ui.values[ 0 ];
                                var end  = ui.values[ 1 ];

                                $("#source_hour_range").val(start + " - " + end);

                                var hours = d3.range(start, end+1, 1);
                                dataService.setHours(hours);
                            }
                        });
                        $("#flow_map_hour_range").val($("#flow_map_slider-range").slider("values", 0) +
                            " - " + $("#flow_map_slider-range").slider("values", 1));
                    });
                }

                var clusterNum = 3;
                flowMapService.data.clusterNum = clusterNum;
                var nodeNumOfCluster = 5;
                flowMapService.data.nodeNumOfCluster = nodeNumOfCluster;

                flowMapService.data.timeFilterNodes = [undefined, undefined, undefined];

                flowMapService.data.flowColors = [
                    "Green",
                    "Crimson"
                ];


                flowMapService.data.clusterColors = [
                    'rgb(107,174,214)',
                    'rgb(254,153,41)',
                    'rgb(158,154,200)',

                    "#2171b5",
                    "#fe9929",
                    "#6a51a3"
                ];
                flowMapService.data.clusterColorsSequential = [
                    ['rgb(239,243,255)','rgb(189,215,231)','rgb(107,174,214)','rgb(33,113,181)'],
                    ['rgb(255,255,212)','rgb(254,217,142)','rgb(254,153,41)','rgb(204,76,2)'],
                    ['rgb(242,240,247)','rgb(203,201,226)','rgb(158,154,200)','rgb(106,81,163)']
                ];

                var width = 800,
                    height = 900;

                var svg = d3.select("#flowmap_graph").append("svg")
                    .attr("width", width)
                    .attr("height", height);

                flowMapService.data.pointA = {
                    pos: {X: width / 3, Y: 30}
                };
                flowMapService.data.pointB = {
                    pos: {X: 2 * width / 3, Y: 30}
                };
                flowMapService.data.sourcePoints = [
                    flowMapService.data.pointA,
                    flowMapService.data.pointB
                ];

                // render left source point
                var pointA = svg
                    .append("circle")
                    .attr("class", "point_A")
                    .attr("cx", function(d) {
                        return flowMapService.data.pointA.pos.X;
                    })
                    .attr("cy", function(d) {
                        return flowMapService.data.pointA.pos.Y;
                    })
                    .attr("r", 20)
                    .attr("stroke", "grey")
                    .attr("stroke-width", 10)
                    .attr("fill", "none");

                svg
                    .append("circle")
                    .attr("class", "point_A")
                    .attr("cx", function(d) {
                        return flowMapService.data.pointA.pos.X;
                    })
                    .attr("cy", function(d) {
                        return flowMapService.data.pointA.pos.Y;
                    })
                    .attr("r", 5)
                    .attr("stroke", "none")
                    .attr("fill", flowMapService.data.flowColors[0])
                    .on("mouseover", function(d) {
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .attr("stroke-width", function () {
                                return 0.1;
                            })
                            .attr("stroke-opacity", function() {
                                return 0.1;
                            });
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .filter(function(_d) {
                                if(_d.source == 0) {
                                    return true;
                                }

                                return false;
                            })
                            .attr("stroke-width", function (d) {
                                return d.width*2;
                            })
                            .attr("stroke-opacity", function(d) {
                                return 1;
                            });

                    })
                    .on("mouseout", function() {
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .attr("stroke-width", function (_d) {
                                return _d.width;
                            })
                            .attr("stroke-opacity", function(_d) {
                                return 1;
                            });
                    });

                // render right source point
                var pointB = svg
                    .append("circle")
                    .attr("class", "point_B")
                    .attr("cx", function(d) {
                        return flowMapService.data.pointB.pos.X;
                    })
                    .attr("cy", function(d) {
                        return flowMapService.data.pointB.pos.Y;
                    })
                    .attr("r", 20)
                    .attr("stroke", "grey")
                    .attr("stroke-width", 10)
                    .attr("fill", "none");

                svg
                    .append("circle")
                    .attr("class", "point_B")
                    .attr("cx", function(d) {
                        return flowMapService.data.pointB.pos.X;
                    })
                    .attr("cy", function(d) {
                        return flowMapService.data.pointB.pos.Y;
                    })
                    .attr("r", 5)
                    .attr("stroke", "none")
                    .attr("fill", flowMapService.data.flowColors[1]).on("mouseover", function(d) {
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .attr("stroke-width", function () {
                                return 0.1;
                            })
                            .attr("stroke-opacity", function() {
                                return 0.1;
                            });
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .filter(function(_d) {
                                if(_d.source == 1) {
                                    return true;
                                }

                                return false;
                            })
                            .attr("stroke-width", function (d) {
                                return d.width*2;
                            })
                            .attr("stroke-opacity", function(d) {
                                return 1;
                            });

                    })
                    .on("mouseout", function() {
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .attr("stroke-width", function (_d) {
                                return _d.width;
                            })
                            .attr("stroke-opacity", function(_d) {
                                return 1;
                            });
                    });

                var mainGraph = svg
                    .append("g")
                    .attr("class", "main_graph");

                var clusters;

                var POI = [
                    "Shopping Service",
                    "Travel accommodation",
                    "Gourmet"
                ];

                var clusterLngLats = [
                    {
                        longitude: -1,
                        latitude: -1
                    },
                    {
                        longitude: -1,
                        latitude: -1
                    },
                    {
                        longitude: -1,
                        latitude: -1
                    }
                ];

                var voronoiIteration = 100;

                var clusterParamenters = {
                    POI: POI,
                    lngLats: clusterLngLats,
                    timeFilterNodes: flowMapService.data.timeFilterNodes
                };

                var allNodes;

                flowMapService.changeControlParameters = function() {
                    for(var i = 0 ; i < 3; ++i) {
                        var poiDom = $("#VoronoiRegion_poi_" + i)[0];
                        if(poiDom.value != '-1' && poiDom.value != '') {
                            POI[i] = poiDom.value;
                        }
                    }

                    var voronoiIterationDom = $("#VoronoiRegion_iteration")[0];
                    voronoiIteration = parseInt(voronoiIterationDom.value);

                    updateClusters();
                    flowMapService.drawMainGraph();
                };

                var flowmapStyle = true;
                $("#flow_map_style").bind("click", setFlowmapStyle);
                function setFlowmapStyle() {
                    flowmapStyle = !flowmapStyle;
                    flowMapService.drawMainGraph();
                }

                flowMapService.updateClusters = updateClusters;
                function updateClusters() {
                    clusters = generateClusters(allNodes, clusterParamenters);
                    flowMapService.data.clusters = clusters;
                }

                pipService.onFlowMapStayDataChange(scope, function (stayData) {
                    receivedData();
                });

                flowMapService.clearMainGraph = clearMainGraph;
                function clearMainGraph() {
                    $(".main_graph").empty();
                }

                // when receiving data
                function receivedData() {
                    var sourceMapData = mapService.data.map.source;
                    var poiSet = generatePoiSet(sourceMapData);

                    flowMapService.data.pointA.nid = flowMapService.data.sources[0];
                    flowMapService.data.pointA.gps = sourceMapData[flowMapService.data.sources[0]].gps;
                    flowMapService.data.pointA.nid = flowMapService.data.sources[1];
                    flowMapService.data.pointB.gps = sourceMapData[flowMapService.data.sources[1]].gps;

                    var stayData = flowMapService.data.stayData;
                    var contacts = generateContacts(stayData);

                    allNodes = constructAllNodes(sourceMapData, contacts);
                    flowMapService.data.allNodes = allNodes;

                    var poiContacts = getContactsOfPOI(allNodes, poiSet);

                    updatePOINameList(poiContacts);
                    updateClusters();

                    var flowData = [
                        {},     // point A
                        {}      // point B
                    ]; // flowData[point][timeSlot][nid]


                    flowMapService.drawMainGraph = drawMainGraph;
                    drawMainGraph();

                    function drawMainGraph() {
                        clearMainGraph();

                        var _gap = 30;
                        var _step = (width - 2 * _gap) / 3;
                        var _size = [_step, 600 / 4];

                        var nodesPos = getNodesPosOfClusters(clusters, _size);

                        var nodes = [];
                        for (var i = 0; i < clusterNum; ++i) {
                            nodes.push([]);
                            var nodesNum = Math.min(clusters[i].length, nodeNumOfCluster);
                            for (var j = 0; j < nodesNum; ++j) {
                                var node = clusters[i][j];

                                node.cid = i;
                                node.X = nodesPos[i][j][0];
                                node.Y = nodesPos[i][j][1];
                                node.W = node.frequency;

                                nodes[i].push(node);
                            }
                        }

                        var weights = getWeightsOfClusters(nodes);

                        for (var i = 0; i < stayData.length; ++i) {
                            var slices = stayData[i].value;

                            for (var j = 0; j < 24; ++j) {
                                if (slices.hasOwnProperty(String(j))) {
                                    flowData[i][j] = {};

                                    var stays = slices[String(j)];
                                    for (var k in stays) {
                                        flowData[i][j][k] = stays[k];
                                    }
                                }
                            }
                        }

                        var ratioBar = RatioBarView("RatioBarView", nodes, weights, flowData, [0, 100], width, mainGraph, flowMapService);

                        flowMapService.data.timeBarPos = [];
                        flowMapService.data.timeBar = [];

                        var voronoiView = addVoronoiView(voronoiIteration);

                        if (flowmapStyle) {
                            var flowMap = FlowMap(flowData, nodes, width, height, 5, mainGraph, flowMapService);
                            for (var i = 0; i < clusterNum; ++i) {
                                flowMapService.data.timeBar[i].drawTimeBar();
                            }
                            var hours = dataService.getHours();
                            flowMapService.setHours(hours);
                        } else {
                            var normalFlowMap = NormalFlowMap(flowData, nodes, width, height, 5, mainGraph, flowMapService, funcService);
                        }

                        var pointsView = PointsView(nodes, mainGraph, flowMapService, mapService);

                        function addVoronoiView(voronoiIteration) {
                            var _voronoiView = mainGraph.append("g");
                            var voronoiView_1 = VoronoiRegionView(0, nodes[0], weights[0], "voronoi_1", [0, 200], _size, voronoiIteration, _voronoiView, mainGraph, flowMapService, funcService);
                            var voronoiView_2 = VoronoiRegionView(1, nodes[1], weights[1], "voronoi_2", [_step + _gap, 200], _size, voronoiIteration, _voronoiView, mainGraph, flowMapService, funcService);
                            var voronoiView_3 = VoronoiRegionView(2, nodes[2], weights[2], "voronoi_3", [(_step + _gap) * 2, 200], _size, voronoiIteration, _voronoiView, mainGraph, flowMapService, funcService);

                            return _voronoiView;
                        }
                    }

                    function generatePoiSet(sourceMapData) {
                        var poiSet = d3.set();
                        for(var i in sourceMapData) {
                            var poi = sourceMapData[i].poi;
                            poiSet.add(poi);
                        }

                        return poiSet;
                    }

                    function updatePOINameList(poiContacts) {
                        for(var i = 0 ; i < 3; ++i) {
                            d3.select("#cluster_" + i + "_dropdownMenu_button").select(".cluster_name")
                                .text(POI[i]);

                            var poiList = [];

                            poiSet.forEach(function(value) {
                                if(value != POI[0] && value != POI[1] && value != POI[2]) {
                                    poiList.push(value);
                                }
                            });
                            if(poiContacts) {
                                poiList.sort(function(a, b) {
                                    return poiContacts[b] - poiContacts[a];
                                });
                            }

                            $("#cluster_" + i + "_dropdown-menu").empty();
                            var listDOM = d3.select("#cluster_" + i + "_dropdown-menu");
                            for(var j = 0; j < poiList.length; ++j) {
                                listDOM.append("li")
                                    .attr("role", "presentation")
                                    .append("a")
                                    .datum({
                                        cid: i,
                                        name: poiList[j]
                                    })
                                    .attr("role", "menuitem")
                                    .attr("tabindex", j)
                                    .text(poiList[j])
                                    .on("click", function(d) {
                                        POI[d.cid] = d.name;
                                        updatePOINameList();
                                        updateClusters();
                                        flowMapService.drawMainGraph();
                                    });
                            }
                        }
                    }

                    function generateContacts(stayData) {
                        var contacts = {};
                        for(var hour in stayData[0].value) {
                            contacts[hour] = {};
                            var destsA = stayData[0].value[hour];
                            for(var destID in destsA) {
                                var destsB = stayData[1].value[hour];
                                if(destsB.hasOwnProperty(destID)) {
                                    contacts[hour][destID] = destsA[destID] + destsB[destID];
                                }
                            }
                        }

                        return contacts;
                    }

                    function getContactsOfPOI(allNodes, poiSet) {
                        var poiContacts = {};
                        var pois = poiSet.values();
                        for (var i in pois) {
                            poiContacts[pois[i]] = 0;
                        }

                        for (var i in allNodes) {
                            var node = allNodes[i];

                            var poi = node.poi;
                            if (poiContacts.hasOwnProperty(poi)) {
                                poiContacts[poi] += node.frequency;
                            }
                        }

                        return poiContacts;
                    }

                    function getTopNodes(allNodes) {

                        var nodes = [];
                        for (var i in allNodes) {
                            var node = allNodes[i];
                            if (node.frequency == 0) {
                                continue;
                            }

                            nodes.push(node);
                        }

                        nodes.sort(function (a, b) {
                            return b["frequency"] - a["frequency"];
                        });

                        return nodes;
                    }

                    function generateTopNodesOfPOIs(allNodes, pois) {
                        var nodes = [];
                        var poiMap = d3.map();
                        for (var i = 0; i < pois.length; ++i) {
                            nodes.push([]);
                            poiMap.set(pois[i], i);
                        }

                        for (var i in allNodes) {
                            var node = allNodes[i];
                            if (node.frequency == 0) {
                                continue;
                            }

                            for (var j = 0; j < node["poi"].length; ++j) {

                                var nPOI = node["poi"][j];
                                if (!poiMap.has(nPOI)) {
                                    continue;
                                }

                                var POIindex = poiMap.get(nPOI);

                                nodes[POIindex].push(node);
                            }
                        }

                        for (var i = 0; i < pois.length; ++i) {
                            nodes[i].sort(function (a, b) {
                                return b["frequency"] - a["frequency"];
                            });
                        }

                        return nodes;
                    }

                    function getNodesPosOfClusters(clusters, _size) {
                        var minX = 1e6;
                        var maxX = 0;
                        var minY = 1e6;
                        var maxY = 0;

                        var nodesPos = [];
                        for (var i = 0; i < clusterNum; ++i) {
                            nodesPos.push([]);
                            var nodesNum = clusters[i].length;
                            for (var j = 0; j < nodesNum; ++j) {
                                var node = clusters[i][j];

                                var pos = node.gps;
                                var _pos = [pos.lng, pos.lat];

                                if (_pos[0] < minX) {
                                    minX = _pos[0];
                                }
                                if (_pos[0] > maxX) {
                                    maxX = _pos[0];
                                }
                                if (_pos[1] < minY) {
                                    minY = _pos[1];
                                }
                                if (_pos[1] > maxY) {
                                    maxY = _pos[1];
                                }

                                nodesPos[i].push(_pos);
                            }
                        }


                        var x_scale = d3.scale.linear()
                            .domain([minX, maxX])
                            .range([0, _size[0]]);

                        var y_scale = d3.scale.linear()
                            .domain([minY, maxY])
                            .range([0, _size[1]]);

                        for (var i = 0; i < clusterNum; ++i) {
                            var nodesNum = nodesPos[i].length;
                            for (var j = 0; j < nodesNum; ++j) {
                                nodesPos[i][j][0] = x_scale(nodesPos[i][j][0]);
                                nodesPos[i][j][1] = y_scale(nodesPos[i][j][1]);
                            }
                        }
                        return nodesPos;
                    }

                    function getWeightsOfClusters(clusters) {
                        var weights = [];

                        for (var i = 0; i < clusterNum; ++i) {
                            weights.push([]);
                            var nodesNum = clusters[i].length;
                            for (var j = 0; j < nodesNum; ++j) {
                                var node = clusters[i][j];
                                var nid = node.nid;
                                var weight = node.frequency;
                                weights[i].push(weight);
                            }
                        }

                        return weights;
                    }

                    function constructAllNodes(sourceMapData, contacts) {
                        var nodes = {};

                        for (var i in sourceMapData) {
                            nodes[i] = {};
                            nodes[i].nid = i;
                            nodes[i].gps = sourceMapData[i].gps;
                            nodes[i].poi = sourceMapData[i].poi;
                        }

                        var nodesContactsMap = d3.map();
                        for (var i = 0; i < 24; ++i) {
                            if (!contacts.hasOwnProperty(i)) {
                                continue;
                            }
                            var _contacts = contacts[i];

                            for (var j in _contacts) {
                                var nid = j;
                                var freq = _contacts[j];
                                if (nodesContactsMap.has(nid)) {
                                    freq = freq + nodesContactsMap.get(nid);
                                }
                                nodesContactsMap.set(nid, freq);
                            }
                        }


                        var gpsAVector = [flowMapService.data.pointA.gps.lng, flowMapService.data.pointA.gps.lat];
                        var gpsBVector = [flowMapService.data.pointB.gps.lng, flowMapService.data.pointB.gps.lat];
                        var kmPerLngLat = 111;
                        for (var i in nodes) {
                            var node = nodes[i];

                            node.frequency = 0;

                            if (nodesContactsMap.has(i)) {
                                var contact_frequency = nodesContactsMap.get(i);
                                node.frequency = contact_frequency;
                            }

                            node.time_slice = getTimeSliceVector(i, contacts);

                            var nodeGpsVector = [node.gps.lng, node.gps.lat];

                            node.distance_A = funcService.getDistance(gpsAVector, nodeGpsVector)*kmPerLngLat;
                            node.distance_B = funcService.getDistance(gpsBVector, nodeGpsVector)*kmPerLngLat;
                            node.distance_M = 0.5*(node.distance_A + node.distance_B);

                            node.direction_A = funcService.getDirectionRadius(gpsAVector, nodeGpsVector)/Math.PI*180;
                            node.direction_B = funcService.getDirectionRadius(gpsBVector, nodeGpsVector)/Math.PI*180;
                            node.direction_M = 0.5*(node.direction_A + node.direction_B);
                        }

                        return nodes;
                    }

                    function getTimeSliceVector(nodeID, contacts) {
                        var vector = new Array(24);
                        for(var hour in vector) {
                            vector[hour] = 0;
                        }

                        for(var hour in contacts) {
                            var _contacts = contacts[hour];
                            if(_contacts.hasOwnProperty(nodeID)) {
                                vector[hour] = 1;
                            }
                        }

                        return vector;
                    }
                }

                function generateClusters(allNodes, clusterParamenters) {
                    var nodes = [];
                    var poiMap = d3.map();
                    for(var i = 0; i < clusterNum; ++i) {
                        nodes.push([]);
                        poiMap.set(clusterParamenters.POI[i], i);
                    }

                    // calculate clusters
                    for(var i in allNodes) {
                        var node = allNodes[i];

                        // filter outlier node
                        if(node.frequency == 0 || node.frequency > 20000) {
                            continue ;
                        }

                        var nPOI = node.poi;
                        if (poiMap.has(nPOI)) {
                            var POIindex = poiMap.get(nPOI);
                            nodes[POIindex].push(node);
                        }
                    }

                    for(var i = 0; i < clusterNum; ++i) {
                        var timeFilterNode = clusterParamenters.timeFilterNodes[i];

                        nodes[i].sort(function (a, b) {
                            if(!timeFilterNode) {
                                return b["frequency"] - a["frequency"];
                            } else {
                                return getTimeSliceSimilarity(timeFilterNode, b) - getTimeSliceSimilarity(timeFilterNode, a);
                            }
                        });
                    }

                    return nodes;
                }

                function getTimeSliceSimilarity(np, nq){
                    var p = np.time_slice;
                    var q = nq.time_slice;
                    var sum = 0;
                    for(var i = 0; i < p.length; ++i){
                        sum += p[i]^q[i];
                    }

                    return (p.length - sum)/p.length;
                }
            }


        }
    }
}]);