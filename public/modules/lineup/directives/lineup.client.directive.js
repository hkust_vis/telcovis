'use strict';

angular.module('lineUp').directive('lineUpDirective', ["lineUpService", 'pipService', 'mapService', function(lineUpService, pipService, mapService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {

            pipService.onLineUpDataChange(scope, function () {

            });

            LineUpStart(LineUp);
            lineUpService.lineUp.setData([]);


            var isLegend = true;
            lineUpService.switchIsLegend = switchIsLegend;

            function switchIsLegend() {
                isLegend = !isLegend;
                d3
                    .select("#legend_lineup")
                    .style("display", isLegend ? "block" : "none");
            }

            function LineUpStart(LineUp) {
                var menuActions = [
                    {name: " new combined", icon: "fa-plus", action: function () {
                        lineup.addNewStackedColumnDialog();
                    }},
                    {name: " add single columns", icon: "fa-plus", action: function () {
                        lineup.addNewSingleColumnDialog();
                    }}
                ];
                var lineUpDemoConfig = {
                    svgLayout: {
                        mode: 'separate',
                        plusSigns: {
                            addStackedColumn: {
                                title: "add stacked column",
                                action: "addNewEmptyStackedColumn",
                                x: 0, y: 2,
                                w: 21, h: 21 // LineUpGlobal.htmlLayout.headerHeight/2-4
                            }
                        }
                    }
                };

                var lineup = null;

                $(window).resize(function() {
                    if (lineup) {
                        lineup.updateBody();
                    }
                });
                function updateMenu() {
                    var config = lineup.config;
                    var kv = d3.entries(lineup.config.renderingOptions);
                    var kvNodes = d3.select("#lugui-menu-rendering").selectAll("span").data(kv, function (d) {
                        return d.key;
                    });
                    kvNodes.exit().remove();
                    kvNodes.enter().append("span").on('click', function (d) {
                        lineup.changeRenderingOption(d.key, !config.renderingOptions[d.key]);
                        updateMenu();
                    });
                    kvNodes.html(function (d) {
                        return '<a href="#"> <i class="fa ' + (d.value ? 'fa-check-square-o' : 'fa-square-o') + '" ></i> ' + d.key + '</a>&nbsp;&nbsp;'
                    });

                    d3.select("#lugui-menu-actions").selectAll("span").data(menuActions)
                        .enter().append("span").html(
                        function (d) {
                            return '<i class="fa ' + (d.icon) + '" ></i>' + d.name + '&nbsp;&nbsp;'
                        }
                    ).on("click", function (d) {
                            d.action.call(d);
                        });

                    d3.select("#lugui-menu-rendering").selectAll("span").remove();
                    d3.select("#lugui-menu-actions").selectAll("span").remove();
                    d3.select(".lu.lu-header").selectAll(".header.emptyHeader").remove();
                    var header = d3.select(".lu.lu-header").selectAll(".header");
                    header.selectAll(".headerSort").remove();
                    header.selectAll(".fontawe.stackedColumnInfo").remove();
                    header.selectAll(".fontawe.singleColumnDelete").remove();

                    header.each(function(){

                        var header = d3.select(this);
                        if(header.select("text")[0][0].innerHTML == "Rank"){
                            header.select("text").remove();
                            header.select("rect.labelBG").remove();
                        }

                    });



                    $(".addColumnButton").empty();
                }


                function loadDataImpl(name, desc, _data) {
                    var spec = {};
                    spec.name = name;
                    spec.dataspec = desc;
                    delete spec.dataspec.file;
                    delete spec.dataspec.separator;
                    spec.dataspec.data = _data;
                    spec.storage = LineUp.createLocalStorage(_data, desc.columns, desc.layout, desc.primaryKey);

                    if (lineup) {
                        lineup.changeDataStorage(spec);
                    } else {
                        lineup = LineUp.create(spec, d3.select('#lugui-wrapper'), lineUpDemoConfig);
                    }
                    updateMenu();

                    var lulubody = d3.select(".lu.lu-body");
                    var rows = lulubody.selectAll(".row").attr("rowId",function(d,i){return "row"+i;});

                    var rowHeight = lineup.config.svgLayout.rowHeight;
                    var columnWidth = mnColumnWidth;
                    var headerHeight = 210;
                    var offsetLeft = parseFloat(d3.select("#lugui-wrapper")[0][0].offsetLeft) - 118 + 6 + 0.35*columnWidth;//+ 20 - 0.5*columnWidth; // 118 is 1/2 of width of horizon charts
                    var offsetTop = parseFloat(d3.select("#lugui-wrapper")[0][0].offsetTop) - 15 - 0.5*columnWidth; // 118 is 1/2 of width of horizon charts

                    var horizonCharHeight = 250;
                    var horizonCharts = d3.select("#horizon_charts");

                    var addColOffset = parseFloat(d3.select(".addColumnButton")[0][0].attributes["transform"].value.split(",")[0].split("(")[1]);

                    var items = rows.selectAll(".tableData.stacked").selectAll("rect")
                        .on("mouseover", function(d) {
                            var result = getNodeInfo(d);
                            var nodeData = result.nodeData;
                            var m_n = result.m_n;
                            if(!nodeData) {
                                return ;
                            }

                            if(m_n == 'm') {
                                mapService.map.source.setView([nodeData.gps.lat, nodeData.gps.lng]);
                            } else {
                                mapService.map.dest.setView([nodeData.gps.lat, nodeData.gps.lng]);
                            }
                        })
                        .on("click", function(d) {
                            var result = getNodeInfo(d);
                            var nodeData = result.nodeData;
                            var m_n = result.m_n;
                            var nid = result.nid;
                            if(!nodeData) {
                                return ;
                            }

                            if(m_n == 'm') {
                                if(!orangeICONS.sourceICONS.hasOwnProperty(nid)) {
                                    var _marker = mapService.map.source.addPurpleMarker(nodeData.gps.lat, nodeData.gps.lng);
                                    orangeICONS.sourceICONS[nid] = _marker;

                                    d3.select(this).attr("height", 8);
                                } else {
                                    var _marker = orangeICONS.sourceICONS[nid];
                                    mapService.map.source.removeLayer(_marker);

                                    d3.select(this).attr("height", 16);

                                    delete orangeICONS.sourceICONS[nid];
                                }
                            } else {
                                if(!orangeICONS.destICONS.hasOwnProperty(nid)) {
                                    var _marker = mapService.map.dest.addYellowMarker(nodeData.gps.lat, nodeData.gps.lng);
                                    orangeICONS.destICONS[nid] = _marker;

                                    d3.select(this).attr("height", 8);
                                } else {
                                    var _marker = orangeICONS.destICONS[nid];
                                    mapService.map.dest.removeLayer(_marker);

                                    d3.select(this).attr("height", 16);

                                    delete orangeICONS.destICONS[nid];
                                }
                            }

                        });

                    function getNodeInfo(d) {
                        var rowID = parseInt(d.row.temporary_id);
                        var splitResult = d.child.columnLink.split("_");
                        var m_n = splitResult[0];
                        var index = parseInt(splitResult[1]);

                        var node = _data[rowID][m_n][index];
                        if(!node) {
                            return {
                                nid: undefined,
                                nodeData: undefined,
                                m_n: m_n

                            };
                        }
                        var nid = node.id;
                        return {
                            nid: nid,
                            nodeData: mapService.data.map.source[nid],
                            m_n: m_n
                        }
                    }

                    var triangles_icons = rows.selectAll(".triangles_icons").selectAll("rect")
                        .on("click", function(d) {
                            var scrollTop = d3.select(".lu-wrapper")[0][0].scrollTop;


                            var _rows = lulubody.selectAll(".row")[0];
                            _rows.sort(function(a, b) {
                                var aValue = parseFloat(a.attributes["transform"].value.split(",")[1]);
                                var bValue = parseFloat(b.attributes["transform"].value.split(",")[1]);

                                return aValue - bValue;
                            });

                            var i = -1;
                            for(var j = 0; j < _rows.length; ++j) {
                                var _d = d3.select(_rows[j]).datum();

                                if(_d.id == d.id) {
                                    i = j;
                                }
                            }

                            if(!clicked[d.id]) {
                                clicked[d.id] = true;
                                lulubody.attr("height", parseInt(lulubody[0][0].attributes["height"].value) + horizonCharHeight);


                                adjustHorizonChartTopPos(i, +1);


                                var transformValue = _rows[i].attributes["transform"].value;
                                var splits = transformValue.split(",");
                                var value = parseFloat(splits[1]);

                                var horizonChart = horizonCharts.append("div")
                                    .attr("class", "horizon_chart_"+ d.id);

                                //console.log(d.m);
                                //console.log(d.n);

                                addHorizonChart(d.m, 0, _rows[i]);
                                addHorizonChart(d.n, mNum, _rows[i]);
                                addTimeIndicatorLine(mNum+nNum, _rows[i]);

                            } else {
                                clicked[d.id] = false;
                                var horizonChart = horizonCharts.select(".horizon_chart_"+ d.id)
                                    .remove();

                                adjustHorizonChartTopPos(i, -1);

                                lulubody.attr("height", parseInt(lulubody[0][0].attributes["height"].value) - horizonCharHeight);
                            }

                            function addHorizonChart(list, offset, row) {
                                var mCol = d3.select(row).selectAll(".tableData.stacked")[0][0];
                                var _transformValue = mCol.attributes["transform"].value;
                                var _splits = _transformValue.split(",")[0].split("(");
                                var colValue = parseFloat(_splits[1]);

                                for(var k = 0; k < list.length; ++k) {
                                    var horizon = Horizon();
                                    var selection = horizonChart.append("div")
                                        .attr("class", "horizon_chart")
                                        .style("width", mnColumnWidth+"px")
                                        .style("height", "236px")
                                        .style("position", "absolute")
                                        .style("top", offsetTop - scrollTop + headerHeight + value + "px")
                                        .style("left", offsetLeft + colValue + (offset+k)*columnWidth + "px");

                                    var values = new Array(288);
                                    var persons = list[k].time_slice_persons;
                                    for(var i = 0; i < values.length; ++i) {
                                        values[i] = 0;
                                    }
                                    for(var kk in persons) {
                                        values[kk] = persons[kk];
                                    }
                                    values = shrinkValues(48, values);

                                    horizon = horizon(selection, values, columnWidth);
                                }
                            }

                            function addTimeIndicatorLine(offset, row) {
                                var mCol = d3.select(row).selectAll(".tableData.stacked")[0][0];
                                var _transformValue = mCol.attributes["transform"].value;
                                var _splits = _transformValue.split(",")[0].split("(");
                                var colValue = parseFloat(_splits[1]);

                                var selection = horizonChart.append("div")
                                    .attr("class", "horizon_chart_timeIndicator")
                                    .style("position", "absolute")
                                    .style("top", parseFloat(d3.select("#lugui-wrapper")[0][0].offsetTop) - 17 + headerHeight + value - 125 - scrollTop+ "px")
                                    .style("left", parseFloat(d3.select("#lugui-wrapper")[0][0].offsetLeft) - 118 + 9 + colValue + offset*columnWidth + 118 + "px");


                                var scale = d3.scale.linear().domain([0, 24]).range([0, 236]);
                                var axis = d3.svg.axis()
                                    .scale(scale)
                                    .orient("right")
                                    .tickValues([0, 6, 12, 18, 24])
                                    .tickSize(6, 2);

                                var svg = selection.append("svg")
                                    .attr("width", 30)
                                    .attr("height", 256)
                                    .append("g")
                                    .attr("transform", "translate(0, 10)")
                                    .call(axis);
                            }

                            function shrinkValues(len, values) {
                                var _values = new Array(len);
                                var eleLen = Math.ceil(values.length / len);
                                for(var i = 0, k = 0; i < len; ++i) {
                                    var total = 0;
                                    for(var j = 0; j < eleLen; ++j) {
                                        if(k >= values.length) {
                                            break ;
                                        }

                                        total = total + values[k];
                                        ++k;
                                    }
                                    _values[i] = total;
                                }

                                return(_values);
                            }

                            function adjustHorizonChartTopPos(index, flag) {
                                for(var j = index+1; j < _rows.length; ++j) {
                                    var _d = d3.select(_rows[j]).datum();

                                    var transformValue = _rows[j].attributes["transform"].value;
                                    var splits = transformValue.split(",");
                                    var value = parseFloat(splits[1]);

                                    value = value + flag * horizonCharHeight;
                                    d3.select(_rows[j]).attr("transform", "translate(0,"+value+")");

                                    if(clicked[_d.id]) {
                                        horizonCharts.select(".horizon_chart_"+_d.id).selectAll(".horizon_chart")
                                            .style("top", offsetTop + headerHeight + value + "px");

                                        horizonCharts.select(".horizon_chart_"+_d.id).selectAll(".horizon_chart_timeIndicator")
                                            .style("top", offsetTop + headerHeight + value - 118 + "px");
                                    }
                                }
                            }

                        });

                }

                function setData(rawAllData) {
                    function loadDesc(desc) {  // load description
                        if (desc.data) {
                            loadDataImpl("m_n", desc, desc.data);
                        } else {
                            console.log("No Data!");
                        }
                    }

                    removeOrangeICON(orangeICONS);
                    clicked = {};
                    $("#horizon_charts").empty();


                    d3.select(".lu.lu-body").selectAll(".row").selectAll(".tableData.stacked").selectAll("rect").attr("height", 16);
                    //console.log();

                    var desc = constructDesc(rawAllData);
                    loadDesc(desc);

                    function removeOrangeICON(orangeICONS) {

                        for(var i in orangeICONS.sourceICONS) {
                            mapService.map.source.removeLayer(orangeICONS.sourceICONS[i]);
                        }
                        for(var i in orangeICONS.destICONS) {
                            mapService.map.dest.removeLayer(orangeICONS.destICONS[i]);
                        }

                        orangeICONS.sourceICONS = {};
                        orangeICONS.destICONS = {};
                    }
                }

                var mnColumnWidth = null;
                var mNum = 0;
                var nNum = 0;

                function constructDesc(rawAllData) {

                    var idWidth = 50;
                    var width = 500 - 50 - idWidth;

                    var m = 0;
                    var n = 0;
                    for(var i in rawAllData) {
                        var rawData = rawAllData[i].value;

                        var _m = rawData.m.length;
                        m = Math.max(m, _m);

                        var _n = rawData.n.length;
                        n = Math.max(n, _n);
                    }

                    mNum = m;
                    nNum = n;

                    mnColumnWidth = Math.floor(width / (m+n));

                    var desc = {};
                    desc.primaryKey = "id";
                    desc.columns = [];
                    desc.columns.push(stringModel("id"));
                    for(var i = 0; i < m; ++i) {
                        desc.columns.push(numberModel("m_"+i, [0, 1]));
                    }

                    for(var i = 0; i < n; ++i) {
                        desc.columns.push(numberModel("n_"+i, [0, 1]));
                    }


                    function stringModel(name) {
                        var objectModel = {
                            column: name,
                            type: "string"
                        };

                        return objectModel;
                    }

                    function numberModel(name, domain) {
                        var objectModel = {
                            column: name,
                            type: "number",
                            domain: domain
                        };

                        return objectModel;
                    }

                    desc.data = [];

                    var maxDayPersons = getMaxDayPersons(rawAllData);
                    var id = 0;
                    for(var i in rawAllData) {
                        var rawData = rawAllData[i].value;
                        var item = {};
                        item.id = parseInt(rawAllData[i].order);
                        item.temporary_id = id++;

                        var mList = rawData.m;
                        var mOpacity = [];
                        for (var k = 0; k < mList.length; ++k) {
                            item["m_" + k] = 1;
                            mOpacity.push(Math.max(0.2, rawData.m[k].day_persons/maxDayPersons));
                        }

                        var nList = rawData.n;
                        var nOpacity = [];
                        for (var k = 0; k < nList.length; ++k) {
                            item["n_" + k] = 1;
                            nOpacity.push(Math.max(0.2, rawData.n[k].day_persons/maxDayPersons));
                        }

                        item.mOpacity = mOpacity;
                        item.nOpacity = nOpacity;
                        item.m = rawData.m;
                        item.n = rawData.n;

                        desc.data.push(item);
                    }

                    desc.layout = {};
                    desc.layout.primary = [];

                    var mLayoutStack = layoutStackModel("m (Out)");
                    for(var i = 0; i < m; ++i) {
                        mLayoutStack.children.push(layoutColumnModel("m_"+i, mnColumnWidth));
                    }
                    desc.layout.primary.push(mLayoutStack);

                    var nLayoutStack = layoutStackModel("n (In)");
                    for(var i = 0; i < n; ++i) {
                        nLayoutStack.children.push(layoutColumnModel("n_"+i, mnColumnWidth));
                    }
                    desc.layout.primary.push(nLayoutStack);

                    function layoutColumnModel(name, width) {
                        var objectModel = {
                            column: name,
                            width: width
                        };

                        return objectModel;
                    }

                    function layoutStackModel(label) {
                        var objectModel = {
                            type: "stacked",
                            label: label,
                            children: []
                        };

                        return objectModel;
                    }

                    function getMaxDayPersons(rawAllData) {
                        var max = 0;

                        for(var i in rawAllData) {
                            var rawData = rawAllData[i].value;
                            for (var k in rawData.m) {
                                max = Math.max(max, rawData.m[k].day_persons);
                            }

                            for (var k in rawData.n) {
                                max = Math.max(max, rawData.n[k].day_persons);
                            }
                        }

                        return max;
                    }


                    //draw legend
                    d3.select("#legend_lineup svg").remove();

                    var margin = {left:15,right:15,top:25,bottom:40};
                    var legendTotalWidth = d3.select("#legend_lineup")[0][0].offsetWidth;
                    var legendWidth = legendTotalWidth/3;
                    //var legendWidth = d3.select("#legend_lineup")[0][0].offsetWidth;
                    var legendHeight = d3.select("#legend_lineup")[0][0].offsetHeight;
                    var legends = d3.select("#legend_lineup").append("svg").attr("id","legendOfLineup")
                        .attr("width",legendTotalWidth).attr("height",legendHeight);

                    var legend1 = legends.append("g").attr("transform","translate("+ "0"+",0)");
                    var legend2 = legends.append("g").attr("transform","translate("+(legendWidth)+",0)");
                    var legend3 = legends.append("g").attr("transform","translate("+(legendWidth*2)+",0)");

                    var colors = ['rgb(199,234,229)','rgb(128,205,193)','rgb(53,151,143)','rgb(1,102,94)'];
                    var labels = ['>0','>400','>800','>1200'];
                    var rectWidth = (legendWidth-margin.left-margin.right)/colors.length;

                    legend3.append("text").text("No. of people per hour").style("font-weight","bold")
                        .attr("transform","translate("+(margin.left) + "," + (margin.top-8)+")")
                        .style("font-size","8px");

                    var rectangle = legend3.selectAll(".legend").data(colors).enter()
                        .append("g").attr("class","legend");

                    rectangle.append("rect").attr("width",rectWidth)
                        .attr("height",legendHeight-margin.top-margin.bottom)
                        .attr("transform",function(d,i){return "translate("+(margin.left+i*rectWidth) + "," + margin.top+")"})
                        .attr("fill",function(d){return d});
                    rectangle.append("text").text(function(d,i){return labels[i]})
                        .attr("transform",function(d,i){return "translate("+(margin.left+i*rectWidth) + "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")"})
                        .style("font-size","8px");

                    var color1 = "purple",color2 = "orange";

                    var gradient1 = legend1.append("g").append("svg:defs").append("svg:linearGradient")
                        .attr("id", "gradient10").attr("x1", "0%").attr("y1", "0%").attr("x2", "100%").attr("y2", "0%").attr("spreadMethod", "pad");
                    gradient1.append("svg:stop").attr("offset", "0%").attr("stop-color", color1).attr("stop-opacity", 0.2);
                    gradient1.append("svg:stop").attr("offset", "100%").attr("stop-color", color1).attr("stop-opacity", 1);

                    legend1.append("g").append("rect").attr("width",legendWidth-margin.left-margin.right).attr("height",legendHeight-margin.top-margin.bottom).attr("transform","translate("+margin.left + "," + margin.top+")")
                        .attr("fill","url(#gradient10)");
                    legend1.append("text").text("No. of people (Out)").style("font-size","8px").style("font-weight","bold").style("text-anchor","start").attr("transform","translate("+margin.left + "," + (margin.top-8)+")");
                    legend1.append("text").text("0").style("font-size","8px").style("text-anchor","start").attr("transform","translate("+margin.left + "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")");
                    legend1.append("text").text(maxDayPersons).style("text-anchor","end").style("font-size","8px").attr("transform","translate("+(legendWidth-margin.right)+ "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")");


                    var gradient2 = legend2.append("g").append("svg:defs").append("svg:linearGradient")
                        .attr("id", "gradient11").attr("x1", "0%").attr("y1", "0%").attr("x2", "100%").attr("y2", "0%").attr("spreadMethod", "pad");
                    gradient2.append("svg:stop").attr("offset", "0%").attr("stop-color", color2).attr("stop-opacity", 0.2);
                    gradient2.append("svg:stop").attr("offset", "100%").attr("stop-color", color2).attr("stop-opacity", 1);

                    legend2.append("g").append("rect").attr("width",legendWidth-margin.left-margin.right).attr("height",legendHeight-margin.top-margin.bottom).attr("transform","translate("+margin.left + "," + margin.top+")")
                        .attr("fill","url(#gradient11)");
                    legend2.append("text").text("No. of people (In)").style("font-size","8px").style("font-weight","bold").style("text-anchor","start").attr("transform","translate("+margin.left + "," + (margin.top-8)+")");
                    legend2.append("text").text("0").style("font-size","8px").style("text-anchor","start").attr("transform","translate("+margin.left + "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")");
                    legend2.append("text").text(maxDayPersons).style("text-anchor","end").style("font-size","8px").attr("transform","translate("+(legendWidth-margin.right)+ "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")");


                    return desc;
                }

                var orangeICONS = {
                    sourceICONS: [],
                    destICONS: []
                };

                var clicked = {};

                LineUp.setData = setData;
                lineUpService.lineUp = LineUp;
            }

        }
    }
}]);