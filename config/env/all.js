'use strict';

module.exports = {
	app: {
		title: 'Telco Vis',
		description: 'Telco Vis',
		keywords: 'urban, telco, visualization'
	},
	port: process.env.PORT || 3007,
	templateEngine: 'swig',
	// The secret should be set to a non-guessable string that
	// is used to compute a session hash
	sessionSecret: 'MEAN',
	// The name of the MongoDB collection to store sessions in
	sessionCollection: 'sessions',
	// The session cookie settings
	sessionCookie: {
		path: '/',
		httpOnly: true,
		// If secure is set to true then it will cause the cookie to be set
		// only when SSL-enabled (HTTPS) is used, and otherwise it won't
		// set a cookie. 'true' is recommended yet it requires the above
		// mentioned pre-requisite.
		secure: false,
		// Only set the maxAge to null if the cookie shouldn't be expired
		// at all. The cookie will expunge when the browser is closed.
		maxAge: null
		// To set the cookie in a specific domain uncomment the following
		// setting:
		// domain: 'yourdomain.com'
	},
	// The session cookie name
	sessionName: 'connect.sid',
	log: {
		// Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
		format: 'combined',
		// Stream defaults to process.stdout
		// Uncomment to enable logging to a log on the file system
		options: {
			stream: 'access.log'
		}
	},
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
                'public/lib/leaflet/dist/leaflet.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',

                'public/lib/jquery/dist/jquery.min.js',
                'public/lib/jquery-ui/jquery-ui.min.js',

                'public/lib/underscore/underscore-min.js',
                'public/lib/underscore/underscore.math.js',

                'public/lib/bootstrap/dist/js/bootstrap.min.js',

                'public/lib/d3/d3.min.js',
                'public/lib/d3/d3.geom.nhull.js',
                'public/lib/d3.layout.voronoiTreemap.js',

                'public/lib/leaflet/dist/leaflet.js',
                'public/lib/leaflet/dist/leaflet-heat.js',
                'public/lib/leaflet/dist/L.D3SvgOverlay.min.js',

                'public/lib/circle_packer_mover.js',
                'public/lib/numeric-1.2.6.min.js',
                'public/lib/mds.js',
                'public/lib/power.js',
                'public/lib/horizon.js',

                'public/lib/lineup/lineup_datastructure.js',
                'public/lib/lineup/lineup_storage.js',
                'public/lib/lineup/lineup.js',
                'public/lib/lineup/lineup_tableheader.js',
                'public/lib/lineup/lineup_tablebody.js',
                'public/lib/lineup/lineup_gui_helper.js',
                'public/lib/lineup/lineup_mappingeditor.js',

                'public/lib/flowmap/FlowMap.js',
                'public/lib/flowmap/NormalFlowMap.js',
                'public/lib/flowmap/PointsView.js',
                'public/lib/flowmap/RatioBarView.js',
                'public/lib/flowmap/VoronoiRegionView.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
