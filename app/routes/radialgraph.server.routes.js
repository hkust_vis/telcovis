'use strict';

module.exports = function(app) {
    var radialGraph = require('../../app/controllers/radialgraph.server.controller');

    // handle contour-based treemap route with date
    app.route('/radialgraph').post(radialGraph.getRadialGraphData);
};
