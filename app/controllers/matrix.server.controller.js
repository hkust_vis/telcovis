'use strict';

var mongoose = require('mongoose');
var _ = require('lodash');
var func = require('./func.server.controller.js');
var lineUp = require('./lineup.server.controller.js');


exports.getMatrixData = getMatrixData;
exports.getMatrixDataHours = getMatrixDataHours;
exports.testCellDataDeeply = testCellDataDeeply;

// get matrix data
function getMatrixData(req, res) {
    var paras = req.body;

    var dateList = paras.dateList;

    var date = dateList.join('_');

    var mongoPara = {
        modelName: 'matrixData',
        collectionName: 'matrix_day',
        findPara: {
            date: date
        }
    };

    func.getDataMongo(mongoPara, function(data) {
        res.send(data[0]);
        res.end();
    });
}

// get matrix data of hours
function getMatrixDataHours(req, res) {
    var paras = req.body;

    var dateList = paras.dateList;
    var hours = paras.hours;

    var date = dateList.join('_');

    var mongoPara = {
        modelName: 'matrixHourData',
        collectionName: 'matrix_hour',
        findPara: {
            date: date,
            hour: {
                '$in': hours
            }
        }
    };

    func.getDataMongo(mongoPara, function(data) {
        data = combineData(data);
        data.date = date;
        res.send(data);
        res.end();
    });

    function combineData(data) {
        var newData = {};

        var matrix = {};
        for(var i in data) {
            var node = data[i];
            if(matrix.hasOwnProperty(node.m_n)) {
                combineValue(matrix[node.m_n], node.value);
            } else {
                matrix[node.m_n] = node.value;
            }
        }

        for(var i in matrix) {
            matrix[i].m_n_num = matrix[i].m_n_record.length;
            delete matrix[i].m_n_record;
        }

        function combineValue(valueA, valueB) {
            valueA.m_n_record = _.union(valueA.m_n_record, valueB.m_n_record);
            valueA.source = _.union(valueA.source, valueB.source);
            valueA.destination = _.union(valueA.destination, valueB.destination);
        }

        newData.matrix = matrix;

        newData.max_m = 0;
        newData.max_n = 0;
        for(var i in matrix) {
            var splits = i.split('_');
            newData.max_m = Math.max(newData.max_m, +splits[0]);
            newData.max_n = Math.max(newData.max_n, +splits[1]);
        }

        return newData;
    }
}

// test matrix cells
function testCellDataDeeply(req, res) {
    var paras = req.body;

    var dateList = paras.dateList;
    var m_n = paras.m_n;
    var selectedSource = paras.selectedSource;
    var selectedDest = paras.selectedDest;

    lineUp.getLineUpDataFilteredWithCallback(dateList, m_n, selectedSource, selectedDest, false, function(filteredData) {
        var flag = false;

        if(filteredData.length > 0) {
            flag = true;
        }

        res.send({
            flag: flag
        });
        res.end();
    });
}