/**
 * Created by Jiayi Xu on 12/1/15.
 */


// render ratio bars
function RatioBarView(className, nodes, weights, flowData, pos, width, container, flowMapService) {

    var height = 20;
    var padding = 25;
    var barWidth = width - 2 * padding;

    var innerRatios = [];
    var maxInnerinnerRatio = 0;

    var flowOfClu = [];
    var flowSum = 0;
    var contactOfClu = [];
    var contactSum = 0;

    for(var i = 0; i < nodes.length; ++i) {
        var _nodes = nodes[i];
        var flowNum = 0;
        var contactNum = 0;
        var maxPossibleContacts = 0;

        var flow0 = 0;
        var flow1 = 0;


        for(var j = 0; j< _nodes.length; ++j) {
            var nid = _nodes[j].nid;

            for(var k = 0; k < 24; ++k) {
                var _flow0 = 0;
                var _flow1 = 0;

                if(flowData[0].hasOwnProperty(k) && flowData[0][k].hasOwnProperty(nid)) {
                    _flow0 = flowData[0][k][nid];
                    flowNum = flowNum + _flow0;
                }
                if(flowData[1].hasOwnProperty(k) && flowData[1][k].hasOwnProperty(nid)) {
                    _flow1 = flowData[1][k][nid];
                    flowNum = flowNum + _flow1;
                }

                maxPossibleContacts = maxPossibleContacts + _flow0 * _flow1;

                flow0 = flow0 + _flow0;
                flow1 = flow1 + _flow1;
            }

            contactNum = contactNum + weights[i][j];
        }
        flowOfClu.push(flowNum);

        var innerinnerRatio = 0;
        if(maxPossibleContacts > 0) {
            innerinnerRatio = contactNum / maxPossibleContacts;
        }
        if(innerinnerRatio > maxInnerinnerRatio) {
            maxInnerinnerRatio = innerinnerRatio;
        }

        if(contactNum > 0) {
            contactNum = Math.log(contactNum) / Math.log(2);
        }


        contactOfClu.push(contactNum);
        flowSum = flowSum + flowNum;
        contactSum = contactSum + contactNum;
    }

    maxInnerinnerRatio = maxInnerinnerRatio * 1.2;

    for(var i = 0; i < nodes.length; ++i) {
        innerRatios.push(contactOfClu[i]/contactSum);
    }

    var sum = 0;

    flowMapService.data.innerRatioBarCenterPos = [];
    var colorsList = flowMapService.data.clusterColors;

    for(var i = 0; i < innerRatios.length; ++i) {
        flowMapService.data.innerRatioBarCenterPos.push({X: padding + pos[0] + (sum+innerRatios[i]/2)*barWidth, Y: pos[1] + 15});
        sum = sum + innerRatios[i];
    }

    drawInnterRatioBarView();

    var ratioBar = {
        drawInnterRatioBarView: drawInnterRatioBarView
    };

    flowMapService.data.ratioBar = ratioBar;

    return ratioBar;

    function drawInnterRatioBarView() {
        var ratioBarView = container.append('g')
            .style("isolation", "isolate")
            .attr("class", className)
            .attr("transform", "translate(" + pos[0] + padding + "," + pos[1] + ")");

        var sum = 0;

        var maxInnerinnerRatio = 0;

        for(var i = 0; i < innerRatios.length; ++i) {
            var innerBarView = InnerRatioBarView(ratioBarView, [sum * barWidth, 0], innerRatios[i] * barWidth, height, colorsList[i]);

            sum = sum + innerRatios[i];
        }
    }

    function InnerRatioBarView(parent, pos, width, height, color) {

        var innerRatioBarView = parent.append('g')
            .attr("transform", "translate(" + pos[0] + "," + pos[1] + ")");

        var innerRatioBar = innerRatioBarView
            .append("rect")
            .attr("width", width)
            .attr("height", height)
            .attr("stroke", "none")
            .attr("fill", color);

        return innerRatioBarView;
    }
}

