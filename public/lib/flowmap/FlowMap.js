/**
 * Created by Jiayi Xu on 15/1/15.
 */

function FlowMap(flowData, nodes, width, height, flowWidth, container, flowMapService) {

    flowMapService.setHours = setHours;

    var flowMap = container.append('g')
        .attr("class", "flowMap")
        .style("isolation", "isolate");

    var line = d3.svg.line()
        .x(function(d) {
            return d.X;
        })
        .y(function(d) {
            return d.Y;
        })
        .interpolate("cardinal");

    var sourcePoints = flowMapService.data.sourcePoints;

    var maxVolume = getMaxVolume();

    var cluNum = nodes.length;
    for(var ci = 0; ci < cluNum; ++ci) {
        var _nodes = nodes[ci];
        for(var ni = 0; ni < _nodes.length; ++ni) {
            for (var t = 0; t < 24; ++t) {
                for(var sp = 0; sp < sourcePoints.length; ++sp) {
                    drawFlow(sp, t, _nodes[ni]);
                }
            }
        }
    }

    //  shadow the flows out of certain hours
    function setHours(hours) {
        var flows = d3.selectAll('.flow');

        flows.each(function(d) {
            var color = 'grey';
            var opacity = 0.1;

            if(_.contains(hours, d.hour)) {
                color = d.color;
                opacity = 1;
            }

            d3
                .select(this)
                .attr('stroke', color)
                .attr('stroke-opacity', opacity);
        });
    }

    // get max volume of each flow
    function getMaxVolume() {
        var maxVolume = 0;

        for (var i = 0; i < 2; ++i) {
            for (var j = 0; j < 24; ++j) {
                if (!flowData[i].hasOwnProperty(j)) {
                    continue;
                }

                var cluNum = flowMapService.data.clusterNum;
                for (var k = 0; k < cluNum; ++k) {
                    var nodesNum = nodes[k].length;
                    for (var L = 0; L < nodesNum; ++L) {
                        if (!flowData[i][j].hasOwnProperty(nodes[k][L].nid)) {
                            continue;
                        }

                        if (flowData[i][j][nodes[k][L].nid] > maxVolume) {
                            maxVolume = flowData[i][j][nodes[k][L].nid];
                        }
                    }
                }
            }
        }

        return maxVolume;
    }

    // draw flow
    function drawFlow(index, t, d) {
        if (flowData[index].hasOwnProperty(t) && flowData[index][t].hasOwnProperty(d.nid)) {
            var destPos = {
                X: d.pX,
                Y: d.pY
            };
            var innerRatioBarCenterPos = flowMapService.data.innerRatioBarCenterPos[d.cid];
            var timeBarPos = flowMapService.data.timeBarPos[d.cid];
            var timePos = flowMapService.timePos(timeBarPos, t);
            var volume = flowData[index][t][d.nid];

            // set positions
            var flowDatum = [sourcePoints[index].pos, innerRatioBarCenterPos, timePos, {X: timeBarPos.X, Y: timeBarPos.Y + 40}, destPos];
            flowDatum.source = index;
            flowDatum.target = d.nid;
            flowDatum.opa = 0;
            flowDatum.hour = t;
            flowDatum.color = flowMapService.data.flowColors[index];
            flowDatum.width = volume / maxVolume * flowWidth;
            if (flowDatum.width < 0.1) {
                flowDatum.width = 0.1;
            }

            // draw flow on svg
            flowMap.append("path")
                .datum(flowDatum)
                .attr("class", "flow")
                .attr("stroke", function (d) {
                    return d.color;
                })
                .attr("stroke-linecap", "round")
                .attr("stroke-width", function (d) {
                    return d.width;
                })
                .attr("stroke-opacity", 1)
                .attr("d", line)
                .attr("fill", "none")
                .style("mix-blend-mode", "Lighten");
        }
    }

    return flowMap;
}