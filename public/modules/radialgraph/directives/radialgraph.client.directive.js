'use strict';

angular.module('radialGraph').directive('radialGraphDirective', ["radialGraphService", 'funcService', 'pipService', 'mapService', function(radialGraphService, funcService, pipService, mapService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            var isLegend = true;

            var rawData = null;

            var sortType = "distance";
            var maxDistance = 20;
            var sigmaNum = 2;

            var startTime = 0;
            var endTime = 23;

            var hoursPerSector = 2;

            pipService.onDateListChange(scope, function() {
                rawData = null;
                clear();
            });

            pipService.onHourRangeChange(scope, function(hourRange) {
                radialGraphService.setRadialGraphStartAndEndTime(hourRange.start, hourRange.end);
                radialGraphService.constructRadialGraph();
            });


            radialGraphService.switchIsLegend = switchIsLegend;

            radialGraphService.constructRadialGraph = constructRadialGraph;

            radialGraphService.setRadialGraphStartAndEndTime = setRadialGraphStartAndEndTime;
            radialGraphService.setRadialGraphHoursPerSector = setRadialGraphHoursPerSector;
            radialGraphService.setRadialGraph_sortType = setRadialGraph_sortType;
            radialGraphService.setRadialGraphMaxDistance = setRadialGraphMaxDistance;
            radialGraphService.setRadialGraphSigmaOfGaussianSmooth = setRadialGraphSigmaOfGaussianSmooth;

            function switchIsLegend() {
                isLegend = !isLegend;
                d3
                    .select("#legend_tree_map")
                    .style("display", isLegend ? "block" : "none");
            }

            function drawLegend() {
                d3.select("#legend_tree_map svg").remove();

                var margin = {left: 15, right: 10, top: 20, bottom: 15};

                var legendWidth = d3.select("#legend_tree_map")[0][0].offsetWidth;
                var legendHeight = d3.select("#legend_tree_map")[0][0].offsetHeight;
                var legend = d3.select("#legend_tree_map").append("svg").attr("id", "legendOfTreemap")
                    .attr("width", legendWidth).attr("height", legendHeight);

                var gradient = legend.append("g").append("svg:defs")
                    .append("svg:linearGradient")
                    .attr("id", "gradient3")
                    .attr("x1", "0%")
                    .attr("y1", "0%")
                    .attr("x2", "100%")
                    .attr("y2", "0%")
                    .attr("spreadMethod", "pad");

                gradient.append("svg:stop")
                    .attr("offset", "0%")
                    .attr("stop-color", "rgb(247,252,245)")
                    .attr("stop-opacity", 1);

                gradient.append("svg:stop")
                    .attr("offset", "90%")
                    .attr("stop-color", "#41ab5d")
                    .attr("stop-opacity", 1);

                gradient.append("svg:stop")
                    .attr("offset", "100%")
                    .attr("stop-color", "#41ab5d")
                    .attr("stop-opacity", 1);

                legend.append("g").append("rect").attr("width", legendWidth - margin.left - margin.right).attr("height", legendHeight - margin.top - margin.bottom).attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                    .attr("fill", "url(#gradient3)");

                legend.append("text").text("Distance (km)").style("font-size", "6px").style("font-weight", "bold").style("text-anchor", "start").attr("transform", "translate(" + margin.left + "," + (margin.top - 5) + ")");

                legend.append("text").text("0").style("font-size", "6px").style("text-anchor", "start").attr("transform", "translate(" + margin.left + "," + (legendHeight - margin.bottom + legendHeight - margin.top - margin.bottom) + ")");

                legend.append("text").text(maxDistance).style("text-anchor", "end").style("font-size", "6px").attr("transform", "translate(" + (legendWidth - margin.right) + "," + (legendHeight - margin.bottom + legendHeight - margin.top - margin.bottom) + ")");
            }

            function setRadialGraphStartAndEndTime(_startTime, _endTime) {
                if(_startTime < 0) {
                    _startTime = 0;
                }
                if(_endTime > 23) {
                    _endTime = 23;
                }

                if(startTime == _startTime && endTime == _endTime) {
                    return ;
                }

                startTime = _startTime;
                endTime = _endTime;
            }


            function setRadialGraphHoursPerSector(_hoursPerSector) {
                if(hoursPerSector == _hoursPerSector) {
                    return ;
                }

                hoursPerSector = _hoursPerSector;
            }

            function setRadialGraph_sortType(_sortType) {
                if(sortType == _sortType) {
                    return ;
                }

                sortType = _sortType;
            }

            function setRadialGraphMaxDistance(_maxDistance) {
                if(maxDistance == _maxDistance) {
                    return ;
                }

                maxDistance = _maxDistance;
            }

            function setRadialGraphSigmaOfGaussianSmooth(_sigmaNum) {
                if(sigmaNum == _sigmaNum) {
                    return ;
                }

                sigmaNum = _sigmaNum;
            }


            pipService.onRadialGraphDataChange(scope, function(data) {
                rawData = data;

                constructRadialGraph();
            });


            function isDataAvailable() {
                if(!rawData || rawData == null) {
                    return false;
                }

                return true;
            }

            function clear() {
                d3.select("#radial_graph_svg").remove();
            }

            function constructRadialGraph() {
                clear();

                if(!isDataAvailable()) {
                    return ;
                }

                var loyaltySliceNum = 1;    // adjust loyalty
                var timeSliceNum = 288;
                var directionSliceNum = (endTime-startTime+1) * (288/24); // adjust direction slice (time slice)

                var marker = null;
                var selected_nid = -1;

                var radialGraph = d3
                    .select("#radial_graph")
                    .append("svg")
                    .attr("id", "radial_graph_svg");

                var radialGraph_g = radialGraph
                    .append("g")
                    .attr("transform", "translate(" + 200 + "," + 300+")");

                var areaType = "persons";

                var nodes = getNodeList();

                var realMaxDistance = d3.max(nodes, function(d) {   // the max distance of data, sometimes we could encode using this
                    return d.distance;
                });

                var totalPersonsOfGraph = nodes.reduce(function(preV, curV) {
                    return preV + curV.totalPersons;
                }, 0);

                setCid("loyalty" , 1, loyaltySliceNum, nodes);
                for(var i = 0; i < nodes.length; ++i) { // reverse loyalty layer
                    nodes[i].loyalty_id = loyaltySliceNum - 1 - nodes[i].loyalty_id;
                }

                var grids = constructGrids();

                gaussianSmooth(grids, loyaltySliceNum, directionSliceNum);
                calculateGridScale(grids, loyaltySliceNum, directionSliceNum);

                drawRadialGraph(grids, loyaltySliceNum, directionSliceNum);

                function getNodeList() {
                    var nodes = [];
                    for(var i in rawData) {
                        var nid = i;
                        var node = rawData[nid];
                        node.nid = nid;

                        for(var j in node.value) {
                            node[j] = node.value[j];
                        }
                        node.value = node[areaType];

                        node.personsOfHours = [];
                        for(var j = 0; j < 24; ++j) {
                            node.personsOfHours.push(0);
                        }
                        var step = timeSliceNum / 24;
                        var totalPersons = 0;
                        for(var j in node.persons) {
                            var _j = Math.floor(j / step);

                            node.personsOfHours[_j] = node.personsOfHours[_j] + node.persons[j];
                            totalPersons = totalPersons + node.persons[j];
                        }
                        node.totalPersons = totalPersons;

                        if(node.distance <= 0.4) {    // filter out distance == 0.4
                            continue ;
                        }

                        nodes.push(node);
                    }

                    return nodes;
                }

                function setCid(propertyName , total, sliceNum, nodes) {

                    var step = total / sliceNum;

                    for(var i = 0; i < nodes.length; ++i) {
                        var value = nodes[i][propertyName];
                        var id = Math.floor(value / step);
                        if(id >= sliceNum) {
                            id = sliceNum - 1;
                        }

                        nodes[i][propertyName+"_id"] = id;
                    }
                }

                function constructGrids() {
                    var grids = new Array(loyaltySliceNum);
                    for (var i = 0; i < loyaltySliceNum; ++i) {
                        grids[i] = new Array(directionSliceNum);
                        for (var j = 0; j < directionSliceNum; ++j) {
                            grids[i][j] = [];
                            grids[i][j].value = 0;
                        }
                    }

                    for(var i in nodes) {
                        var node = nodes[i];
                        for(var j in node.persons) {
                            var step = timeSliceNum / 24;
                            var hour = Math.floor(j / step);
                            if(hour >= startTime && hour <= endTime) {
                                var _j = j - startTime * (timeSliceNum / 24);

                                grids[node.loyalty_id][_j].value = grids[node.loyalty_id][_j].value + node.persons[j];
                            }
                        }
                    }

                    return grids;
                }

                function calculateGaussianValue(d, sigma) {
                    return Math.exp(-d*d/2/sigma/sigma)/Math.sqrt(2*Math.PI)/sigma;
                }

                function gaussianSmooth(grids, loyaltySliceNum, directionSliceNum) {
                    for(var i = 0; i < loyaltySliceNum; ++i) {
                        for(var j = 0; j < directionSliceNum; ++j) {
                            var grid = grids[i][j];
                            grid.gaussianValue = 0;

                            for(var k = 0; k < directionSliceNum; ++k) {
                                var _grid = grids[i][k];

                                var d1 = k-j;
                                if(d1 < 0) {
                                    d1 = d1 + directionSliceNum;
                                }
                                var d2 = j-k;
                                if(d2 < 0) {
                                    d2 = d2 + directionSliceNum;
                                }
                                var dis = Math.min(d1, d2);

                                grid.gaussianValue = grid.gaussianValue + _grid.value * calculateGaussianValue(dis, sigmaNum);
                            }
                        }
                    }

                }

                function calculateGridScale(grids, loyaltySliceNum, directionSliceNum) {    // scale = sqrt(gaussianValue)
                    for(var i = 0; i < loyaltySliceNum; ++i) {
                        for(var j = 0; j < directionSliceNum; ++j) {
                            var grid = grids[i][j];
                            grid.scale = Math.sqrt(grid.gaussianValue);
                        }
                    }
                }

                function drawRadialGraph(grids, loyaltySliceNum, directionSliceNum) {
                    var maxLength = 150;

                    var maxScale = 0;
                    for(var i = 0; i < loyaltySliceNum; ++i) {
                        if (nodes.length == 0) {
                            continue;
                        }

                        // calculate angles
                        var clus = constructClusters(nodes);
                        var clusAngles = generateClusAngles(i, clus);

                        maxScale = d3.max(clusAngles, function (d) {
                            return d3.max(d, function (_d) {
                                return _d.scaleCeil;
                            })
                        });

                        // draw treemap
                        var roots = [];
                        for(var j = 0; j < clus.length; ++j) {
                            var root = {
                                elements: clus[j],
                                edges: clusAngles[j]
                            };

                            constructTree(root, clus[j].timeSlice);
                            roots.push(root);
                        }

                        for(var j = 0; j < roots.length; ++j) {
                            drawTreemap(roots[j]);
                        }
                    }

                    drawClockDial();

                    function constructClusters(nodes) {
                        var clusLen = Math.ceil((endTime-startTime+1) / hoursPerSector);

                        var clus = [];
                        var clusSet = [];   // make sure uniqueness of nodes in one hour sector

                        // init clusters
                        for(var i = 0; i < clusLen; ++i) {
                            var clu = [];
                            clu.persons = 0;
                            clu.timeSlice = i;

                            clus.push(clu);
                            clusSet.push(d3.set());
                        }

                        for(var i in nodes) {
                            for(var j in nodes[i].personsOfHours) {
                                if(j<startTime || j>endTime) {  // get cluster of certain hours
                                    continue ;
                                }

                                var _j = Math.floor((j-startTime) / hoursPerSector);

                                if(nodes[i].personsOfHours[j] > 0) {
                                    clus[_j].persons = clus[_j].persons + nodes[i].personsOfHours[j];

                                    if(!clusSet[_j].has(i)) {
                                        clus[_j].push(nodes[i]);
                                        clusSet[_j].add(i);
                                    }
                                }
                            }
                        }

                        generateMinMaxAngleOfClusters(clus);

                        return clus;
                    }

                    function generateMinMaxAngleOfClusters(clus) {
                        clus[0].minAngle = 0;
                        clus[0].maxAngle = 360;
                        if(hoursPerSector<=(endTime-startTime+1)) {
                            clus[0].maxAngle = hoursPerSector/(endTime-startTime+1)*360;
                        }

                        for(var i = 1; i < clus.length-1; ++i) {
                            clus[i].minAngle = clus[i-1].maxAngle;
                            clus[i].maxAngle = clus[i].minAngle + hoursPerSector/(endTime-startTime+1)*360;
                        }
                        if(clus.length > 1) {
                            clus[clus.length-1].minAngle = clus[clus.length-2].maxAngle;
                            clus[clus.length-1].maxAngle = 360;
                        }
                    }

                    function generateClusAngles(loyaltyLevel, clus) {
                        var clusAngles = [];

                        for(var j = 0; j < clus.length; ++j) {
                            var clusAngle = [];
                            clusAngle.startAngle = clus[j].minAngle;
                            clusAngle.endAngle = clus[j].maxAngle;

                            clusAngles.push(clusAngle);
                        }

                        // If grids are on the range of clusAngle, then simply add them.
                        for (var j = 0; j < clus.length; ++j) {
                            transformGridsToCluAngles(loyaltyLevel, clusAngles[j]);
                        }

                        // head and tail are normally not on a certain grid, so we need to calculate their info
                        generateClusAnglesHeadAndTail(clusAngles);

                        return clusAngles;
                    }

                    function transformGridsToCluAngles(loyaltyLevel ,cluAngles) {
                        var step = 360 / directionSliceNum;

                        cluAngles.length = 0;
                        var startAngle = cluAngles.startAngle;
                        var endAngle = cluAngles.endAngle;

                        var offset = 0;
                        if (endAngle <= startAngle) {
                            offset = 360;
                        }

                        var n = Math.floor((startAngle - step / 2) / step);

                        if (!funcService.isInRangeInclude(n * step + step / 2, startAngle, endAngle + offset)) {
                            n = n + 1;
                        }

                        while (funcService.isInRangeInclude(n * step + step / 2, startAngle, endAngle + offset)) {
                            var _n = n;
                            var angle = n * step + step / 2;
                            if (angle >= 360) {
                                angle = angle - 360;
                                _n = _n - directionSliceNum;
                            }

                            cluAngles.push({
                                angle: angle,
                                scaleFloor: loyaltyLevel - 1 < 0 ? 0 : grids[loyaltyLevel - 1][_n].scale,
                                scaleCeil: grids[i][_n].scale
                            });

                            n = n + 1;
                        }
                    }

                    function generateClusAnglesHeadAndTail(clusAngles) {
                        for(var j = 0; j < clus.length; ++j) {
                            var cluAngles = clusAngles[j];

                            var cluAngleHead = generateCluAngle(cluAngles.startAngle);
                            var cluAngleTail = generateCluAngle(cluAngles.endAngle);

                            // cluAngleHead is the head of cluAngles and cluAngleTail is the tail.
                            cluAngles.splice(0, 0, cluAngleHead);
                            cluAngles.push(cluAngleTail);
                        }
                    }

                    function generateCluAngle(gama) {   // calculate CluAngle, which the angle is not on certain grid
                        var step = 360 / directionSliceNum;

                        var cluAngle = {
                            angle: gama,
                            scaleFloor: 0,
                            scaleCeil: 0
                        };

                        var _gama = gama-step/2;
                        if(_gama<0) {
                            _gama = _gama+360;
                        }
                        if(_gama >= 360) {
                            _gama = _gama-360;
                        }

                        var pre_gridIndex = Math.floor(_gama / step);

                        if(pre_gridIndex*step == _gama) {
                            cluAngle.scaleFloor = i-1<0?0:grids[i-1][pre_gridIndex].scale;
                            cluAngle.scaleCeil = grids[i][pre_gridIndex].scale;

                        } else {
                            var succ_gridIndex = pre_gridIndex+1;

                            var alpha = step/2 + pre_gridIndex*step;
                            var beta = step/2 + succ_gridIndex*step;

                            if(succ_gridIndex >= directionSliceNum) {
                                succ_gridIndex = succ_gridIndex - directionSliceNum;

                                if(beta < alpha) {
                                    beta = beta + 360;
                                }
                                if(gama < alpha) {
                                    gama = gama + 360;
                                }
                            }

                            cluAngle.scaleFloor = 0;
                            if(i-1>=0) {
                                cluAngle.scaleFloor = getScaleInGivenLine(gama, alpha, grids[i-1][pre_gridIndex].scale, beta, grids[i-1][succ_gridIndex].scale);
                            }
                            cluAngle.scaleCeil = getScaleInGivenLine(gama, alpha, grids[i][pre_gridIndex].scale, beta, grids[i][succ_gridIndex].scale);
                        }

                        return cluAngle;
                    }


                    function getScaleInGivenLine(angle, angle1, scale1, angle2, scale2) {

                        var radian = angle/180*Math.PI;

                        var p1 = [scale1*Math.cos(angle1/180*Math.PI), scale1*Math.sin(angle1/180*Math.PI)];
                        var p2 = [scale2*Math.cos(angle2/180*Math.PI), scale2*Math.sin(angle2/180*Math.PI)];

                        var scale = 0;

                        if(p2[0]-p1[0] != 0) {
                            var k = (p2[1] - p1[1]) / (p2[0] - p1[0]);
                            var b = p1[1] - p1[0]*k;

                            scale = b/(Math.sin(radian)-k*Math.cos(radian));
                        } else {

                            scale = p1[0] / Math.cos(radian);
                        }

                        return scale;
                    }

                    function getAngleInGivenLine(scale, angle1, scale1, angle2, scale2) {   // precision of acos is too bad, sad~~~
                        var e = 1e-6;

                        if(Math.abs(scale-scale1) < e) {
                            return angle1;
                        }
                        if(Math.abs(scale-scale2) < e) {
                            return angle2;
                        }

                        var p1 = [scale1*Math.cos(angle1/180*Math.PI), scale1*Math.sin(angle1/180*Math.PI)];
                        var p2 = [scale2*Math.cos(angle2/180*Math.PI), scale2*Math.sin(angle2/180*Math.PI)];

                        if(p2[0]-p1[0] != 0) {
                            var k = (p2[1] - p1[1]) / (p2[0] - p1[0]);
                            var b = p1[1] - p1[0]*k;

                            var delta2 = b*b*k*k-(k*k+1)*(b*b-scale*scale);
                            var delta = Math.sqrt(delta2);

                            var cosAngle_1 = (-b*k+delta)/(k*k+1)/scale;
                            var sinAngle_1 = (k*scale*cosAngle_1+b)/scale;
                            var angle_1 = Math.acos(cosAngle_1);

                            if(Math.abs(Math.sin(angle_1) - sinAngle_1) > e) {
                                angle_1 = 2*Math.PI - angle_1;
                            }

                            angle_1 = angle_1/Math.PI*180;
                            if(angle_1<0) {
                                angle_1 = angle_1 + 360;
                            }
                            if(angle_1>=360) {
                                angle_1 = angle_1 - 360;
                            }

                            if(isInclude(angle_1, angle1, angle2)) {
                                return angle_1;
                            }

                            var cosAngle_2 = (-b*k-delta)/(k*k+1)/scale;
                            var sinAngle_2 = (k*scale*cosAngle_2+b)/scale;
                            var angle_2 = Math.acos(cosAngle_2);

                            if(Math.abs(Math.sin(angle_2) - sinAngle_2) > e) {
                                angle_2 = 2*Math.PI - angle_2;
                            }

                            angle_2 = angle_2/Math.PI*180;
                            if(angle_2<0) {
                                angle_2 = angle_2 + 360;
                            }
                            if(angle_2>=360) {
                                angle_2 = angle_2 - 360;
                            }

                            if(isInclude(angle_2, angle1, angle2)) {
                                return angle_2;
                            }

                        } else {
                            var cosAngle = p1[0] / scale;
                            var angle = Math.acos(cosAngle_1);
                            angle = angle/Math.PI*180;
                            if(isInclude(angle, angle1, angle2)) {
                                return angle;
                            } else {
                                angle = angle + 180;
                                if(isInclude(angle, angle1, angle2)) {
                                    return angle;
                                }
                            }
                        }

                        function isInclude(angle, angle1, angle2) {
                            var delta = angle-angle1;
                            if(delta < 0) {
                                delta = delta + 360;
                            }
                            var _delta = angle2-angle1;
                            if(_delta < 0) {
                                _delta = _delta + 360;
                            }

                            if(_delta >= delta) {
                                return true;
                            }

                            return false;
                        }

                        console.log("getAngleInGivenLine is Null !");
                        return null;
                    }

                    function constructTree(node, timeSlice) {
                        if(node.elements.length <= 1) {
                            return ;
                        }

                        var cutResult_vertical = verticalCut(node, timeSlice);

                        var cutResult_horozontal = horizontalCut(node, timeSlice);

                        node.children = cutResult_vertical;
                        if(cutResult_horozontal != null) {
                            var L_W_ratio_vertical = calculateLenthWidthRatioOfCutResult(cutResult_vertical);
                            var L_W_ratio_horizontal = calculateLenthWidthRatioOfCutResult(cutResult_horozontal);

                            if(L_W_ratio_horizontal <= L_W_ratio_vertical) {
                                node.children = cutResult_horozontal;
                            }
                        }

                        constructTree(node.children.left, timeSlice);
                        constructTree(node.children.right, timeSlice);
                    }

                    function calculateLenthWidthRatioOfCutResult(cutResult) {
                        var lwRatioLeft = calculateLenthWidthRatioOfNode(cutResult.left);
                        var lwRatioRight = calculateLenthWidthRatioOfNode(cutResult.right);

                        return (lwRatioLeft+lwRatioRight)/2;
                    }

                    function calculateLenthWidthRatioOfNode(node) {
                        var edges = node.edges;

                        var totalWidth = 0;
                        for(var i = 0; i < edges.length; ++i) {
                            var w1 = scaleToLength(edges[i].scaleFloor);
                            var w2 = scaleToLength(edges[i].scaleCeil);

                            totalWidth = totalWidth + w2 - w1;
                        }
                        var width = totalWidth / edges.length;

                        var totalUpLenth = 0;
                        var totalDownLenth = 0;
                        for(var i = 1; i < edges.length; ++i) {
                            var pre_up_p = getPoint_angle(edges[i-1].angle, scaleToLength(edges[i-1].scaleCeil));
                            var up_p = getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleCeil));
                            totalUpLenth = totalUpLenth + funcService.getDistance(pre_up_p, up_p);

                            var pre_down_p = getPoint_angle(edges[i-1].angle, scaleToLength(edges[i-1].scaleFloor));
                            var down_p = getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleFloor));
                            totalDownLenth = totalDownLenth + funcService.getDistance(pre_down_p, down_p);
                        }
                        var length = (totalUpLenth+totalDownLenth)/2;
                        var ratio = 1e9;
                        if(width > 0) {
                            ratio = length / width;
                        }
                        if(ratio <= 0) {
                            ratio = 1e9;
                        }
                        if(ratio < 1) {
                            ratio = 1 / ratio;
                        }

                        return ratio;
                    }

                    function verticalCut(node, timeSlice) {
                        return cut(node, sortType, "angle", timeSlice);
                    }

                    function horizontalCut(node, timeSlice) {
                        return cut(node, sortType, "scale", timeSlice);
                    }

                    function cut(node, elePropertyType, edgePropertyType, timeSlice) {

                        var elements = node.elements;
                        var edges = funcService.objectClone(node.edges);

                        elements.sort(function(a, b) {
                            if(elePropertyType == "distance") {
                                return b[elePropertyType] - a[elePropertyType];
                            } else {
                                return a[elePropertyType] - b[elePropertyType];
                            }
                        });

                        var totalPersons = 0;
                        for(var i = 0; i < elements.length; ++i) {
                            totalPersons = totalPersons + getPersonsOfTimeslice(i, timeSlice);
                        }

                        var accuPersons = 0;
                        var index = -1;
                        for(var i = 0; i < elements.length; ++i) {
                            if(accuPersons+getPersonsOfTimeslice(i, timeSlice) > totalPersons/2) {
                                break ;
                            } else {
                                accuPersons = accuPersons + getPersonsOfTimeslice(i, timeSlice);
                                index = i;
                            }
                        }
                        if(index == -1) {
                            index = 0;
                            accuPersons = accuPersons + getPersonsOfTimeslice(0, timeSlice);
                        }

                        function getPersonsOfTimeslice(eleID ,timeSlice) {
                            var persons = 0;

                            var timeSliceEnd = Math.min(endTime, startTime+(timeSlice+1)*hoursPerSector-1);

                            for(var j = startTime+timeSlice*hoursPerSector; j <= timeSliceEnd; ++j) {
                                persons = persons + elements[eleID].personsOfHours[j];
                            }

                            return persons;
                        }

                        var ratio = accuPersons/totalPersons;
                        var bisectResult = bisect(edges, ratio, edgePropertyType);
                        if(bisectResult == null) {
                            return null;
                        }

                        var leftChild = {
                            elements: [],
                            edges: []
                        };
                        var rightChild = {
                            elements: [],
                            edges: []
                        };

                        for (var i = 0; i <= index; ++i) {
                            leftChild.elements.push(elements[i]);
                        }
                        for (var i = index + 1; i < node.elements.length; ++i) {
                            rightChild.elements.push(elements[i]);
                        }

                        if(edgePropertyType == "angle") {
                            edges = node.edges;

                            leftChild.edges = calculateLeftEdges(edges, bisectResult.edgeIndex, bisectResult.edge);
                            rightChild.edges = calculateRightEdges(edges, bisectResult.edgeIndex, bisectResult.edge);
                        }

                        if(edgePropertyType == "scale") {
                            edges = node.edges;

                            leftChild.edges = calculateUpEdges(edges, bisectResult.scale, bisectResult.property, bisectResult.startIndex, bisectResult.endIndex);
                            rightChild.edges = calculateDownEdges(edges, bisectResult.scale, bisectResult.property, bisectResult.startIndex, bisectResult.endIndex);
                        }

                        return {
                            left: leftChild,
                            right: rightChild
                        };

                        function calculateLeftEdges(edges, bisectEdgeIndex, edge) {
                            var result = [];

                            for (var i = 0; i <= bisectEdgeIndex; ++i) {
                                result.push(edges[i]);
                            }
                            result.push(edge);

                            result.startAngle = edges.startAngle;
                            result.endAngle = edge.angle;

                            return result;
                        }

                        function calculateRightEdges(edges, bisectEdgeIndex, edge) {
                            var result = [];

                            result.push(edge);
                            for (var i = bisectEdgeIndex + 1; i < edges.length; ++i) {
                                result.push(edges[i]);
                            }

                            result.startAngle = edge.angle;
                            result.endAngle = edges.endAngle;

                            return result;
                        }

                        function calculateDownEdges(edges, scale, property, startIndex, endIndex) {
                            var result = [];
                            result.startAngle = edges.startAngle;
                            result.endAngle = edges.endAngle;

                            if(property == "scaleCeil") {
                                for(var i =0; i<startIndex; ++i) {
                                    result.push(edges[i]);
                                }
                            }

                            if(startIndex != 0) {
                                var startEdge = {
                                    angle: 0,
                                    scaleFloor: scale,
                                    scaleCeil: scale
                                };

                                var angle = getAngleInGivenLine(scale, edges[startIndex-1].angle, edges[startIndex-1][property], edges[startIndex].angle, edges[startIndex][property]);
                                startEdge.angle = angle;

                                if(property == "scaleCeil") {
                                    startEdge.scaleFloor = getScaleInGivenLine(angle, edges[startIndex-1].angle, edges[startIndex-1]["scaleFloor"], edges[startIndex].angle, edges[startIndex]["scaleFloor"]);
                                } else {
                                    result.startAngle = angle;
                                }

                                result.push(startEdge);
                            }

                            for(var i = startIndex; i <= endIndex; ++i) {
                                var edge = {
                                    angle: edges[i].angle,
                                    scaleFloor: edges[i].scaleFloor,
                                    scaleCeil: scale
                                };

                                result.push(edge);
                            }

                            if(endIndex != edges.length-1) {
                                var endEdge = {
                                    angle: 0,
                                    scaleFloor: scale,
                                    scaleCeil: scale
                                };


                                var angle = getAngleInGivenLine(scale, edges[endIndex].angle, edges[endIndex][property], edges[endIndex+1].angle, edges[endIndex+1][property]);
                                endEdge.angle = angle;

                                if(property == "scaleCeil") {
                                    endEdge.scaleFloor = getScaleInGivenLine(angle, edges[endIndex].angle, edges[endIndex]["scaleFloor"], edges[endIndex+1].angle, edges[endIndex+1]["scaleFloor"]);
                                }  else {
                                    result.endAngle = angle;
                                }

                                result.push(endEdge);
                            }

                            if(property == "scaleCeil") {
                                for(var i = endIndex+1; i<edges.length; ++i) {
                                    result.push(edges[i]);
                                }
                            }


                            return result;
                        }

                        function calculateUpEdges(edges, scale, property, startIndex, endIndex) {
                            var result = [];
                            result.startAngle = edges.startAngle;
                            result.endAngle = edges.endAngle;

                            if(property == "scaleFloor") {
                                for(var i =0; i<startIndex; ++i) {
                                    result.push(edges[i]);
                                }
                            }

                            if(startIndex != 0) {
                                var startEdge = {
                                    angle: 0,
                                    scaleFloor: scale,
                                    scaleCeil: scale
                                };

                                var angle = getAngleInGivenLine(scale, edges[startIndex-1].angle, edges[startIndex-1][property], edges[startIndex].angle, edges[startIndex][property]);
                                startEdge.angle = angle;

                                if(property == "scaleFloor") {
                                    startEdge.scaleFloor = getScaleInGivenLine(angle, edges[startIndex-1].angle, edges[startIndex-1]["scaleCeil"], edges[startIndex].angle, edges[startIndex]["scaleCeil"]);
                                } else {
                                    result.startAngle = angle;
                                }

                                result.push(startEdge);
                            }

                            for(var i = startIndex; i <= endIndex; ++i) {
                                var edge = {
                                    angle: edges[i].angle,
                                    scaleFloor: scale,
                                    scaleCeil: edges[i].scaleCeil
                                };

                                result.push(edge);
                            }

                            if(endIndex != edges.length-1) {
                                var endEdge = {
                                    angle: 0,
                                    scaleFloor: scale,
                                    scaleCeil: scale
                                };

                                var angle = getAngleInGivenLine(scale, edges[endIndex].angle, edges[endIndex][property], edges[endIndex+1].angle, edges[endIndex+1][property]);
                                endEdge.angle = angle;

                                if(property == "scaleFloor") {
                                    endEdge.scaleFloor = getScaleInGivenLine(angle, edges[endIndex].angle, edges[endIndex]["scaleCeil"], edges[endIndex+1].angle, edges[endIndex+1]["scaleCeil"]);
                                }  else {
                                    result.endAngle = angle;
                                }

                                result.push(endEdge);
                            }

                            if(property == "scaleFloor") {
                                for(var i = endIndex+1; i<edges.length; ++i) {
                                    result.push(edges[i]);
                                }
                            }

                            return result;
                        }

                        function bisect(edges, ratio, edgePropertyType) {

                            var polygon = constructPolygonOfEdges(edges);
                            var polygonFloor = constructPolygonOfEdgesOfFloor(edges);
                            var polygonCeil = constructPolygonOfEdgesOfCeil(edges);
                            var isConcave = {
                                general: funcService.isConcave(polygon),
                                floor: funcService.isConcave(polygonFloor),
                                ceil: funcService.isConcave(polygonCeil)
                            };
//                        isConcave = true;   // set true to avoid convex case

                            if(edgePropertyType == "scale") {
                                ratio = 1 - ratio;
                            }

                            var min_max = getMinMax(edges, edgePropertyType, isConcave);
                            var left = min_max.min;
                            var right = min_max.max;

                            if(left > right) {
                                return null;
                            }

                            var totalArea = funcService.calculateArea(polygon);

                            var bisectResult_left = calculateBisectResult(edges, left, totalArea, edgePropertyType);
                            var bisectResult_right = calculateBisectResult(edges, right, totalArea, edgePropertyType);

                            if(isFound(bisectResult_left, ratio)) {
                                return bisectResult_left;
                            }

                            if(isFound(bisectResult_right, ratio)) {
                                return bisectResult_right;
                            }

                            if((ratio-bisectResult_left.ratio)*(ratio-bisectResult_right.ratio) > 0) {
                                return null;
                            }

                            do {
                                var m = (left+right)/2;

                                var biserctResult = calculateBisectResult(edges, m, totalArea, edgePropertyType);

                                if(isFound(biserctResult, ratio)) {
                                    return biserctResult;
                                }

                                if(biserctResult.ratio < ratio) {
                                    left = m;
                                } else {
                                    right = m;
                                }

                            } while(true);


                            function getMinMax(edges, edgePropertyType, isConcave) {

                                var min = 1e9;
                                var max = -1e9;

                                var edgeP_1 = edgePropertyType;
                                var edgeP_2 = edgePropertyType;

                                if(edgePropertyType == "scale") {
                                    if(isConcave.floor) {
                                        min = -1e9;
                                    }
                                    if(isConcave.ceil) {
                                        max = 1e9;
                                    }
                                }

                                if(edgePropertyType == "scale") {
                                    edgeP_1 = edgePropertyType + "Floor";
                                    edgeP_2 = edgePropertyType + "Ceil";
                                }

                                for(var i = 0; i < edges.length; ++i) {
                                    if(edgePropertyType == "scale" && isConcave.floor) {
                                        if (edges[i][edgeP_1] > min) {
                                            min = edges[i][edgeP_1];
                                        }
                                    } else {
                                        if (edges[i][edgeP_1] < min) {
                                            min = edges[i][edgeP_1];
                                        }
                                    }

                                    if(edgePropertyType == "scale" && isConcave.ceil) {
                                        if(edges[i][edgeP_2] < max) {
                                            max = edges[i][edgeP_2];
                                        }
                                    } else {
                                        if(edges[i][edgeP_2] > max) {
                                            max = edges[i][edgeP_2];
                                        }
                                    }
                                }

                                return {
                                    min: min,
                                    max: max
                                }
                            }

                            function calculateBisectResult(edges, m, totalArea, edgePropertyType) {

                                if(edgePropertyType == "angle") {
                                    return calculateBisectResult_vertical(edges, m, totalArea);
                                }

                                if(edgePropertyType == "scale") {
                                    return calculateBisectResult_horizontal(edges, m, totalArea);
                                }
                            }

                            function calculateBisectResult_vertical(edges, m, totalArea) {
                                var edge = {
                                    angle: m,
                                    scaleFloor: 0,
                                    scaleCeil: 0
                                };
                                if(m > 360) {
                                    edge.angle = edge.angle - 360;
                                }

                                var index = -1;
                                for(var i = 0; i < edges.length-1; ++i) {
                                    index = i;

                                    if(edges[i].angle<=m && edges[i+1].angle>=m) {
                                        break;
                                    }
                                }

                                var alpha = edges[index].angle;
                                var beta = edges[index+1].angle;
                                var gama = m;

                                edge.scaleFloor = getScaleInGivenLine(gama, alpha, edges[index].scaleFloor, beta, edges[index+1].scaleFloor);
                                edge.scaleCeil = getScaleInGivenLine(gama, alpha, edges[index].scaleCeil, beta, edges[index+1].scaleCeil);


                                var leftEdges = calculateLeftEdges(edges, index, edge);
                                var polygon = constructPolygonOfEdges(leftEdges);

                                var leftArea = funcService.calculateArea(polygon);

                                var _ratio = leftArea/totalArea;

                                return {
                                    ratio: _ratio,
                                    edge: edge,
                                    edgeIndex: index
                                }
                            }

                            function calculateBisectResult_horizontal(edges, m, totalArea) {
                                var scale = m;

                                var property = "scaleCeil";
                                if(m < edges[0].scaleFloor || m < edges[edges.length-1].scaleFloor) {
                                    property = "scaleFloor";
                                }

                                for(var startIndex = 0; startIndex < edges.length; ++startIndex) {
                                    if(edges[startIndex].scaleFloor <= m && edges[startIndex].scaleCeil >=m) {
                                        break ;
                                    }
                                }

                                for(var endIndex = edges.length-1; endIndex >= 0; --endIndex) {
                                    if(edges[endIndex].scaleFloor <= m && edges[endIndex].scaleCeil >=m) {
                                        break ;
                                    }
                                }

                                var downEdges = calculateDownEdges(edges, scale, property, startIndex, endIndex);
                                var polygon = constructPolygonOfEdges(downEdges);
                                var downArea = funcService.calculateArea(polygon);

                                var _ratio = downArea/totalArea;

                                return {
                                    ratio: _ratio,
                                    scale: scale,
                                    property: property,
                                    startIndex: startIndex,
                                    endIndex: endIndex
                                }
                            }

                            function isFound(biserctResult, ratio) {
                                var e = 1e-6;

                                if(Math.abs(biserctResult.ratio-ratio)<e) {
                                    return true;
                                }

                                return false;

                            }
                        }
                    }

                    function drawTreemap(node) {

                        var polygon = constructPolygonOfEdges(node.edges);

                        var fill = "none";
                        var opa = 1;


                        if(node.elements.length == 1) {
                            fill = "green";
                            opa = node.elements[0].distance;

                            var fillColor = d3.scale.linear()
                                .range(['rgb(247,252,245)','rgb(229,245,224)','rgb(199,233,192)','rgb(161,217,155)','rgb(116,196,118)','rgb(65,171,93)','rgb(65,171,93)'])
                                .domain([0.2,0.05*maxDistance,0.1*maxDistance,0.5*maxDistance,0.7*maxDistance, 0.9*maxDistance, maxDistance]);

                            drawLegend();

                            var nid = node.elements[0].nid;
                            var nodeData = mapService.data.map.source[nid];

                            var _marker = null;

                            radialGraph_g
                                .append("path")
                                .attr("class", "treemap_node_"+nid)
                                .attr("d", funcService.polygon(polygon))
                                .attr("fill", fillColor((opa)))
                                .attr("fill-opacity", 1)
                                .attr("stroke", "black")
                                .attr("stroke-width", 0.3)
                                .attr("stroke-opacity", 0.8)
                                .on("mouseover", function() {
                                    d3.selectAll(".treemap_node_"+nid)
                                        .attr("stroke", "red")
                                        .attr("stroke-width", 2.5)
                                        .attr("stroke-opacity", 0.7);

                                    if(selected_nid == -1) {
                                        mapService.map.source.setView([nodeData.gps.lat, nodeData.gps.lng]);
                                    }

//                                    if(mapService.data.d3Overlay.sel.source) {
//                                        mapService.data.d3Overlay.sel.source
//                                            .selectAll("circle")
//                                            .attr("display", "none");
//                                    }

                                    _marker = mapService.map.source.addGreenMarker(nodeData.gps.lat, nodeData.gps.lng);
                                })
                                .on("mouseout", function() {

                                    if(_marker != null) {
                                        mapService.map.source.removeLayer(_marker);
                                        _marker = null;
                                    }

                                    if(selected_nid != nid) {
                                        d3.selectAll(".treemap_node_" + nid)
                                            .attr("stroke", "black")
                                            .attr("stroke-width", 1)
                                            .attr("stroke-opacity", 0.3);
                                    }

//                                    if(mapService.data.d3Overlay.sel.source) {
//                                        mapService.data.d3Overlay.sel.source
//                                            .selectAll("circle")
//                                            .attr("display", "");
//                                    }

                                })
                                .on("click", function() {
                                    if(marker != null) {
                                        mapService.map.source.removeLayer(marker);
                                        marker = null;

                                        d3.selectAll(".treemap_node_"+selected_nid)
                                            .attr("stroke", "black")
                                            .attr("stroke-width", 1)
                                            .attr("stroke-opacity", 0.3);
                                    }

                                    if(selected_nid == nid) {
                                        selected_nid = -1;

                                        return ;
                                    }

                                    if(mapService.map.source) {
                                        mapService.map.source.setView([nodeData.gps.lat, nodeData.gps.lng]);
                                    }

                                    selected_nid = nid;
                                    marker = _marker;
                                    _marker = null;
                                });
                        }

                        if(node.elements.length > 1) {
                            drawTreemap(node.children.left);
                            drawTreemap(node.children.right);
                        }
                    }

                    function constructPolygonOfEdges(edges) {
                        var polygon = [];
                        for(var i = 0; i < edges.length; ++i) {
                            polygon.push(getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleFloor)));
                        }
                        for(var i = edges.length-1; i >= 0; --i) {
                            polygon.push(getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleCeil)));
                        }

                        return polygon;
                    }

                    function constructPolygonOfEdgesOfFloor(edges) {
                        var polygon = [];
                        for(var i = 0; i < edges.length; ++i) {
                            polygon.push(getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleFloor)));
                        }

                        return polygon;
                    }

                    function constructPolygonOfEdgesOfCeil(edges) {
                        var polygon = [];
                        for(var i = edges.length-1; i >= 0; --i) {
                            polygon.push(getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleCeil)));
                        }

                        return polygon;
                    }

                    function scaleToLength(scale) {
                        var len = 0;
                        if(maxScale > 0) {
                            len = scale / maxScale * maxLength;
                        }

                        return len;
                    }

                    function getPoint(_radian, len) {
                        var radian = _radian - Math.PI/2;

                        var point = [len*Math.cos(radian), len*Math.sin(radian)];
                        return point;
                    }

                    function getPoint_angle(_angle, len) {
                        var angle = _angle - 90;

                        var radian = angle/180*Math.PI;

                        var point = [len*Math.cos(radian), len*Math.sin(radian)];
                        return point;
                    }

                    function drawClockDial() {
                        var radius = scaleToLength(maxScale) + 5;
                        var outer_circle = radialGraph_g.append("circle")
                            .attr("r", function () {
                                return radius;
                            })
                            .attr("cx", 0)
                            .attr("cy", 0)
                            .attr("fill", "none")
                            .attr("stroke", "black")
                            .attr("stroke-width", 1)
                            .attr("stroke-opacity", 1);

                        var outer_circle2 = radialGraph_g.append("circle")
                            .attr("r", function () {
                                return radius + 7;
                            })
                            .attr("cx", 0)
                            .attr("cy", 0)
                            .attr("fill", "none")
                            .attr("stroke", "dodgerblue")
                            .attr("stroke-width", 5)
                            .attr("stroke-opacity", 1);

                        var step = 360 * hoursPerSector / (endTime - startTime + 1);
                        var i = 0;
                        while (i * hoursPerSector + startTime <= endTime) {
                            var point = getPoint_angle(step * i, radius);
                            radialGraph_g.append("line")
                                .attr("x1", 0)
                                .attr("y1", 0)
                                .attr("x2", point[0])
                                .attr("y2", point[1])
                                .attr("stroke", "#f46d43")
                                .attr("stroke-width", 2)
                                .attr("stroke-opacity", 0.8)
                                .style("stroke-dasharray", 3);

                            point = getPoint_angle(step * i, radius + 22);
                            radialGraph_g.append("text")
                                .attr("transform", "translate(" + (point[0] - 6) + "," + (point[1] + 6) + ")")
                                .text(function () {
                                    return i * hoursPerSector + startTime;
                                }).style("font-weight", "bold");

                            i = i + 1;
                        }

                        radialGraph_g.append("text")
                            .text("Visitors: " + totalPersonsOfGraph)
                            .attr("transform", "translate(100, -180)");
                    }
                }
            }
        }
    }
}]);