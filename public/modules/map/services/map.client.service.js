'use strict';

angular.module('map').factory('mapService',
    function() {
        var mapService = {
            map: {},
            destCircleSizeIndex: 'regionNum'
        };

        return mapService;
    }
);