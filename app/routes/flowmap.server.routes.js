'use strict';

module.exports = function(app) {
    var flowMap = require('../../app/controllers/flowmap.server.controller');

    // handle flowmap route with date
    app.route('/flowmap').post(flowMap.getFlowMapData);
};