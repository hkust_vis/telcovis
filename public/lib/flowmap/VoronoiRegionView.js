/**
 * Created by Jiayi Xu on 12/1/15.
 */

// render voronoi map
function VoronoiRegionView(cid, nodes, weights, className, pos, size, voronoiIteration, container, mainGraph, flowMapService, funcService) {
    var width = size[0];
    var height = size[1];

    var offset = [pos[0], pos[1]];

    var graphGroup = container.append('g')
        .attr("class", className);

    setTimeBar(offset);
    var timeBar = {
        drawTimeBar: drawTimeBar
    };
    flowMapService.data.timeBar.push(timeBar);

    var voronoiView = graphGroup.append('g');

    if(nodes.length == 0) {
        return voronoiView;
    }

    var polygons = getCartogram(nodes, weights);

    var oScale = d3.scale.linear()
        .domain([0, 3, 10, 30])
        .range([0, 1, 2, 3]);

    var voronoiPolygons = voronoiView.append('g')
        .selectAll("path")
        .data(polygons)
        .enter()
        .append("path")
        .attr('class', 'voronoi_view_region')
        .attr("transform", "translate(" + offset[0] + ", " + offset[1] +")")
        .attr("d", funcService.polygon)
        .attr("fill", function(d, i) {
            var _i = oScale(nodes[i].distance_M);
            return flowMapService.data.clusterColorsSequential[cid][Math.floor(_i)];
        })
        .attr("stroke", "grey")
        .attr("stroke-width", 1);

    for(var i = 0; i < nodes.length; ++i) {
        nodes[i].pX = nodes[i].X + offset[0];
        nodes[i].pY = nodes[i].Y + offset[1];
    }

    // set time bar positions
    function setTimeBar(offset) {
        var length = height / 1.5;
        var angle = Math.PI/3;
        var timeBarHeight = (1-Math.cos(angle))/Math.sin(angle)*length;
        var timeBarWidth = height / 2 * 0.2;
        var timeBarPos = {X: offset[0] + width/2, Y: offset[1]+80};
        flowMapService.data.timeBarPos.push(timeBarPos);

        var cornerHeight = Math.cos(angle)*timeBarHeight/(1-Math.cos(angle));

        var rHeight = height / 2 * 0.25;

        flowMapService.timePos = function(timeBarPos, hour) {

            var _angle = getTimeAngle(hour);
            var cornersPos = [
                {X: (timeBarPos.X-Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)},
                {X: (timeBarPos.X+Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)}
            ];
            var centerPos = timeBarPos;
            var radius = timeBarHeight + timeBarWidth/2;
            if(hour < 6) {
                centerPos = cornersPos[0];
                radius = cornerHeight - timeBarWidth/2;
            } else if(hour > 18) {
                centerPos = cornersPos[1];
                radius = cornerHeight - timeBarWidth/2;
            }

            var pos = {X: centerPos.X+radius*Math.cos(_angle), Y: centerPos.Y+radius*Math.sin(_angle)};

            return pos;
        };


        flowMapService.timePos3 = function(timeBarPos, hour) {

            var _angle = getTimeAngle(hour);
            var cornersPos = [
                {X: (timeBarPos.X-Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)},
                {X: (timeBarPos.X+Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)}
            ];
            var centerPos = timeBarPos;
            var radius = timeBarHeight-timeBarWidth/2+rHeight;
            if(hour < 6) {
                centerPos = cornersPos[0];
                radius = cornerHeight + timeBarWidth/2 - rHeight;
            } else if(hour > 18) {
                centerPos = cornersPos[1];
                radius = cornerHeight + timeBarWidth/2 - rHeight;
            }

            var pos = {X: centerPos.X+radius*Math.cos(_angle), Y: centerPos.Y+radius*Math.sin(_angle)};

            return pos;
        };

        offset[1] = offset[1] + height / 2 * 1.3 + 80;
    }

    // get angles of certain hour in time bar
    function getTimeAngle(hour) {
        var tAngle;
        var angle = Math.PI/3;
        var _angle = Math.PI/2-angle;
        if(hour < 6) {
            tAngle = Math.PI/2 - angle/6*hour;
        } else if(hour>=6 && hour<=18) {
            tAngle = Math.PI+_angle+2*angle/12*(hour-6);
        } else if(hour > 18) {
            tAngle = Math.PI/2+angle-angle/6*(hour-18);
        }

        return tAngle;
    }

    // draw time bars
    function drawTimeBar() {
        var graphGroup = mainGraph.append('g')
            .attr("class", "time_bar")
            .style("isolation", "isolate");

        var length = height / 1.5;
        var angle = Math.PI/3;
        var timeBarHeight = (1-Math.cos(angle))/Math.sin(angle)*length;
        var timeBarWidth = height / 2 * 0.2;
        var timeBarPos = flowMapService.data.timeBarPos[cid];

        var cornerHeight = Math.cos(angle)*timeBarHeight/(1-Math.cos(angle));
        var rHeight = height / 2 * 0.25;

        var cornersPos = [
            {X: (timeBarPos.X-Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)},
            {X: (timeBarPos.X+Math.sin(angle)*(timeBarHeight+cornerHeight)), Y: (timeBarPos.Y-cornerHeight)}
        ];

        var e = 0.01;   // eliminate gap in the corner

        graphGroup
            .append('path')
            .attr("transform", "translate(" + timeBarPos.X + ", " + timeBarPos.Y +")")
            .attr("fill", "grey")
            .attr('d', d3.svg.arc().innerRadius(timeBarHeight-timeBarWidth/2).outerRadius(timeBarHeight+timeBarWidth/2).startAngle(-angle).endAngle(angle));

        graphGroup
            .append('path')
            .attr("transform", "translate(" + cornersPos[0].X + ", " + cornersPos[0].Y +")")
            .attr("fill", "grey")
            .attr('d', d3.svg.arc().innerRadius(cornerHeight-timeBarWidth/2).outerRadius(cornerHeight+timeBarWidth/2).startAngle(Math.PI-angle-e).endAngle(Math.PI));

        graphGroup
            .append('path')
            .attr("transform", "translate(" + cornersPos[1].X  + ", " + cornersPos[1].Y +")")
            .attr("fill", "grey")
            .attr('d', d3.svg.arc().innerRadius(cornerHeight-timeBarWidth/2).outerRadius(cornerHeight+timeBarWidth/2).startAngle(Math.PI).endAngle(Math.PI+angle+e));

        for(var i = 0; i < 9; ++i) {
            var hour = i*3;

            var _pos = flowMapService.timePos3(timeBarPos, hour);

            var _angle = getTimeAngle(hour);
            var rWidth = 3;

            if(hour < 6) {
                _angle = _angle-Math.PI/2;
            } else if(hour>=6 && hour<=18) {
                _angle = _angle+Math.PI/2;
            } else if(hour > 18) {
                _angle = _angle-Math.PI/2;
            }

            _angle = _angle/Math.PI*180;

            graphGroup.append('rect')
                .attr("transform", "translate(" + (_pos.X-rWidth/2) + ", " + _pos.Y +"), rotate(" + _angle + " " + rWidth/2 + "," +0+ ")")
                .attr("fill", "black")
                .attr("width", rWidth)
                .attr("height", rHeight);
        }
    }

    // get cartogram
    function getCartogram(nodes, weights) {
        var voronoi = d3.geom.voronoi()
            .x(function(d) {
                return d.X;
            })
            .y(function(d) {
                return d.Y;
            })
            .clipExtent([[0, 0], [width, height]]);
        var polygons = voronoi(nodes);

        var vertexes = getVertexesOfPolygons(polygons);
        markOuterVertexes(vertexes);

        var initTotalWeight = 0;
        for(var i = 0; i < weights.length; ++i) {
            initTotalWeight = initTotalWeight + (weights[i]+1); // weight = weight+1 to avoid 0 problem
        }
        var initWeightRatio = [];
        for(var i = 0; i < weights.length; ++i) {
            initWeightRatio.push((weights[i]+1)/initTotalWeight); // weight = weight+1 to avoid 0 problem
        }

        setCentroids(nodes, polygons);

        var iterationTimes = voronoiIteration;
        for (var i = 0; i < iterationTimes/2; ++i) {
            if (!adjustPolygons(nodes, polygons, vertexes, initWeightRatio)) {
                console.log("iteration times are " + i);
                break;
            }
        }

        addFramePointsToOuterPolygons(polygons);
        var vertexes = getVertexesOfPolygons(polygons);

        var maxMove = 1024;
        for(var k = 0; k < 20; ++k) {
            var _iterationTimes = iterationTimes/80;
            for (var i = 0; i < _iterationTimes; ++i) {
                if (!adjustPolygons(nodes, polygons, vertexes, initWeightRatio)) {
                    console.log("iteration times are " + i);
                    break;
                }
            }
            _iterationTimes = iterationTimes/80;
            for (var i = 0; i < _iterationTimes; ++i) {
                if (!adjustPolygons(nodes, polygons, vertexes, initWeightRatio, maxMove)) {
                    console.log("iteration times are " + i);
                    break;
                }
            }

            maxMove = maxMove / 2;
        }

        setVertexesInBound(vertexes);
        setCentroids(nodes, polygons);

        return polygons;
    }

    // get vertexes of polygons
    function getVertexesOfPolygons(polygons) {
        var vertexes = [];
        var e = 1e-6;

        for(var i = 0; i < polygons.length; ++i) {
            var polygon = polygons[i];
            for(var j = 0; j < polygon.length; ++j) {
                var indicator = true;
                for(var k = 0; k < vertexes.length; ++k) {
                    if(funcService.getDistance(polygon[j], vertexes[k]) < e) {
                        polygon[j] = vertexes[k];
                        indicator = false;
                        break;
                    }
                }
                if(indicator) {
                    addVertex(polygon[j], vertexes, false);
                }
            }
        }


        var isExpandAtFirst = true;
        if(isExpandAtFirst) {
            var e = 5;
            for (var i = 0; i < vertexes.length; ++i) {
                if (vertexes[i][0] == 0) {
                    vertexes[i][0] = vertexes[i][0] - width / e;
                }

                if (vertexes[i][0] == width) {
                    vertexes[i][0] = vertexes[i][0] + width / e;
                }

                if (vertexes[i][1] == 0) {
                    vertexes[i][1] = vertexes[i][1] - height / e;
                }

                if (vertexes[i][1] == height) {
                    vertexes[i][1] = vertexes[i][1] + height / e;
                }
            }
        }

        return vertexes;
    }

    // add vertex to polygon
    function addVertex(vertex, vertexes, isPined) {
        vertexes.push(vertex);
        vertex.vid = vertexes.length-1;
        vertex.isPined = isPined;
    }

    // mark which are outer vertexes
    function markOuterVertexes(vertexes) {
        for(var i = 0; i < vertexes.length; ++i) {
            var vertex = vertexes[i];
            vertex.isOnOuterFrame = false;
            if(isOnOuterFrame(vertex)) {
                vertex.isOnOuterFrame = true;
            }
        }

    }

    // add frame points to outer polygons
    function addFramePointsToOuterPolygons(polygons) {
        var n = polygons.length;

        for(var i = 0; i < n; ++i) {
            var polygon = polygons[i];
            var _polygon = [];

            for (var j = 0; j < polygon.length; ++j) {
                var p1, p2;

                if (j == 0) {
                    p1 = polygon[polygon.length - 1];
                } else {
                    p1 = polygon[j - 1];
                }

                p2 = polygon[j];

                if (p1.isOnOuterFrame && p2.isOnOuterFrame) {
                    var newP = funcService.getCenterPoint(p1, p2);
                    newP.isOnOuterFrame = true;

                    _polygon.push(newP);
                }
                _polygon.push(polygon[j]);
            }

            polygons[i] = _polygon;
        }
    }

    // is on outer frame
    function isOnOuterFrame(p) {
        if((p[0] == 0) || (p[0] == width) || (p[1] == 0) || (p[1] == height)){
            return true;
        }
        return false;
    }

    // adjust polygons
    function adjustPolygons(nodes, polygons, vertexes, weightRatio, maxMove) {

        if(maxMove == undefined) {
            maxMove = 1e9;
        }

        var num = nodes.length;

        var areas = calculateAreas(polygons);

        var totalArea = 0;
        for (var i = 0; i < areas.length; ++i) {
            totalArea = totalArea + areas[i];
        }

        var areaRatio = [];
        for (var i = 0; i < areas.length; ++i) {
            areaRatio.push(areas[i] / totalArea);
        }

        var polygonAttrs = [];
        var meanSizeError = 0;

        for(var i = 0; i < num; ++i) {
            polygonAttrs.push({});

            polygonAttrs[i]["desired"] = totalArea*weightRatio[i];
            polygonAttrs[i]["area"] = areas[i];
            polygonAttrs[i]["radius"] = Math.sqrt(Math.abs(areas[i]) / Math.PI);
            polygonAttrs[i]["desired_radius"] = Math.sqrt(polygonAttrs[i]["desired"] / Math.PI);
            polygonAttrs[i]["mass"] = polygonAttrs[i]["desired_radius"] - polygonAttrs[i]["radius"];

            polygonAttrs[i]["sizeError"] = 1e6;
            if(areas[i] > 0 && polygonAttrs[i]["desired"] > 0) {
                polygonAttrs[i]["sizeError"] = Math.max(areas[i], polygonAttrs[i]["desired"]) / Math.min(areas[i], polygonAttrs[i]["desired"]);
            }

            meanSizeError = meanSizeError + polygonAttrs[i]["sizeError"];
        }
        meanSizeError = meanSizeError / num;
        var forceReductionFactor = 1/(1 + meanSizeError);

        var vectors = [];
        var maxLen = 0;

        for(var i = 0; i < vertexes.length; ++i) {
            var vector = [0, 0];

            if(vertexes[i].isPined) {
                vectors.push(vector);
                continue ;
            }

            var affectedPolygons = [];

            for(var j = 0; j < num; ++j) {
                var polygon = polygons[j];
                var isPart = false; // is the vectex part of this polygon
                for(var k = 0; k < polygon.length; ++k) {
                    if(polygon[k].vid == i) {
                        isPart = true;
                        break;
                    }
                }

                var dis = funcService.getDistance(vertexes[i], [nodes[j].X, nodes[j].Y]);
                var mass = polygonAttrs[j]["mass"];// * Math.sqrt(polygonAttrs[j]["sizeError"]);
//                var _mass = polygonAttrs[j]["desired"] - dis;
//                if(Math.abs(_mass) > Math.abs(mass)) {
//                    mass = _mass;
//                }

                // if areas[i] == 0, then this polygon needs to adjust, it is "broken", so did not affect other points
                if(polygonAttrs[j]["radius"] == 0) {
                    mass = 0;
                }
                var radius = polygonAttrs[j]["radius"];

                var f = 0;
                if(dis != 0) {
//                    f = mass * (radius / dis);
                    f = mass * (1 / dis);
                }

                if(!isPart) {
//                    f = f / 50;
                    f = 0;
                }


//                var f = 0;
//                if(dis > radius) {
//                    f = mass * (radius / dis); // equal to mass * cos(thita), the small area will converge to one point
//                } else {
//                    f = mass * (Math.pow(dis,2)/Math.pow(radius,2)) * (4 - 3*(dis/radius));
//                }

                var pVertexID = -1; // whether this vertex belongs to this polygon
                for (var k = 0; k < polygon.length; ++k) {
                    var pVertex = polygon[k];
                    if (pVertex.vid == i) {
                        pVertexID = k;

                        break ;
                    }
                }


                if(pVertexID > -1) {

                    affectedPolygons.push({
                        pid: j,
                        pVertexID: pVertexID
                    });
                }

                // if this vertex belongs to this polygon, make sure f > 0 could enlarge the polygon and f < 0 could narrow the polygon.
                if(pVertexID > -1 && f != 0) {
                    var _polygon = funcService.objectClone(polygon);

                    var pVertex = _polygon[pVertexID];
                    pVertex[0] = pVertex[0] + (vertexes[i][0] - nodes[j].X) * f * forceReductionFactor;
                    pVertex[1] = pVertex[1] + (vertexes[i][1] - nodes[j].Y) * f * forceReductionFactor;

                    var _area = calculateArea(_polygon);
                    var area = calculateArea(polygon);

                    if (((f > 0) && (_area < area)) || ((f < 0) && (_area > area))) {   // not satisfied, let f = 0
                        f = 0;
                    }
                }

                vector[0] = vector[0] + (vertexes[i][0] - nodes[j].X)*f;
                vector[1] = vector[1] + (vertexes[i][1] - nodes[j].Y)*f;
            }
            vector[0] = vector[0]*forceReductionFactor;
            vector[1] = vector[1]*forceReductionFactor;

            if(vector[0] != 0 && vector[1] != 0) {
                var dis = funcService.getDistance([0,0], vector);
                if(dis > maxMove) {
                    vector[0] = vector[0] / dis * maxMove;
                    vector[1] = vector[1] / dis * maxMove;
                }
            }

            if(vector[0] != 0 && vector[1] != 0) {

                for (var j = 0; j < affectedPolygons.length; ++j) {
                    var e = 1.05;

                    var pid = affectedPolygons[j].pid;
                    var pVertexID = affectedPolygons[j].pVertexID;
                    var polygon = polygons[pid];

                    var _polygon = funcService.objectClone(polygon);


                    var pVertex = _polygon[pVertexID];
                    pVertex[0] = pVertex[0] + vector[0];
                    pVertex[1] = pVertex[1] + vector[1];

                    var _area = calculateArea(_polygon);
                    if (_area == 0 || (polygonAttrs[pid]["desired"] / _area > e && _area < polygonAttrs[pid]["area"])) { // control small region will not diminish
                        vector[0] = 0;
                        vector[1] = 0;

                        break;
                    }
                    else {    // control ratio of width/height
                        var dis = funcService.getDistance([nodes.X, nodes.Y], polygon[pVertexID]);
                        var ratio = 0;
                        if (dis == 0) {
                            continue;
                        }
                        ratio = polygonAttrs[pid]["radius"] / dis;

                        var _centroid = calculateCentroid(_polygon);
                        var _radius = Math.sqrt(_area / Math.PI);
                        var _dis = funcService.getDistance(_centroid, pVertex);
                        if (_dis == 0) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break;
                        }
                        var _ratio = _radius / _dis;


                        if (_ratio > e && ratio > e && _ratio > ratio) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break;
                        }

                        if (_ratio < 1 / e && ratio < 1 / e && _ratio < ratio) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break;
                        }
                    }



                    var _n = _polygon.length;   // not allow concave polygon

                    for (var k = pVertexID - 1; k <= pVertexID + 1; ++k) {

                        var indexList = [k - 1, k, k + 1];
                        var pList = [];

                        for (var L = 0; L < 3; ++L) {
                            if (indexList[L] < 0) {
                                indexList[L] = indexList[L] + _n;
                            }

                            if (indexList[L] >= _n) {
                                indexList[L] = indexList[L] - _n;
                            }

                            pList.push([_polygon[indexList[L]][0], -1 * _polygon[indexList[L]][1]])
                        }

                        var result = crossMulti(pList[0], pList[1], pList[2]);
                        if (result < 0) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break;
                        }
                    }


                    if (vector[0] == 0 && vector[1] == 0) {
                        break;
                    }
                }
            }

            if(vector[0] != 0 || vector[1] !=0) {

                var oldVertex = vertexes[i];
                var newVertex = funcService.objectClone(oldVertex);
                newVertex[0] = newVertex[0] + vector[0];
                newVertex[1] = newVertex[1] + vector[1];

                for (var j = 0; j < polygons.length; ++j) {     // vertex could not step over edge of each polygon
                    var polygon = polygons[j];
                    for (var k = 0; k < polygon.length; ++k) {
                        var k1 = k;
                        var k2 = k1 + 1;
                        if (k2 >= polygon.length) {
                            k2 = k2 - polygon.length;
                        }

                        var v1 = polygon[k1];
                        var v2 = polygon[k2];


                        if (v1.vid == i || v2.vid == i) {
                            continue;
                        }

                        var oldCrossResult = crossMulti(oldVertex, v1, v2);
                        var newCrossResult = crossMulti(newVertex, v1, v2);

                        if((oldCrossResult >= 0 && newCrossResult <= 0) || (oldCrossResult <= 0 && newCrossResult >= 0)) {
                            var v1CrossResult = crossMulti(v1, oldVertex, newVertex);
                            var v2CrossResult = crossMulti(v2, oldVertex, newVertex);

                            if((v1CrossResult >= 0 && v2CrossResult <= 0) || (v1CrossResult <= 0 && v2CrossResult >= 0)) {
                                vector[0] = 0;
                                vector[1] = 0;

                                break ;
                            }
                        }

                    }
                    if(vector[0] == 0 && vector[1] == 0) {
                        break;
                    }
                }

                // other vertexes could not step over edge linked to this vertex
                var affectedVertexes = [];
                var affectedVertexesMap = {};
                affectedVertexesMap[i] = true;
                for(var j = 0; j < affectedPolygons.length; ++j) {
                    var aPolygon = affectedPolygons[j];
                    var pid = aPolygon.pid;
                    var polygon = polygons[pid];
                    var pVertexID = aPolygon.pVertexID;

                    var v;
                    var vid = pVertexID - 1;
                    if(vid < 0) {
                        vid = vid + polygon.length;
                    }
                    vid = polygon[vid].vid;
                    if(!affectedVertexesMap.hasOwnProperty(vid)) {
                        v = vertexes[vid];
                        affectedVertexes.push(v);
                        affectedVertexesMap[vid] = true;
                    }

                    vid = pVertexID + 1;
                    if(vid >= polygon.length) {
                        vid = vid - polygon.length;
                    }
                    vid = polygon[vid].vid;
                    if(!affectedVertexesMap.hasOwnProperty(vid)) {
                        v = vertexes[vid];
                        affectedVertexes.push(v);
                        affectedVertexesMap[vid] = true;
                    }
                }

                for(var j = 0; j < affectedVertexes.length; ++j) {
                    var aV = affectedVertexes[j];

                    for(var k = 0; k < vertexes.length; ++k) {
                        var v = vertexes[k];
                        if(v.vid == i || v.vid == aV.vid) {
                            continue ;
                        }

                        var crossResult1 = crossMulti(v, oldVertex, aV);
                        var crossResult2 = crossMulti(v, aV, newVertex);
                        var crossResult3 = crossMulti(v, newVertex, oldVertex);

                        if((crossResult1 >= 0 && crossResult2 >= 0 && crossResult3 > 0) || (crossResult1 <= 0 && crossResult2 <= 0 && crossResult3 <= 0)) {
                            vector[0] = 0;
                            vector[1] = 0;

                            break ;
                        }

                    }
                }

                var len = funcService.getDistance([0,0], vector);
                if(len > maxLen) {
                    maxLen = len;
                }
            }

            vectors.push(vector);
        }

        var flag = false;
        var max = 0;
        var maxI = -1;


        for(var i = 0; i < vertexes.length; ++i) {
            var vector = vectors[i];

            if(vector[0] != 0 && vector[1] != 0) {

                var _len = funcService.getDistance([0,0], vector);

                if(_len > max) {
                    max = _len;
                    maxI = i;
                }

            }
        }


        if(maxI > -1) { // only move one vertex point one time
            flag = true;

            var vector = vectors[maxI];
            vertexes[maxI][0] = vertexes[maxI][0] + vector[0];
            vertexes[maxI][1] = vertexes[maxI][1] + vector[1];
        }

        setCentroids(nodes, polygons);

        return flag;
    }

    // calculate centroid of new polygons
    function setCentroids(nodes, polygons) {
        var num = nodes.length;

        for(var i = 0; i < num; ++i) {
            var polygon = polygons[i];
            var centroid = calculateCentroid(polygon);
            nodes[i].X = centroid[0];
            nodes[i].Y = centroid[1];
        }
    }

    // calculate centroid of polygon
    function calculateCentroid(polygon) {
        var centroid = [0, 0];
        var vertexNum = polygon.length;
        for(var j = 0; j < vertexNum; ++j) {
            centroid[0] = centroid[0] + polygon[j][0];
            centroid[1] = centroid[1] + polygon[j][1];
        }
        centroid[0] = centroid[0] / vertexNum;
        centroid[1] = centroid[1] / vertexNum;

        return centroid;
    }

    // calculate areas of polygons
    function calculateAreas(polygons) {
        var areas = [];
        for(var i = 0; i < polygons.length; ++i) {
            var area = calculateArea(polygons[i]);
            areas.push(area);
        }

        return areas;
    }

    // calculate area of polygons
    function calculateArea(polygon) {
        var area = 0;
        var n = polygon.length;

        for(var i = 0; i < polygon.length; ++i) {
            var i2 = i;
            var i3 = i+1;
            if(i3>=n){
                i3 = i3 - n;
            }
            var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
            var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

            var _area = p2[0]*p3[1] - p3[0]*p2[1];
            _area = _area * 0.5;

            area = area + _area;
        }

        if(area == 0) {
            console.log("polygon is a line!!!");
        }

        if(area < 0) {
//            console.log("polygon is concave!!!");

            area = 0;
        }

        return area;
    }

    // cross multiply
    function crossMulti(p1, p2, p3) {
        return (p1[0]*p2[1]+p2[0]*p3[1]+p3[0]*p1[1]) - (p1[0]*p3[1]+p2[0]*p1[1]+p3[0]*p2[1]);
    }

    // set vertexes in bound
    function setVertexesInBound(vertexes) {
        var minX = 1e6;
        var minY = 1e6;
        var maxX = -1e6;
        var maxY = -1e6;

        for(var i = 0; i < vertexes.length; ++i) {
            var _pos = vertexes[i];

            if (_pos[0] < minX) {
                minX = _pos[0];
            }
            if (_pos[0] > maxX) {
                maxX = _pos[0];
            }
            if (_pos[1] < minY) {
                minY = _pos[1];
            }
            if (_pos[1] > maxY) {
                maxY = _pos[1];
            }
        }

        var x_scale = d3.scale.linear()
            .domain([minX, maxX])
            .range([0, width]);

        var y_scale = d3.scale.linear()
            .domain([minY, maxY])
            .range([0, height]);

        for(var i = 0; i < vertexes.length; ++i) {
            if(vertexes[i].isPined) {
                continue ;
            }

            vertexes[i][0] = x_scale(vertexes[i][0]);
            vertexes[i][1] = y_scale(vertexes[i][1]);
        }
    }

    return voronoiView;
}