'use strict';

module.exports = function(app) {
    var lineUp = require('../../app/controllers/lineup.server.controller');

    // handle Lineup route with date
    app.route('/lineup/filter').post(lineUp.getLineUpDataFiltered);
};