'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Mixed = mongoose.Schema.Types.Mixed;

// map info data schema
var mapInfoSchema = new Schema({
    value: Mixed
});

// heatmap data schema
var heatMapSchema = new Schema({
    date: String,
    value: Mixed
});

// cooccurrence data schema of flow-out map
var cooccurrenceSourceSchema = new Schema({
    date: Number,
    value: Mixed,
    hour: Number,
    gpsID: Number
});

// cooccurrence data schema of flow-in map
var cooccurrenceDestSchema = new Schema({
    date: Number,
    value: Mixed,
    hour: Number,
    gpsID: Number,
    stationID: Number
});

mongoose.model('mapInfo', mapInfoSchema);
mongoose.model('heatMap', heatMapSchema);

mongoose.model('cooccurrenceSource', cooccurrenceSourceSchema);
mongoose.model('cooccurrenceDest', cooccurrenceDestSchema);