'use strict';

exports.index = function(req, res) {
    // render index
	res.render('index', {
		user: req.user || null,
		request: req
	});
};
