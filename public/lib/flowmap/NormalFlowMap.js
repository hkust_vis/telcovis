/**
 * Created by Jiayi on 29/3/15.
 */


// draw normal flowmap
function NormalFlowMap(flowData, nodes, width, height, flowWidth, container, flowMapService, funcService) {
    var flowMap = container.append('g')
        .attr("class", "normalFlowMap");

    var cluNum = flowMapService.data.clusterNum;

    var line = d3.svg.line()
        .x(function(d) {
            return d.X;
        })
        .y(function(d) {
            return d.Y;
        })
        .interpolate("cardinal");

    var pointAPos = flowMapService.data.pointA.pos;
    var pointBPos = flowMapService.data.pointB.pos;

    var volumesOfNodes = {

    };

    for(var i in nodes) {
        for(var j in nodes[i]) {
            var volume = {};
            volume = [0, 0];
            volumesOfNodes[nodes[i][j].nid] = volume;
        }
    }

    for(var i = 0; i < 2; ++i) {
        for(var j = 0; j < 24; ++j) {
            if(!flowData[i].hasOwnProperty(j)) {
                continue ;
            }

            for(var k = 0; k < cluNum; ++k) {
                var nodesNum = nodes[k].length;
                for(var L = 0; L < nodesNum; ++L) {
                    if (flowData[i][j].hasOwnProperty(nodes[k][L].nid)) {
                        volumesOfNodes[nodes[k][L].nid][i] += flowData[i][j][nodes[k][L].nid];
                    }
                }
            }
        }
    }

    var rootA = constructTree(0);
    var rootB = constructTree(1);

    var maxVolume = 0;
    for(var i = 0; i < cluNum; ++i) {
        maxVolume = Math.max(maxVolume, rootA.children[i].volume, rootB.children[i].volume);
    }

    drawTree(rootA, "SteelBlue");
    drawTree(rootB, "Crimson");

    // construct tree structure
    function constructTree(index) {
        var root = {
            volume: 0,
            children: []
        };
        if(index == 0) {
            root.pos = pointAPos;
        } else {
            root.pos = pointBPos;
        }

        for(var i = 0; i < cluNum; ++i) {
            var child = {
                interpolate_pos: funcService.objectClone(flowMapService.data.innerRatioBarCenterPos[i]),
                pos: funcService.objectClone(flowMapService.data.timeBarPos[i]),
                volume: 0,
                children: []
            };
            if(index == 0) {
                child.interpolate_pos.X = child.interpolate_pos.X - 30;
            } else {
                child.interpolate_pos.X = child.interpolate_pos.X + 30;
            }
            if(index == 0) {
                child.pos.X = child.pos.X - 50;
            } else {
                child.pos.X = child.pos.X + 50;
            }

            root.children.push(child);

            var nodesNum = nodes[i].length;
            for(var j = 0; j < nodesNum; ++j) {
                var node = nodes[i][j];

                var __child = {
                    pos: {X: node.pX, Y: node.pY},
                    volume: volumesOfNodes[node.nid][index],
                    children: undefined
                };
                child.children.push(__child);
            }
            child.children.sort(function(a, b){
                return a.pos.X - b.pos.X;
            });
        }

        setVolume(root);
        function setVolume(node) {
            if(node.children) {
                for(var i in node.children) {
                    node.volume = node.volume + setVolume(node.children[i]);
                }
            }

            return node.volume;
        }

        return root;
    }

    // draw tree
    function drawTree(node, color) {
        if(!node.children) {
            return ;
        }

        var len = node.children.length;
        for(var i = 0; i < len; ++i) {

            var volumeWidth = node.children[i].volume / maxVolume * flowWidth;

            flowMap.append("path")
                .attr("class", "normal_flow")
                .attr("stroke", color)
                .attr("stroke-linecap", "round")
                .attr("stroke-width", volumeWidth)
                .attr("stroke-opacity", 1)
                .attr("d", function() {
                    var nodeList = [node.pos];
                    if(node.children[i].interpolate_pos) {
                        nodeList.push(node.children[i].interpolate_pos)
                    } else {
                        var controlPoint = {
                            X: (node.pos.X + node.children[i].pos.X) / 2,
                            Y: (node.pos.Y + node.children[i].pos.Y) / 2
                        };
                        controlPoint.X = controlPoint.X + (i+1-len/2)*10;
                        nodeList.push(controlPoint);
                    }

                    nodeList.push(node.children[i].pos);

                    return line(nodeList);
                })
                .attr("fill", "none");
        }

        for(var i = 0; i < node.children.length; ++i) {
            drawTree(node.children[i], color);
        }
    }
}