'use strict';

angular.module('lineUp').controller('lineUpController', ['$scope', 'lineUpService', 'dataService', 'dbService', 'pipService',
    function($scope, lineUpService, dataService, dbService, pipService) {

        lineUpService.loadData = loadData;
        lineUpService.getDataListFiltered = getDataListFiltered;

        $scope.switchIsLegend = switchIsLegend;

        function loadData(data) {
            lineUpService.data = data;

            pipService.emitLineUpDataChange(data);
        }

        function getDataListFiltered(mnList, selectedSourceList, selectedDestList, callback) {
            var dateList = dataService.getDateList();

            var names = [];
            var parasList = [];
            for(var i = 0; i < mnList.length; ++i) {
                names.push('/lineup/filter');
                parasList.push({
                    dateList: dateList,
                    m_n: mnList[i][0]+'_'+mnList[i][1],
                    selectedSource: selectedSourceList,
                    selectedDest: selectedDestList
                });
            }

            dbService.posts(names, parasList, function(data) {
                callback(data);
            });
        }

        function switchIsLegend() {
            lineUpService.switchIsLegend();
        }
    }
]);