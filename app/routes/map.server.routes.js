'use strict';

module.exports = function(app) {
    var map = require('../../app/controllers/map.server.controller');

    // handle map info route with date
    app.route('/map/info').post(map.getMapInfo);

    // handle heatmap route with date
    app.route('/map/heatmap/source').post(map.getHeatMapSource);
    app.route('/map/heatmap/destination').post(map.getHeatMapDest);

    // handle cooccurrence route with date
    app.route('/map/cooccurrence').post(map.getCooccurrence);
};
