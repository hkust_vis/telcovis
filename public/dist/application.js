'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'telco';
	var applicationModuleVendorDependencies = ['ngResource', 'ngAnimate', 'ui.router', 'ui.bootstrap', 'ui.utils'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('core');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('flowMap');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('lineUp');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('map');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('matrix');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('parallelCoordinates');

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('radialGraph');

'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}
]);
'use strict';

angular.module('core').controller('headerController', ['$rootScope', '$scope',
	function($rootScope, $scope) {
	}
]);
'use strict';

angular.module('core').controller('homeController', ['$scope', 'dataService',
	function($scope, dataService) {
        $scope.changeControlParameters = changeControlParameters;

        function changeControlParameters() {
            changeDate();
            changeHourRange();
        }

        function changeDate() {
            var wholeDateList = dataService.getWholeDateList();

            var dateList = [];
            var startDateIndex = $("#date-slider-range").slider("values", 0);
            var endDateIndex = $("#date-slider-range").slider("values", 1);

            for(var i = startDateIndex; i <= endDateIndex; ++i ) {
                dateList.push(wholeDateList[i]);
            }

            dataService.setDateList(dateList);
        }

        function changeHourRange() {
            var start = $("#hour-slider-range").slider("values", 0);
            var end = $("#hour-slider-range").slider("values", 1);

            var hours = d3.range(start, end+1, 1);
            dataService.setHours(hours);
        }
	}
]);
'use strict';

angular.module('core').directive('homeDirective', ['homeService', 'dataService', function(homeService, dataService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            var wholeDateList = dataService.getWholeDateList();

            setDateRangeSlider();
            setHourRangeSlider();

            // init date range slider
            function setDateRangeSlider() {
                $(function () {
                    $("#date-slider-range").slider({
                        range: true,
                        min: 0,
                        max: wholeDateList.length-1,
                        values: [ 0, wholeDateList.length-1 ],
                        step: 1,
                        slide: function (event, ui) {
                            var start = ui.values[ 0 ];
                            var end  = ui.values[ 1 ];

                            $("#date-range").val(wholeDateList[start] + " - " + wholeDateList[end]);
                        }
                    });
                    $("#date-range").val(wholeDateList[$("#date-slider-range").slider("values", 0)] +
                        " - " + wholeDateList[$("#date-slider-range").slider("values", 1)]);
                });
            }

            // init hour range slider
            function setHourRangeSlider() {

                $(function () {
                    $("#hour-slider-range").slider({
                        range: true,
                        min: 0,
                        max: 23,
                        values: [ 0, 23 ],
                        step: 1,
                        slide: function (event, ui) {
                            var start = ui.values[ 0 ];
                            var end  = ui.values[ 1 ];

                            $("#hour-range").val(start + " - " + end);
                        }
                    });
                    $("#hour-range").val($("#hour-slider-range").slider("values", 0) +
                        " - " + $("#hour-slider-range").slider("values", 1));
                });
            }
        }
    }
}]);
'use strict';

angular.module('core').factory('configService',
    function() {

        var matrixGraph = {
            size: {
                width: 500,
                height: 260
            },
            cell: {
                gap: 1,
                opacityRange: [0.3, 1],
                minOpacity: 0.1,
                color: {
                    normal: 'dodgerblue',
                    selected: 'red',
                    unavailable: 'gray',
                    testing: 'orange'
                }
            },
            zoomScaleExtent: [1, 32],
            legend: {
                margin: {
                    left: 5,
                    right: 15,
                    top: 45,
                    bottom: 20
                },
                textSize: 8,
                startOpacity: 0.1,
                endOpacity: 1
            },
            tooltip: {
                opacity: 0.9
            }
        };

        var parallelCoordinates = {
            size: {
                width: 520,
                height: 360
            },
            margin: {
                top: 30,
                right: 50,
                bottom: 20,
                left: 10
            },
            dimensionNameMap: {
                "m_sd_distance":"m.Std.Dist",
                "m_mean_distance":"m.Avg.Dist",
                "n_m_sd_distance":"n.m.Std.Dist",
                "n_m_min":"n.m.Min.Dist",
                "n_m_mean_distance":"n.m.Avg.Dist",
                "n_m_max": "n.m.Max.Dist",
                "time_slice_sum":"no.TimeSlice",
                "persons_sum":"no.People"
            },
            histogram: {
                slice: {
                    num: 100,
                    gap: 10
                },
                opacity: 0.8,
                colorScale: {
                    domain: [-2, -1, -0.75, -0.5, -0.25, 0, 0.25, 0.5, 0.75, 1, 2],
                    range: ['rgb(165,0,38)', 'rgb(215,48,39)', 'rgb(244,109,67)', 'rgb(253,174,97)', 'rgb(254,224,139)', 'rgb(255,255,191)', 'rgb(217,239,139)', 'rgb(166,217,106)', 'rgb(102,189,99)', 'rgb(26,152,80)', 'rgb(0,104,55)']  //red,yellow,green
                }
            },
            axis: {
                textAnchor: 'middle',
                y: -9
            },
            brush: {
                x: -8,
                width: 16
            },
            legend: {
                margin: {
                    top: 20,
                    bottom: 10,
                    left: 35,
                    right: 30
                }
            }
        };

        return {
            matrixGraph: matrixGraph,
            parallelCoordinates: parallelCoordinates
        };
    }
);

'use strict';

angular.module('core').factory('dataService', ['pipService', function(pipService) {
    // Guang Zhou database ['20131020', '20131021'];
    // Shang Hai database ['20150410', '20150411'];
    var wholeDateList = ['20150410', '20150411']; // all date used
    var dateList = ['20150410', '20150411'];    // date used now
    var hourRange = {
        start: 0,
        end: 24
    };  // hour range used now
    var hours = d3.range(24);   // hours used now

    function getWholeDateList() {
        return wholeDateList;
    }

    function getDateList() {
        return dateList;
    }

    function setDateList(_dateList) {
        if(!_.isEqual(dateList, _dateList)) {
            dateList = _dateList;

            pipService.emitDateListChange(dateList);
        }
    }

    function getHourRange() {
        return hourRange;
    }

    function setHourRange(start, end) {
        start = +start;
        end = +end;

        var _start = Math.min(start, end);
        var _end = Math.max(start, end);
        if(_start != hourRange.start || _end != hourRange.end) {
            hourRange.start = _start;
            hourRange.end = _end;

            pipService.emitHourRangeChange(hourRange);
        }
    }

    function getHours() {
        return hours;
    }

    function setHours(_hours) {
        _hours.forEach(function(d) {
            d = +d;
        });

        _hours = _.uniq(_hours);

        _hours.sort(function(a, b) {
            return a-b;
        });

        if(!_.isEqual(hours, _hours)) {
            hours = _hours;

            pipService.emitHoursChange(hours);

            setHourRange(hours[0], hours[hours.length - 1]+1);
        }
    }

    return {
        getWholeDateList: getWholeDateList,

        getDateList: getDateList,
        setDateList: setDateList,

        getHourRange: getHourRange,
        setHourRange: setHourRange,
        getHours: getHours,
        setHours: setHours
    }
}]);
'use strict';

angular.module('core').factory('dbService', ['$http',
    function($http) {

        function get(name, callback){
            $http.get(name)
                .success(function(data, status, headers, config){
                    callback(data);
                })
                .error(function(data, status, headers, config){
                    console.log('get data from db error: ' + status);
                });
        }

        function gets(names, callback) {
            var i = 0;
            var dataList = [];

            var _callback = function(data) {
                dataList.push(data);

                if(names.length <= i) {
                    callback(dataList);
                    return ;
                }

                get(names[i++], _callback);
            };

            if(names.length > 0) {
                get(names[i++], _callback);
            } else {
                callback(dataList);
            }
        }

        function post(name, req, callback){
            $http.post(name, req)
                .success(function(data, status, headers, config){
                    callback(data);
                })
                .error(function(data, status, headers, config){
                    console.log('get data from db error: ' + status);
                });
        }

        function posts(names, reqs, callback) {
            var i = 0;
            var dataList = [];

            var _callback = function(data) {
                dataList.push(data);

                if(names.length <= i) {
                    callback(dataList);
                    return ;
                }

                post(names[i], reqs[i++], _callback);
            };

            if(names.length > 0) {
                post(names[i], reqs[i++], _callback);
            } else {
                callback(dataList);
            }
        }

        // Public API
        return {
            get: get,
            gets: gets,

            post: post,
            posts: posts
        };
    }
]);
'use strict';

angular.module('core').factory('funcService',
    function() {

        // clone object
        function objectClone (sObj) {
            if (typeof sObj !== "object") {
                return sObj;
            }

            var s = {};
            if (sObj.constructor == Array) {
                s = [];
            }

            for (var i in sObj) {
                s[i] = objectClone(sObj[i]);
            }

            return s;
        }

        // get coordinate vector
        function getCoordinateVector(node) {
            if (node.hasOwnProperty('X') && node.hasOwnProperty('Y')) {
                return [node.X, node.Y];
            }
            else if (node.hasOwnProperty('x') && node.hasOwnProperty('y')) {
                return [node.x, node.y];
            }
            else if (node instanceof Array && node instanceof Array) {
                return node;
            }

            return node;
        }

        // get euclidean distance
        function getDistance(p, q) {
            var dis = 0;
            p = getCoordinateVector(p);
            q = getCoordinateVector(q);

            if (p instanceof Array && q instanceof Array) {
                dis = Math.sqrt(Math.pow(p[0] - q[0], 2) + Math.pow(p[1] - q[1], 2));
            }

            return dis;
        }

        function getDirectionRadius(source, dest) {
            source = getCoordinateVector(source);
            dest = getCoordinateVector(dest);

            var thita = Math.atan2(source[1]-dest[1], source[0]-dest[0]);

            return thita;
        }

        // swap two variables
        function swap(a, b) {
            var c = a;
            a = b;
            b = c;
        }

        // generate polygon path
        function polygon(d) {
            if (d.length == 0) {
                return "";
            }
            return 'M' + d.join('L') + 'Z';
        }

        // generate line path
        function line(d) {
            return 'M' + d.join('L');
        }

        // get center point
        function getCenterPoint(p1, p2) {
            return [(p1[0] + p2[0]) / 2, (p1[1] + p2[1]) / 2];
        }

        // is include in the range
        function isInRangeInclude(value, min, max) {
            return (value >= min) && (value <= max);
        }

        // calculate area of polygon
        function calculateArea(polygon) {
            var area = 0;
            var n = polygon.length;

            for (var i = 0; i < polygon.length; ++i) {
                var i2 = i;
                var i3 = i + 1;
                if (i3 >= n) {
                    i3 = i3 - n;
                }
                var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
                var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

                var _area = p2[0] * p3[1] - p3[0] * p2[1];
                _area = _area * 0.5;

                area = area + _area;
            }

            return area;
        }

        // is polygon concave
        function isConcave(polygon) {

            var n = polygon.length;

            var flag = false;

            for (var i = 0; i < n; ++i) {
                var i1 = i - 1;
                if (i1 < 0) {
                    i1 = i1 + n;
                }
                var i2 = i;
                var i3 = i + 1;
                if (i3 >= n) {
                    i3 = i3 - n;
                }
                var p1 = [polygon[i1][0], -1 * polygon[i1][1]];
                var p2 = [polygon[i2][0], -1 * polygon[i2][1]];
                var p3 = [polygon[i3][0], -1 * polygon[i3][1]];

                var result = crossMulti(p1, p2, p3);
                if (result < 0) {
                    flag = true;
                }
            }

            return flag;
        }

        // cross multiply
        function crossMulti(p1, p2, p3) {
            return (p1[0]*p2[1]+p2[0]*p3[1]+p3[0]*p1[1]) - (p1[0]*p3[1]+p2[0]*p1[1]+p3[0]*p2[1]);
        }

        // get number of properties in one object
        function getPropertyNum(obj) {
            var count = 0;
            for(var i in obj) {
                ++count;
            }

            return count;
        }

        return {
            objectClone: objectClone,
            getCoordinateVector: getCoordinateVector,
            getDistance: getDistance,
            getDirectionRadius: getDirectionRadius,
            swap: swap,
            polygon: polygon,
            line: line,
            getCenterPoint: getCenterPoint,
            isInRangeInclude: isInRangeInclude,
            calculateArea: calculateArea,
            isConcave: isConcave,
            crossMulti: crossMulti,
            getPropertyNum: getPropertyNum
        }
    }
);

'use strict';

angular.module('core').factory('homeService',
    function() {
        return {
        };
    }
);
'use strict';

angular.module('core').factory('jsonService', ['$http',
	function($http) {

		var getJson = function(path, callback){
			$http.get(path).success(
				function(res){
					callback(res);
				}
			);
		};

        var getJsons = function(paths, callback) {
            var i = 0;
            var dataList = [];

            var _callback = function(data) {
                dataList.push(data);

                if(paths.length <= i) {
                    callback(dataList);
                    return ;
                }

                getJson(paths[i++], _callback);
            };

            if(paths.length > 0) {
                getJson(paths[i++], _callback);
            } else {
                callback(dataList);
            }
        };

		// Public API
		return {
			getJson: getJson,
            getJsons: getJsons
		};
	}
]);
'use strict';

angular.module('core').factory('pipService', ['$window', '$rootScope',
	function($window, $rootScope) {

        var MAP_DATA_MESSAGE = "mapDataChange";
        var PARALLEL_COORDINATES_DATA_MESSAGE = "parallelCoordinatesDataChange";
        var LINE_UP_DATA_MESSAGE = "lineUpDataChange";
        var RADIAL_GRAPH_DATA_MESSAGE = "radialGraphDataChange";
        var FLOW_MAP_STAY_DATA_MESSAGE = "flowMapStayDataChange";
        var FLOW_MAP_SOURCE_MESSAGE = "flowMapSourceChange";
        var HOUR_RANGE_MESSAGE = 'hourRangeChange';
        var HOURS_MESSAGE = 'hoursChange';
        var DATE_LIST_MESSAGE = 'dateListChange';

		var emitMapDataChange = function(data){
			$rootScope.$broadcast(MAP_DATA_MESSAGE, data);
		};

		var onMapDataChange = function(scope, callback){
			scope.$on(MAP_DATA_MESSAGE, function(event, data){
				callback(data);
			});
		};

        var emitParallelCoordinatesDataChange = function(data){
            $rootScope.$broadcast(PARALLEL_COORDINATES_DATA_MESSAGE, data);
        };

        var onParallelCoordinatesDataChange = function(scope, callback){
            scope.$on(PARALLEL_COORDINATES_DATA_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitLineUpDataChange = function(data){
            $rootScope.$broadcast(LINE_UP_DATA_MESSAGE, data);
        };

        var onLineUpDataChange = function(scope, callback){
            scope.$on(LINE_UP_DATA_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitRadialGraphDataChange = function(data){
            $rootScope.$broadcast(RADIAL_GRAPH_DATA_MESSAGE, data);
        };

        var onRadialGraphDataChange = function(scope, callback){
            scope.$on(RADIAL_GRAPH_DATA_MESSAGE, function(event, data){
                callback(data);
            });
        };


        var emitFlowMapStayDataChange = function(data){
            $rootScope.$broadcast(FLOW_MAP_STAY_DATA_MESSAGE, data);
        };

        var onFlowMapStayDataChange = function(scope, callback){
            scope.$on(FLOW_MAP_STAY_DATA_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitFlowMapSourceChange = function(data){
            $rootScope.$broadcast(FLOW_MAP_SOURCE_MESSAGE, data);
        };

        var onFlowMapSourceChange = function(scope, callback){
            scope.$on(FLOW_MAP_SOURCE_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitHourRangeChange = function(data){
            $rootScope.$broadcast(HOUR_RANGE_MESSAGE, data);
        };

        var onHourRangeChange = function(scope, callback){
            scope.$on(HOUR_RANGE_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitHoursChange = function(data){
            $rootScope.$broadcast(HOURS_MESSAGE, data);
        };

        var onHoursChange = function(scope, callback){
            scope.$on(HOURS_MESSAGE, function(event, data){
                callback(data);
            });
        };

        var emitDateListChange = function(data){
            $rootScope.$broadcast(DATE_LIST_MESSAGE, data);
        };

        var onDateListChange = function(scope, callback){
            scope.$on(DATE_LIST_MESSAGE, function(event, data){
                callback(data);
            });
        };

        // Public API
		return {
            emitMapDataChange: emitMapDataChange,
            onMapDataChange: onMapDataChange,

            emitParallelCoordinatesDataChange: emitParallelCoordinatesDataChange,
            onParallelCoordinatesDataChange: onParallelCoordinatesDataChange,

            emitLineUpDataChange: emitLineUpDataChange,
            onLineUpDataChange: onLineUpDataChange,

            emitRadialGraphDataChange: emitRadialGraphDataChange,
            onRadialGraphDataChange: onRadialGraphDataChange,

            emitFlowMapStayDataChange: emitFlowMapStayDataChange,
            onFlowMapStayDataChange: onFlowMapStayDataChange,
            emitFlowMapSourceChange: emitFlowMapSourceChange,
            onFlowMapSourceChange: onFlowMapSourceChange,

            emitHourRangeChange: emitHourRangeChange,
            onHourRangeChange: onHourRangeChange,
            emitHoursChange: emitHoursChange,
            onHoursChange: onHoursChange,

            emitDateListChange: emitDateListChange,
            onDateListChange: onDateListChange
		};
	}
]);

'use strict';

angular.module('flowMap').controller('flowMapController', ['$scope', 'flowMapService', 'dataService', 'dbService', 'pipService', 'mapService',
    function($scope, flowMapService, dataService, dbService, pipService) {

        flowMapService.loadData = loadData;

        $scope.changeControlParameters = function changeControlParameters() {
            flowMapService.changeControlParameters();
        };

        pipService.onFlowMapSourceChange($scope, function(sources) {
            flowMapService.data.sources = sources;
            flowMapService.loadData(sources);
        });

        // load data
        function loadData(sources) {
            if(!sources || sources.length == 0) {
                flowMapService.clearMainGraph();

                return ;
            }

            var name = '/flowmap';
            var paras = {
                gpsIDs: sources,
                dateList: dataService.getDateList()
            };

            dbService.post(name, paras, function(data) {
                flowMapService.data.stayData = data;

                pipService.emitFlowMapStayDataChange(flowMapService.data.stayData);
            });
        }


    }
]);
'use strict';

angular.module('flowMap').directive('flowMapDirective', ["flowMapService", 'dataService', 'funcService', 'pipService', 'mapService', function(flowMapService, dataService, funcService, pipService, mapService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {

            setFlowMap();

            function setFlowMap() {
                setHourRangeSlider();

                pipService.onDateListChange(scope, function() {
                    flowMapService.clearMainGraph();
                });

                pipService.onHoursChange(scope, function(hours) {
                    $("#flow_map_slider-range").slider({
                        values: [hours[0], hours[hours.length-1]]
                    });

                    if(flowMapService.setHours) {
                        flowMapService.setHours(hours);
                    }
                });

                // init hour range slider
                function setHourRangeSlider() {
                    $(function () {
                        $("#flow_map_slider-range").slider({
                            range: true,
                            min: 0,
                            max: 23,
                            values: [ 0, 23 ],
                            step: 1,
                            slide: function (event, ui) {
                                var start = ui.values[ 0 ];
                                var end  = ui.values[ 1 ];

                                $("#source_hour_range").val(start + " - " + end);

                                var hours = d3.range(start, end+1, 1);
                                dataService.setHours(hours);
                            }
                        });
                        $("#flow_map_hour_range").val($("#flow_map_slider-range").slider("values", 0) +
                            " - " + $("#flow_map_slider-range").slider("values", 1));
                    });
                }

                var clusterNum = 3;
                flowMapService.data.clusterNum = clusterNum;
                var nodeNumOfCluster = 5;
                flowMapService.data.nodeNumOfCluster = nodeNumOfCluster;

                flowMapService.data.timeFilterNodes = [undefined, undefined, undefined];

                flowMapService.data.flowColors = [
                    "Green",
                    "Crimson"
                ];


                flowMapService.data.clusterColors = [
                    'rgb(107,174,214)',
                    'rgb(254,153,41)',
                    'rgb(158,154,200)',

                    "#2171b5",
                    "#fe9929",
                    "#6a51a3"
                ];
                flowMapService.data.clusterColorsSequential = [
                    ['rgb(239,243,255)','rgb(189,215,231)','rgb(107,174,214)','rgb(33,113,181)'],
                    ['rgb(255,255,212)','rgb(254,217,142)','rgb(254,153,41)','rgb(204,76,2)'],
                    ['rgb(242,240,247)','rgb(203,201,226)','rgb(158,154,200)','rgb(106,81,163)']
                ];

                var width = 800,
                    height = 900;

                var svg = d3.select("#flowmap_graph").append("svg")
                    .attr("width", width)
                    .attr("height", height);

                flowMapService.data.pointA = {
                    pos: {X: width / 3, Y: 30}
                };
                flowMapService.data.pointB = {
                    pos: {X: 2 * width / 3, Y: 30}
                };
                flowMapService.data.sourcePoints = [
                    flowMapService.data.pointA,
                    flowMapService.data.pointB
                ];

                // render left source point
                var pointA = svg
                    .append("circle")
                    .attr("class", "point_A")
                    .attr("cx", function(d) {
                        return flowMapService.data.pointA.pos.X;
                    })
                    .attr("cy", function(d) {
                        return flowMapService.data.pointA.pos.Y;
                    })
                    .attr("r", 20)
                    .attr("stroke", "grey")
                    .attr("stroke-width", 10)
                    .attr("fill", "none");

                svg
                    .append("circle")
                    .attr("class", "point_A")
                    .attr("cx", function(d) {
                        return flowMapService.data.pointA.pos.X;
                    })
                    .attr("cy", function(d) {
                        return flowMapService.data.pointA.pos.Y;
                    })
                    .attr("r", 5)
                    .attr("stroke", "none")
                    .attr("fill", flowMapService.data.flowColors[0])
                    .on("mouseover", function(d) {
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .attr("stroke-width", function () {
                                return 0.1;
                            })
                            .attr("stroke-opacity", function() {
                                return 0.1;
                            });
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .filter(function(_d) {
                                if(_d.source == 0) {
                                    return true;
                                }

                                return false;
                            })
                            .attr("stroke-width", function (d) {
                                return d.width*2;
                            })
                            .attr("stroke-opacity", function(d) {
                                return 1;
                            });

                    })
                    .on("mouseout", function() {
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .attr("stroke-width", function (_d) {
                                return _d.width;
                            })
                            .attr("stroke-opacity", function(_d) {
                                return 1;
                            });
                    });

                // render right source point
                var pointB = svg
                    .append("circle")
                    .attr("class", "point_B")
                    .attr("cx", function(d) {
                        return flowMapService.data.pointB.pos.X;
                    })
                    .attr("cy", function(d) {
                        return flowMapService.data.pointB.pos.Y;
                    })
                    .attr("r", 20)
                    .attr("stroke", "grey")
                    .attr("stroke-width", 10)
                    .attr("fill", "none");

                svg
                    .append("circle")
                    .attr("class", "point_B")
                    .attr("cx", function(d) {
                        return flowMapService.data.pointB.pos.X;
                    })
                    .attr("cy", function(d) {
                        return flowMapService.data.pointB.pos.Y;
                    })
                    .attr("r", 5)
                    .attr("stroke", "none")
                    .attr("fill", flowMapService.data.flowColors[1]).on("mouseover", function(d) {
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .attr("stroke-width", function () {
                                return 0.1;
                            })
                            .attr("stroke-opacity", function() {
                                return 0.1;
                            });
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .filter(function(_d) {
                                if(_d.source == 1) {
                                    return true;
                                }

                                return false;
                            })
                            .attr("stroke-width", function (d) {
                                return d.width*2;
                            })
                            .attr("stroke-opacity", function(d) {
                                return 1;
                            });

                    })
                    .on("mouseout", function() {
                        svg.select(".flowMap")
                            .selectAll(".flow")
                            .attr("stroke-width", function (_d) {
                                return _d.width;
                            })
                            .attr("stroke-opacity", function(_d) {
                                return 1;
                            });
                    });

                var mainGraph = svg
                    .append("g")
                    .attr("class", "main_graph");

                var clusters;

                var POI = [
                    "Shopping Service",
                    "Travel accommodation",
                    "Gourmet"
                ];

                var clusterLngLats = [
                    {
                        longitude: -1,
                        latitude: -1
                    },
                    {
                        longitude: -1,
                        latitude: -1
                    },
                    {
                        longitude: -1,
                        latitude: -1
                    }
                ];

                var voronoiIteration = 100;

                var clusterParamenters = {
                    POI: POI,
                    lngLats: clusterLngLats,
                    timeFilterNodes: flowMapService.data.timeFilterNodes
                };

                var allNodes;

                flowMapService.changeControlParameters = function() {
                    for(var i = 0 ; i < 3; ++i) {
                        var poiDom = $("#VoronoiRegion_poi_" + i)[0];
                        if(poiDom.value != '-1' && poiDom.value != '') {
                            POI[i] = poiDom.value;
                        }
                    }

                    var voronoiIterationDom = $("#VoronoiRegion_iteration")[0];
                    voronoiIteration = parseInt(voronoiIterationDom.value);

                    updateClusters();
                    flowMapService.drawMainGraph();
                };

                var flowmapStyle = true;
                $("#flow_map_style").bind("click", setFlowmapStyle);
                function setFlowmapStyle() {
                    flowmapStyle = !flowmapStyle;
                    flowMapService.drawMainGraph();
                }

                flowMapService.updateClusters = updateClusters;
                function updateClusters() {
                    clusters = generateClusters(allNodes, clusterParamenters);
                    flowMapService.data.clusters = clusters;
                }

                pipService.onFlowMapStayDataChange(scope, function (stayData) {
                    receivedData();
                });

                flowMapService.clearMainGraph = clearMainGraph;
                function clearMainGraph() {
                    $(".main_graph").empty();
                }

                // when receiving data
                function receivedData() {
                    var sourceMapData = mapService.data.map.source;
                    var poiSet = generatePoiSet(sourceMapData);

                    flowMapService.data.pointA.nid = flowMapService.data.sources[0];
                    flowMapService.data.pointA.gps = sourceMapData[flowMapService.data.sources[0]].gps;
                    flowMapService.data.pointA.nid = flowMapService.data.sources[1];
                    flowMapService.data.pointB.gps = sourceMapData[flowMapService.data.sources[1]].gps;

                    var stayData = flowMapService.data.stayData;
                    var contacts = generateContacts(stayData);

                    allNodes = constructAllNodes(sourceMapData, contacts);
                    flowMapService.data.allNodes = allNodes;

                    var poiContacts = getContactsOfPOI(allNodes, poiSet);

                    updatePOINameList(poiContacts);
                    updateClusters();

                    var flowData = [
                        {},     // point A
                        {}      // point B
                    ]; // flowData[point][timeSlot][nid]


                    flowMapService.drawMainGraph = drawMainGraph;
                    drawMainGraph();

                    function drawMainGraph() {
                        clearMainGraph();

                        var _gap = 30;
                        var _step = (width - 2 * _gap) / 3;
                        var _size = [_step, 600 / 4];

                        var nodesPos = getNodesPosOfClusters(clusters, _size);

                        var nodes = [];
                        for (var i = 0; i < clusterNum; ++i) {
                            nodes.push([]);
                            var nodesNum = Math.min(clusters[i].length, nodeNumOfCluster);
                            for (var j = 0; j < nodesNum; ++j) {
                                var node = clusters[i][j];

                                node.cid = i;
                                node.X = nodesPos[i][j][0];
                                node.Y = nodesPos[i][j][1];
                                node.W = node.frequency;

                                nodes[i].push(node);
                            }
                        }

                        var weights = getWeightsOfClusters(nodes);

                        for (var i = 0; i < stayData.length; ++i) {
                            var slices = stayData[i].value;

                            for (var j = 0; j < 24; ++j) {
                                if (slices.hasOwnProperty(String(j))) {
                                    flowData[i][j] = {};

                                    var stays = slices[String(j)];
                                    for (var k in stays) {
                                        flowData[i][j][k] = stays[k];
                                    }
                                }
                            }
                        }

                        var ratioBar = RatioBarView("RatioBarView", nodes, weights, flowData, [0, 100], width, mainGraph, flowMapService);

                        flowMapService.data.timeBarPos = [];
                        flowMapService.data.timeBar = [];

                        var voronoiView = addVoronoiView(voronoiIteration);

                        if (flowmapStyle) {
                            var flowMap = FlowMap(flowData, nodes, width, height, 5, mainGraph, flowMapService);
                            for (var i = 0; i < clusterNum; ++i) {
                                flowMapService.data.timeBar[i].drawTimeBar();
                            }
                            var hours = dataService.getHours();
                            flowMapService.setHours(hours);
                        } else {
                            var normalFlowMap = NormalFlowMap(flowData, nodes, width, height, 5, mainGraph, flowMapService, funcService);
                        }

                        var pointsView = PointsView(nodes, mainGraph, flowMapService, mapService);

                        function addVoronoiView(voronoiIteration) {
                            var _voronoiView = mainGraph.append("g");
                            var voronoiView_1 = VoronoiRegionView(0, nodes[0], weights[0], "voronoi_1", [0, 200], _size, voronoiIteration, _voronoiView, mainGraph, flowMapService, funcService);
                            var voronoiView_2 = VoronoiRegionView(1, nodes[1], weights[1], "voronoi_2", [_step + _gap, 200], _size, voronoiIteration, _voronoiView, mainGraph, flowMapService, funcService);
                            var voronoiView_3 = VoronoiRegionView(2, nodes[2], weights[2], "voronoi_3", [(_step + _gap) * 2, 200], _size, voronoiIteration, _voronoiView, mainGraph, flowMapService, funcService);

                            return _voronoiView;
                        }
                    }

                    function generatePoiSet(sourceMapData) {
                        var poiSet = d3.set();
                        for(var i in sourceMapData) {
                            var poi = sourceMapData[i].poi;
                            poiSet.add(poi);
                        }

                        return poiSet;
                    }

                    function updatePOINameList(poiContacts) {
                        for(var i = 0 ; i < 3; ++i) {
                            d3.select("#cluster_" + i + "_dropdownMenu_button").select(".cluster_name")
                                .text(POI[i]);

                            var poiList = [];

                            poiSet.forEach(function(value) {
                                if(value != POI[0] && value != POI[1] && value != POI[2]) {
                                    poiList.push(value);
                                }
                            });
                            if(poiContacts) {
                                poiList.sort(function(a, b) {
                                    return poiContacts[b] - poiContacts[a];
                                });
                            }

                            $("#cluster_" + i + "_dropdown-menu").empty();
                            var listDOM = d3.select("#cluster_" + i + "_dropdown-menu");
                            for(var j = 0; j < poiList.length; ++j) {
                                listDOM.append("li")
                                    .attr("role", "presentation")
                                    .append("a")
                                    .datum({
                                        cid: i,
                                        name: poiList[j]
                                    })
                                    .attr("role", "menuitem")
                                    .attr("tabindex", j)
                                    .text(poiList[j])
                                    .on("click", function(d) {
                                        POI[d.cid] = d.name;
                                        updatePOINameList();
                                        updateClusters();
                                        flowMapService.drawMainGraph();
                                    });
                            }
                        }
                    }

                    function generateContacts(stayData) {
                        var contacts = {};
                        for(var hour in stayData[0].value) {
                            contacts[hour] = {};
                            var destsA = stayData[0].value[hour];
                            for(var destID in destsA) {
                                var destsB = stayData[1].value[hour];
                                if(destsB.hasOwnProperty(destID)) {
                                    contacts[hour][destID] = destsA[destID] + destsB[destID];
                                }
                            }
                        }

                        return contacts;
                    }

                    function getContactsOfPOI(allNodes, poiSet) {
                        var poiContacts = {};
                        var pois = poiSet.values();
                        for (var i in pois) {
                            poiContacts[pois[i]] = 0;
                        }

                        for (var i in allNodes) {
                            var node = allNodes[i];

                            var poi = node.poi;
                            if (poiContacts.hasOwnProperty(poi)) {
                                poiContacts[poi] += node.frequency;
                            }
                        }

                        return poiContacts;
                    }

                    function getTopNodes(allNodes) {

                        var nodes = [];
                        for (var i in allNodes) {
                            var node = allNodes[i];
                            if (node.frequency == 0) {
                                continue;
                            }

                            nodes.push(node);
                        }

                        nodes.sort(function (a, b) {
                            return b["frequency"] - a["frequency"];
                        });

                        return nodes;
                    }

                    function generateTopNodesOfPOIs(allNodes, pois) {
                        var nodes = [];
                        var poiMap = d3.map();
                        for (var i = 0; i < pois.length; ++i) {
                            nodes.push([]);
                            poiMap.set(pois[i], i);
                        }

                        for (var i in allNodes) {
                            var node = allNodes[i];
                            if (node.frequency == 0) {
                                continue;
                            }

                            for (var j = 0; j < node["poi"].length; ++j) {

                                var nPOI = node["poi"][j];
                                if (!poiMap.has(nPOI)) {
                                    continue;
                                }

                                var POIindex = poiMap.get(nPOI);

                                nodes[POIindex].push(node);
                            }
                        }

                        for (var i = 0; i < pois.length; ++i) {
                            nodes[i].sort(function (a, b) {
                                return b["frequency"] - a["frequency"];
                            });
                        }

                        return nodes;
                    }

                    function getNodesPosOfClusters(clusters, _size) {
                        var minX = 1e6;
                        var maxX = 0;
                        var minY = 1e6;
                        var maxY = 0;

                        var nodesPos = [];
                        for (var i = 0; i < clusterNum; ++i) {
                            nodesPos.push([]);
                            var nodesNum = clusters[i].length;
                            for (var j = 0; j < nodesNum; ++j) {
                                var node = clusters[i][j];

                                var pos = node.gps;
                                var _pos = [pos.lng, pos.lat];

                                if (_pos[0] < minX) {
                                    minX = _pos[0];
                                }
                                if (_pos[0] > maxX) {
                                    maxX = _pos[0];
                                }
                                if (_pos[1] < minY) {
                                    minY = _pos[1];
                                }
                                if (_pos[1] > maxY) {
                                    maxY = _pos[1];
                                }

                                nodesPos[i].push(_pos);
                            }
                        }


                        var x_scale = d3.scale.linear()
                            .domain([minX, maxX])
                            .range([0, _size[0]]);

                        var y_scale = d3.scale.linear()
                            .domain([minY, maxY])
                            .range([0, _size[1]]);

                        for (var i = 0; i < clusterNum; ++i) {
                            var nodesNum = nodesPos[i].length;
                            for (var j = 0; j < nodesNum; ++j) {
                                nodesPos[i][j][0] = x_scale(nodesPos[i][j][0]);
                                nodesPos[i][j][1] = y_scale(nodesPos[i][j][1]);
                            }
                        }
                        return nodesPos;
                    }

                    function getWeightsOfClusters(clusters) {
                        var weights = [];

                        for (var i = 0; i < clusterNum; ++i) {
                            weights.push([]);
                            var nodesNum = clusters[i].length;
                            for (var j = 0; j < nodesNum; ++j) {
                                var node = clusters[i][j];
                                var nid = node.nid;
                                var weight = node.frequency;
                                weights[i].push(weight);
                            }
                        }

                        return weights;
                    }

                    function constructAllNodes(sourceMapData, contacts) {
                        var nodes = {};

                        for (var i in sourceMapData) {
                            nodes[i] = {};
                            nodes[i].nid = i;
                            nodes[i].gps = sourceMapData[i].gps;
                            nodes[i].poi = sourceMapData[i].poi;
                        }

                        var nodesContactsMap = d3.map();
                        for (var i = 0; i < 24; ++i) {
                            if (!contacts.hasOwnProperty(i)) {
                                continue;
                            }
                            var _contacts = contacts[i];

                            for (var j in _contacts) {
                                var nid = j;
                                var freq = _contacts[j];
                                if (nodesContactsMap.has(nid)) {
                                    freq = freq + nodesContactsMap.get(nid);
                                }
                                nodesContactsMap.set(nid, freq);
                            }
                        }


                        var gpsAVector = [flowMapService.data.pointA.gps.lng, flowMapService.data.pointA.gps.lat];
                        var gpsBVector = [flowMapService.data.pointB.gps.lng, flowMapService.data.pointB.gps.lat];
                        var kmPerLngLat = 111;
                        for (var i in nodes) {
                            var node = nodes[i];

                            node.frequency = 0;

                            if (nodesContactsMap.has(i)) {
                                var contact_frequency = nodesContactsMap.get(i);
                                node.frequency = contact_frequency;
                            }

                            node.time_slice = getTimeSliceVector(i, contacts);

                            var nodeGpsVector = [node.gps.lng, node.gps.lat];

                            node.distance_A = funcService.getDistance(gpsAVector, nodeGpsVector)*kmPerLngLat;
                            node.distance_B = funcService.getDistance(gpsBVector, nodeGpsVector)*kmPerLngLat;
                            node.distance_M = 0.5*(node.distance_A + node.distance_B);

                            node.direction_A = funcService.getDirectionRadius(gpsAVector, nodeGpsVector)/Math.PI*180;
                            node.direction_B = funcService.getDirectionRadius(gpsBVector, nodeGpsVector)/Math.PI*180;
                            node.direction_M = 0.5*(node.direction_A + node.direction_B);
                        }

                        return nodes;
                    }

                    function getTimeSliceVector(nodeID, contacts) {
                        var vector = new Array(24);
                        for(var hour in vector) {
                            vector[hour] = 0;
                        }

                        for(var hour in contacts) {
                            var _contacts = contacts[hour];
                            if(_contacts.hasOwnProperty(nodeID)) {
                                vector[hour] = 1;
                            }
                        }

                        return vector;
                    }
                }

                function generateClusters(allNodes, clusterParamenters) {
                    var nodes = [];
                    var poiMap = d3.map();
                    for(var i = 0; i < clusterNum; ++i) {
                        nodes.push([]);
                        poiMap.set(clusterParamenters.POI[i], i);
                    }

                    // calculate clusters
                    for(var i in allNodes) {
                        var node = allNodes[i];

                        // filter outlier node
                        if(node.frequency == 0 || node.frequency > 20000) {
                            continue ;
                        }

                        var nPOI = node.poi;
                        if (poiMap.has(nPOI)) {
                            var POIindex = poiMap.get(nPOI);
                            nodes[POIindex].push(node);
                        }
                    }

                    for(var i = 0; i < clusterNum; ++i) {
                        var timeFilterNode = clusterParamenters.timeFilterNodes[i];

                        nodes[i].sort(function (a, b) {
                            if(!timeFilterNode) {
                                return b["frequency"] - a["frequency"];
                            } else {
                                return getTimeSliceSimilarity(timeFilterNode, b) - getTimeSliceSimilarity(timeFilterNode, a);
                            }
                        });
                    }

                    return nodes;
                }

                function getTimeSliceSimilarity(np, nq){
                    var p = np.time_slice;
                    var q = nq.time_slice;
                    var sum = 0;
                    for(var i = 0; i < p.length; ++i){
                        sum += p[i]^q[i];
                    }

                    return (p.length - sum)/p.length;
                }
            }


        }
    }
}]);
'use strict';

angular.module('flowMap').factory('flowMapService',
    function() {
        var flowMapService = {
            data: {}
        };

        return flowMapService;
    }
);
'use strict';

angular.module('lineUp').controller('lineUpController', ['$scope', 'lineUpService', 'dataService', 'dbService', 'pipService',
    function($scope, lineUpService, dataService, dbService, pipService) {

        lineUpService.loadData = loadData;
        lineUpService.getDataListFiltered = getDataListFiltered;

        $scope.switchIsLegend = switchIsLegend;

        function loadData(data) {
            lineUpService.data = data;

            pipService.emitLineUpDataChange(data);
        }

        function getDataListFiltered(mnList, selectedSourceList, selectedDestList, callback) {
            var dateList = dataService.getDateList();

            var names = [];
            var parasList = [];
            for(var i = 0; i < mnList.length; ++i) {
                names.push('/lineup/filter');
                parasList.push({
                    dateList: dateList,
                    m_n: mnList[i][0]+'_'+mnList[i][1],
                    selectedSource: selectedSourceList,
                    selectedDest: selectedDestList
                });
            }

            dbService.posts(names, parasList, function(data) {
                callback(data);
            });
        }

        function switchIsLegend() {
            lineUpService.switchIsLegend();
        }
    }
]);
'use strict';

angular.module('lineUp').directive('lineUpDirective', ["lineUpService", 'pipService', 'mapService', function(lineUpService, pipService, mapService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {

            pipService.onLineUpDataChange(scope, function () {

            });

            LineUpStart(LineUp);
            lineUpService.lineUp.setData([]);


            var isLegend = true;
            lineUpService.switchIsLegend = switchIsLegend;

            function switchIsLegend() {
                isLegend = !isLegend;
                d3
                    .select("#legend_lineup")
                    .style("display", isLegend ? "block" : "none");
            }

            function LineUpStart(LineUp) {
                var menuActions = [
                    {name: " new combined", icon: "fa-plus", action: function () {
                        lineup.addNewStackedColumnDialog();
                    }},
                    {name: " add single columns", icon: "fa-plus", action: function () {
                        lineup.addNewSingleColumnDialog();
                    }}
                ];
                var lineUpDemoConfig = {
                    svgLayout: {
                        mode: 'separate',
                        plusSigns: {
                            addStackedColumn: {
                                title: "add stacked column",
                                action: "addNewEmptyStackedColumn",
                                x: 0, y: 2,
                                w: 21, h: 21 // LineUpGlobal.htmlLayout.headerHeight/2-4
                            }
                        }
                    }
                };

                var lineup = null;

                $(window).resize(function() {
                    if (lineup) {
                        lineup.updateBody();
                    }
                });
                function updateMenu() {
                    var config = lineup.config;
                    var kv = d3.entries(lineup.config.renderingOptions);
                    var kvNodes = d3.select("#lugui-menu-rendering").selectAll("span").data(kv, function (d) {
                        return d.key;
                    });
                    kvNodes.exit().remove();
                    kvNodes.enter().append("span").on('click', function (d) {
                        lineup.changeRenderingOption(d.key, !config.renderingOptions[d.key]);
                        updateMenu();
                    });
                    kvNodes.html(function (d) {
                        return '<a href="#"> <i class="fa ' + (d.value ? 'fa-check-square-o' : 'fa-square-o') + '" ></i> ' + d.key + '</a>&nbsp;&nbsp;'
                    });

                    d3.select("#lugui-menu-actions").selectAll("span").data(menuActions)
                        .enter().append("span").html(
                        function (d) {
                            return '<i class="fa ' + (d.icon) + '" ></i>' + d.name + '&nbsp;&nbsp;'
                        }
                    ).on("click", function (d) {
                            d.action.call(d);
                        });

                    d3.select("#lugui-menu-rendering").selectAll("span").remove();
                    d3.select("#lugui-menu-actions").selectAll("span").remove();
                    d3.select(".lu.lu-header").selectAll(".header.emptyHeader").remove();
                    var header = d3.select(".lu.lu-header").selectAll(".header");
                    header.selectAll(".headerSort").remove();
                    header.selectAll(".fontawe.stackedColumnInfo").remove();
                    header.selectAll(".fontawe.singleColumnDelete").remove();

                    header.each(function(){

                        var header = d3.select(this);
                        if(header.select("text")[0][0].innerHTML == "Rank"){
                            header.select("text").remove();
                            header.select("rect.labelBG").remove();
                        }

                    });



                    $(".addColumnButton").empty();
                }


                function loadDataImpl(name, desc, _data) {
                    var spec = {};
                    spec.name = name;
                    spec.dataspec = desc;
                    delete spec.dataspec.file;
                    delete spec.dataspec.separator;
                    spec.dataspec.data = _data;
                    spec.storage = LineUp.createLocalStorage(_data, desc.columns, desc.layout, desc.primaryKey);

                    if (lineup) {
                        lineup.changeDataStorage(spec);
                    } else {
                        lineup = LineUp.create(spec, d3.select('#lugui-wrapper'), lineUpDemoConfig);
                    }
                    updateMenu();

                    var lulubody = d3.select(".lu.lu-body");
                    var rows = lulubody.selectAll(".row").attr("rowId",function(d,i){return "row"+i;});

                    var rowHeight = lineup.config.svgLayout.rowHeight;
                    var columnWidth = mnColumnWidth;
                    var headerHeight = 210;
                    var offsetLeft = parseFloat(d3.select("#lugui-wrapper")[0][0].offsetLeft) - 118 + 6 + 0.35*columnWidth;//+ 20 - 0.5*columnWidth; // 118 is 1/2 of width of horizon charts
                    var offsetTop = parseFloat(d3.select("#lugui-wrapper")[0][0].offsetTop) - 15 - 0.5*columnWidth; // 118 is 1/2 of width of horizon charts

                    var horizonCharHeight = 250;
                    var horizonCharts = d3.select("#horizon_charts");

                    var addColOffset = parseFloat(d3.select(".addColumnButton")[0][0].attributes["transform"].value.split(",")[0].split("(")[1]);

                    var items = rows.selectAll(".tableData.stacked").selectAll("rect")
                        .on("mouseover", function(d) {
                            var result = getNodeInfo(d);
                            var nodeData = result.nodeData;
                            var m_n = result.m_n;
                            if(!nodeData) {
                                return ;
                            }

                            if(m_n == 'm') {
                                mapService.map.source.setView([nodeData.gps.lat, nodeData.gps.lng]);
                            } else {
                                mapService.map.dest.setView([nodeData.gps.lat, nodeData.gps.lng]);
                            }
                        })
                        .on("click", function(d) {
                            var result = getNodeInfo(d);
                            var nodeData = result.nodeData;
                            var m_n = result.m_n;
                            var nid = result.nid;
                            if(!nodeData) {
                                return ;
                            }

                            if(m_n == 'm') {
                                if(!orangeICONS.sourceICONS.hasOwnProperty(nid)) {
                                    var _marker = mapService.map.source.addPurpleMarker(nodeData.gps.lat, nodeData.gps.lng);
                                    orangeICONS.sourceICONS[nid] = _marker;

                                    d3.select(this).attr("height", 8);
                                } else {
                                    var _marker = orangeICONS.sourceICONS[nid];
                                    mapService.map.source.removeLayer(_marker);

                                    d3.select(this).attr("height", 16);

                                    delete orangeICONS.sourceICONS[nid];
                                }
                            } else {
                                if(!orangeICONS.destICONS.hasOwnProperty(nid)) {
                                    var _marker = mapService.map.dest.addYellowMarker(nodeData.gps.lat, nodeData.gps.lng);
                                    orangeICONS.destICONS[nid] = _marker;

                                    d3.select(this).attr("height", 8);
                                } else {
                                    var _marker = orangeICONS.destICONS[nid];
                                    mapService.map.dest.removeLayer(_marker);

                                    d3.select(this).attr("height", 16);

                                    delete orangeICONS.destICONS[nid];
                                }
                            }

                        });

                    function getNodeInfo(d) {
                        var rowID = parseInt(d.row.temporary_id);
                        var splitResult = d.child.columnLink.split("_");
                        var m_n = splitResult[0];
                        var index = parseInt(splitResult[1]);

                        var node = _data[rowID][m_n][index];
                        if(!node) {
                            return {
                                nid: undefined,
                                nodeData: undefined,
                                m_n: m_n

                            };
                        }
                        var nid = node.id;
                        return {
                            nid: nid,
                            nodeData: mapService.data.map.source[nid],
                            m_n: m_n
                        }
                    }

                    var triangles_icons = rows.selectAll(".triangles_icons").selectAll("rect")
                        .on("click", function(d) {
                            var scrollTop = d3.select(".lu-wrapper")[0][0].scrollTop;


                            var _rows = lulubody.selectAll(".row")[0];
                            _rows.sort(function(a, b) {
                                var aValue = parseFloat(a.attributes["transform"].value.split(",")[1]);
                                var bValue = parseFloat(b.attributes["transform"].value.split(",")[1]);

                                return aValue - bValue;
                            });

                            var i = -1;
                            for(var j = 0; j < _rows.length; ++j) {
                                var _d = d3.select(_rows[j]).datum();

                                if(_d.id == d.id) {
                                    i = j;
                                }
                            }

                            if(!clicked[d.id]) {
                                clicked[d.id] = true;
                                lulubody.attr("height", parseInt(lulubody[0][0].attributes["height"].value) + horizonCharHeight);


                                adjustHorizonChartTopPos(i, +1);


                                var transformValue = _rows[i].attributes["transform"].value;
                                var splits = transformValue.split(",");
                                var value = parseFloat(splits[1]);

                                var horizonChart = horizonCharts.append("div")
                                    .attr("class", "horizon_chart_"+ d.id);

                                //console.log(d.m);
                                //console.log(d.n);

                                addHorizonChart(d.m, 0, _rows[i]);
                                addHorizonChart(d.n, mNum, _rows[i]);
                                addTimeIndicatorLine(mNum+nNum, _rows[i]);

                            } else {
                                clicked[d.id] = false;
                                var horizonChart = horizonCharts.select(".horizon_chart_"+ d.id)
                                    .remove();

                                adjustHorizonChartTopPos(i, -1);

                                lulubody.attr("height", parseInt(lulubody[0][0].attributes["height"].value) - horizonCharHeight);
                            }

                            function addHorizonChart(list, offset, row) {
                                var mCol = d3.select(row).selectAll(".tableData.stacked")[0][0];
                                var _transformValue = mCol.attributes["transform"].value;
                                var _splits = _transformValue.split(",")[0].split("(");
                                var colValue = parseFloat(_splits[1]);

                                for(var k = 0; k < list.length; ++k) {
                                    var horizon = Horizon();
                                    var selection = horizonChart.append("div")
                                        .attr("class", "horizon_chart")
                                        .style("width", mnColumnWidth+"px")
                                        .style("height", "236px")
                                        .style("position", "absolute")
                                        .style("top", offsetTop - scrollTop + headerHeight + value + "px")
                                        .style("left", offsetLeft + colValue + (offset+k)*columnWidth + "px");

                                    var values = new Array(288);
                                    var persons = list[k].time_slice_persons;
                                    for(var i = 0; i < values.length; ++i) {
                                        values[i] = 0;
                                    }
                                    for(var kk in persons) {
                                        values[kk] = persons[kk];
                                    }
                                    values = shrinkValues(48, values);

                                    horizon = horizon(selection, values, columnWidth);
                                }
                            }

                            function addTimeIndicatorLine(offset, row) {
                                var mCol = d3.select(row).selectAll(".tableData.stacked")[0][0];
                                var _transformValue = mCol.attributes["transform"].value;
                                var _splits = _transformValue.split(",")[0].split("(");
                                var colValue = parseFloat(_splits[1]);

                                var selection = horizonChart.append("div")
                                    .attr("class", "horizon_chart_timeIndicator")
                                    .style("position", "absolute")
                                    .style("top", parseFloat(d3.select("#lugui-wrapper")[0][0].offsetTop) - 17 + headerHeight + value - 125 - scrollTop+ "px")
                                    .style("left", parseFloat(d3.select("#lugui-wrapper")[0][0].offsetLeft) - 118 + 9 + colValue + offset*columnWidth + 118 + "px");


                                var scale = d3.scale.linear().domain([0, 24]).range([0, 236]);
                                var axis = d3.svg.axis()
                                    .scale(scale)
                                    .orient("right")
                                    .tickValues([0, 6, 12, 18, 24])
                                    .tickSize(6, 2);

                                var svg = selection.append("svg")
                                    .attr("width", 30)
                                    .attr("height", 256)
                                    .append("g")
                                    .attr("transform", "translate(0, 10)")
                                    .call(axis);
                            }

                            function shrinkValues(len, values) {
                                var _values = new Array(len);
                                var eleLen = Math.ceil(values.length / len);
                                for(var i = 0, k = 0; i < len; ++i) {
                                    var total = 0;
                                    for(var j = 0; j < eleLen; ++j) {
                                        if(k >= values.length) {
                                            break ;
                                        }

                                        total = total + values[k];
                                        ++k;
                                    }
                                    _values[i] = total;
                                }

                                return(_values);
                            }

                            function adjustHorizonChartTopPos(index, flag) {
                                for(var j = index+1; j < _rows.length; ++j) {
                                    var _d = d3.select(_rows[j]).datum();

                                    var transformValue = _rows[j].attributes["transform"].value;
                                    var splits = transformValue.split(",");
                                    var value = parseFloat(splits[1]);

                                    value = value + flag * horizonCharHeight;
                                    d3.select(_rows[j]).attr("transform", "translate(0,"+value+")");

                                    if(clicked[_d.id]) {
                                        horizonCharts.select(".horizon_chart_"+_d.id).selectAll(".horizon_chart")
                                            .style("top", offsetTop + headerHeight + value + "px");

                                        horizonCharts.select(".horizon_chart_"+_d.id).selectAll(".horizon_chart_timeIndicator")
                                            .style("top", offsetTop + headerHeight + value - 118 + "px");
                                    }
                                }
                            }

                        });

                }

                function setData(rawAllData) {
                    function loadDesc(desc) {  // load description
                        if (desc.data) {
                            loadDataImpl("m_n", desc, desc.data);
                        } else {
                            console.log("No Data!");
                        }
                    }

                    removeOrangeICON(orangeICONS);
                    clicked = {};
                    $("#horizon_charts").empty();


                    d3.select(".lu.lu-body").selectAll(".row").selectAll(".tableData.stacked").selectAll("rect").attr("height", 16);
                    //console.log();

                    var desc = constructDesc(rawAllData);
                    loadDesc(desc);

                    function removeOrangeICON(orangeICONS) {

                        for(var i in orangeICONS.sourceICONS) {
                            mapService.map.source.removeLayer(orangeICONS.sourceICONS[i]);
                        }
                        for(var i in orangeICONS.destICONS) {
                            mapService.map.dest.removeLayer(orangeICONS.destICONS[i]);
                        }

                        orangeICONS.sourceICONS = {};
                        orangeICONS.destICONS = {};
                    }
                }

                var mnColumnWidth = null;
                var mNum = 0;
                var nNum = 0;

                function constructDesc(rawAllData) {

                    var idWidth = 50;
                    var width = 500 - 50 - idWidth;

                    var m = 0;
                    var n = 0;
                    for(var i in rawAllData) {
                        var rawData = rawAllData[i].value;

                        var _m = rawData.m.length;
                        m = Math.max(m, _m);

                        var _n = rawData.n.length;
                        n = Math.max(n, _n);
                    }

                    mNum = m;
                    nNum = n;

                    mnColumnWidth = Math.floor(width / (m+n));

                    var desc = {};
                    desc.primaryKey = "id";
                    desc.columns = [];
                    desc.columns.push(stringModel("id"));
                    for(var i = 0; i < m; ++i) {
                        desc.columns.push(numberModel("m_"+i, [0, 1]));
                    }

                    for(var i = 0; i < n; ++i) {
                        desc.columns.push(numberModel("n_"+i, [0, 1]));
                    }


                    function stringModel(name) {
                        var objectModel = {
                            column: name,
                            type: "string"
                        };

                        return objectModel;
                    }

                    function numberModel(name, domain) {
                        var objectModel = {
                            column: name,
                            type: "number",
                            domain: domain
                        };

                        return objectModel;
                    }

                    desc.data = [];

                    var maxDayPersons = getMaxDayPersons(rawAllData);
                    var id = 0;
                    for(var i in rawAllData) {
                        var rawData = rawAllData[i].value;
                        var item = {};
                        item.id = parseInt(rawAllData[i].order);
                        item.temporary_id = id++;

                        var mList = rawData.m;
                        var mOpacity = [];
                        for (var k = 0; k < mList.length; ++k) {
                            item["m_" + k] = 1;
                            mOpacity.push(Math.max(0.2, rawData.m[k].day_persons/maxDayPersons));
                        }

                        var nList = rawData.n;
                        var nOpacity = [];
                        for (var k = 0; k < nList.length; ++k) {
                            item["n_" + k] = 1;
                            nOpacity.push(Math.max(0.2, rawData.n[k].day_persons/maxDayPersons));
                        }

                        item.mOpacity = mOpacity;
                        item.nOpacity = nOpacity;
                        item.m = rawData.m;
                        item.n = rawData.n;

                        desc.data.push(item);
                    }

                    desc.layout = {};
                    desc.layout.primary = [];

                    var mLayoutStack = layoutStackModel("m (Out)");
                    for(var i = 0; i < m; ++i) {
                        mLayoutStack.children.push(layoutColumnModel("m_"+i, mnColumnWidth));
                    }
                    desc.layout.primary.push(mLayoutStack);

                    var nLayoutStack = layoutStackModel("n (In)");
                    for(var i = 0; i < n; ++i) {
                        nLayoutStack.children.push(layoutColumnModel("n_"+i, mnColumnWidth));
                    }
                    desc.layout.primary.push(nLayoutStack);

                    function layoutColumnModel(name, width) {
                        var objectModel = {
                            column: name,
                            width: width
                        };

                        return objectModel;
                    }

                    function layoutStackModel(label) {
                        var objectModel = {
                            type: "stacked",
                            label: label,
                            children: []
                        };

                        return objectModel;
                    }

                    function getMaxDayPersons(rawAllData) {
                        var max = 0;

                        for(var i in rawAllData) {
                            var rawData = rawAllData[i].value;
                            for (var k in rawData.m) {
                                max = Math.max(max, rawData.m[k].day_persons);
                            }

                            for (var k in rawData.n) {
                                max = Math.max(max, rawData.n[k].day_persons);
                            }
                        }

                        return max;
                    }


                    //draw legend
                    d3.select("#legend_lineup svg").remove();

                    var margin = {left:15,right:15,top:25,bottom:40};
                    var legendTotalWidth = d3.select("#legend_lineup")[0][0].offsetWidth;
                    var legendWidth = legendTotalWidth/3;
                    //var legendWidth = d3.select("#legend_lineup")[0][0].offsetWidth;
                    var legendHeight = d3.select("#legend_lineup")[0][0].offsetHeight;
                    var legends = d3.select("#legend_lineup").append("svg").attr("id","legendOfLineup")
                        .attr("width",legendTotalWidth).attr("height",legendHeight);

                    var legend1 = legends.append("g").attr("transform","translate("+ "0"+",0)");
                    var legend2 = legends.append("g").attr("transform","translate("+(legendWidth)+",0)");
                    var legend3 = legends.append("g").attr("transform","translate("+(legendWidth*2)+",0)");

                    var colors = ['rgb(199,234,229)','rgb(128,205,193)','rgb(53,151,143)','rgb(1,102,94)'];
                    var labels = ['>0','>400','>800','>1200'];
                    var rectWidth = (legendWidth-margin.left-margin.right)/colors.length;

                    legend3.append("text").text("No. of people per hour").style("font-weight","bold")
                        .attr("transform","translate("+(margin.left) + "," + (margin.top-8)+")")
                        .style("font-size","8px");

                    var rectangle = legend3.selectAll(".legend").data(colors).enter()
                        .append("g").attr("class","legend");

                    rectangle.append("rect").attr("width",rectWidth)
                        .attr("height",legendHeight-margin.top-margin.bottom)
                        .attr("transform",function(d,i){return "translate("+(margin.left+i*rectWidth) + "," + margin.top+")"})
                        .attr("fill",function(d){return d});
                    rectangle.append("text").text(function(d,i){return labels[i]})
                        .attr("transform",function(d,i){return "translate("+(margin.left+i*rectWidth) + "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")"})
                        .style("font-size","8px");

                    var color1 = "purple",color2 = "orange";

                    var gradient1 = legend1.append("g").append("svg:defs").append("svg:linearGradient")
                        .attr("id", "gradient10").attr("x1", "0%").attr("y1", "0%").attr("x2", "100%").attr("y2", "0%").attr("spreadMethod", "pad");
                    gradient1.append("svg:stop").attr("offset", "0%").attr("stop-color", color1).attr("stop-opacity", 0.2);
                    gradient1.append("svg:stop").attr("offset", "100%").attr("stop-color", color1).attr("stop-opacity", 1);

                    legend1.append("g").append("rect").attr("width",legendWidth-margin.left-margin.right).attr("height",legendHeight-margin.top-margin.bottom).attr("transform","translate("+margin.left + "," + margin.top+")")
                        .attr("fill","url(#gradient10)");
                    legend1.append("text").text("No. of people (Out)").style("font-size","8px").style("font-weight","bold").style("text-anchor","start").attr("transform","translate("+margin.left + "," + (margin.top-8)+")");
                    legend1.append("text").text("0").style("font-size","8px").style("text-anchor","start").attr("transform","translate("+margin.left + "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")");
                    legend1.append("text").text(maxDayPersons).style("text-anchor","end").style("font-size","8px").attr("transform","translate("+(legendWidth-margin.right)+ "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")");


                    var gradient2 = legend2.append("g").append("svg:defs").append("svg:linearGradient")
                        .attr("id", "gradient11").attr("x1", "0%").attr("y1", "0%").attr("x2", "100%").attr("y2", "0%").attr("spreadMethod", "pad");
                    gradient2.append("svg:stop").attr("offset", "0%").attr("stop-color", color2).attr("stop-opacity", 0.2);
                    gradient2.append("svg:stop").attr("offset", "100%").attr("stop-color", color2).attr("stop-opacity", 1);

                    legend2.append("g").append("rect").attr("width",legendWidth-margin.left-margin.right).attr("height",legendHeight-margin.top-margin.bottom).attr("transform","translate("+margin.left + "," + margin.top+")")
                        .attr("fill","url(#gradient11)");
                    legend2.append("text").text("No. of people (In)").style("font-size","8px").style("font-weight","bold").style("text-anchor","start").attr("transform","translate("+margin.left + "," + (margin.top-8)+")");
                    legend2.append("text").text("0").style("font-size","8px").style("text-anchor","start").attr("transform","translate("+margin.left + "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")");
                    legend2.append("text").text(maxDayPersons).style("text-anchor","end").style("font-size","8px").attr("transform","translate("+(legendWidth-margin.right)+ "," + (legendHeight-margin.bottom + legendHeight-margin.top-margin.bottom)+")");


                    return desc;
                }

                var orangeICONS = {
                    sourceICONS: [],
                    destICONS: []
                };

                var clicked = {};

                LineUp.setData = setData;
                lineUpService.lineUp = LineUp;
            }

        }
    }
}]);
'use strict';

angular.module('lineUp').factory('lineUpService',
    function() {
        var lineUpService = {};

        return lineUpService;
    }
);
'use strict';

angular.module('map').controller('mapController', ['$scope', 'mapService', 'dataService', 'dbService', 'pipService',
    function($scope, mapService, dataService, dbService, pipService) {
        $scope.changeSourceControlParameters = changeSourceControlParameters;
        $scope.changeDestControlParameters = changeDestControlParameters;

        mapService.loadData = loadData;

        mapService.getCooccurrence = getCooccurrence;

        pipService.onDateListChange($scope, function() {
            mapService.loadData();
        });

        function loadData() {
            mapService.clear();

            var dateList = dataService.getDateList();

            var names = [
                "/map/info",
                '/map/heatmap/source',
                '/map/heatmap/destination'
            ];

            var parasList = [
                {
                },
                {
                    dateList: dateList
                }, {
                    dateList: dateList
                }

            ];

            dbService.posts(names, parasList, function(dataList) {
                mapService.data = {
                    info: dataList[0],
                    heatMap:  {
                        source: dataList[1],
                        dest: dataList[2]
                    },
                    map: {
                        source: undefined,
                        dest: undefined
                    },
                    d3Overlay: {
                        sel: {
                            source: undefined,
                            dest: undefined
                        },
                        proj: {
                            source: undefined,
                            dest: undefined
                        }
                    },
                    circles: {
                        source: undefined,
                        dest: undefined
                    }
                };

                pipService.emitMapDataChange(mapService.data);
            });
        }

        // get coocurrence data
        function getCooccurrence(selectedSource, selectedDest, callback) {
            var dateList = dataService.getDateList();
            var hours = dataService.getHours();
            var name = "/map/cooccurrence";

            var paras = {
                dateList: dateList,
                hours: hours,
                selectedSource: selectedSource,
                selectedDest: selectedDest
            };

            dbService.post(name, paras, function(data) {
                callback(data);
            });
        }

        function changeSourceControlParameters() {
            var circleSizeFilter = $(".source-circle-size-filter")[0];
            mapService.filterSourceCircles(+(circleSizeFilter.value));
        }

        function changeDestControlParameters() {
            var destCircleSizeIndexDoms = $(".dest_circle_size_index");
            for(var i in destCircleSizeIndexDoms) {
                if(destCircleSizeIndexDoms[i].checked) {
                    mapService.destCircleSizeIndex = destCircleSizeIndexDoms[i].value;

                    break;
                }
            }

            mapService.setDestCircleSize(mapService.destCircleSizeIndex);

            var circleSizeFilter = $(".dest-circle-size-filter")[0];
            mapService.filterDestCircles(+(circleSizeFilter.value));
        }
    }


]);
'use strict';

angular.module('map').directive('mapDirective', ["mapService", 'dataService', 'funcService', 'pipService', 'radialGraphService', 'matrixService', function(mapService, dataService, funcService, pipService, radialGraphService, matrixService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            mapService.clear = clear;
            setHourRangeSlider();
            mapService.loadData();

            pipService.onMapDataChange(scope, function() {
                setMap();
            });

            pipService.onHoursChange(scope, function(hours) {
                $("#source_slider-range").slider({
                    values: [hours[0], hours[hours.length-1]]
                });

                $("#dest_slider-range").slider({
                    values: [hours[0], hours[hours.length-1]]
                });

                mapService.map.source.setHours(hours);
                mapService.map.dest.setHours(hours);

                mapService.updateCircles();
            });

            // clear map
            function clear() {
                if(mapService.map.source) {
                    mapService.map.source.remove();
                }
                if(mapService.map.dest) {
                    mapService.map.dest.remove();
                }

                $('#source_map').empty();
                $('#dest_map').empty();
            }

            // init hour range slider
            function setHourRangeSlider() {
                $(function () {
                    $("#source_slider-range").slider({
                        range: true,
                        min: 0,
                        max: 23,
                        values: [ 0, 23 ],
                        step: 1,
                        slide: function (event, ui) {
                            var start = ui.values[ 0 ];
                            var end  = ui.values[ 1 ];

                            $("#source_hour_range").val(start + " - " + end);

                            var hours = d3.range(start, end+1, 1);
                            dataService.setHours(hours);
                        }
                    });
                    $("#source_hour_range")
                        .val($("#source_slider-range").slider("values", 0) +
                        " - " + $("#source_slider-range").slider("values", 1));
                });

                $(function () {
                    $("#dest_slider-range").slider({
                        range: true,
                        min: 0,
                        max: 23,
                        values: [ 0, 23 ],
                        step: 1,
                        slide: function (event, ui) {
                            var start = ui.values[ 0 ];
                            var end  = ui.values[ 1 ];

                            $("#dest_hour_range").val(ui.values[ 0 ] + " - " + ui.values[ 1 ]);

                            var hours = d3.range(start, end+1, 1);
                            dataService.setHours(hours);
                        }
                    });
                    $("#dest_hour_range").val($("#dest_slider-range").slider("values", 0) +
                        " - " + $("#dest_slider-range").slider("values", 1));
                });
            }

            // set map
            function setMap() {
                mapService.updateCircles = updateCircles;

                var maxCircleR = 12;
                var destCircleSizeIndex = "regionNum";

                var green_marker_icon = L.icon({
                    iconUrl: 'lib/leaflet/dist/images/marker-icon-green.png',
                    iconRetinaUrl: 'lib/leaflet/dist/images/marker-icon-green-2x.png',
                    iconSize: [25, 40],
                    iconAnchor: [12.5, 35],
                    popupAnchor: [0, -32],
                    shadowUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowRetinaUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowSize: [41, 36],
                    shadowAnchor: [12.5, 35]
                });

                var purple_marker_icon = L.icon({
                    iconUrl: 'lib/leaflet/dist/images/marker-icon-purple.png',
                    iconRetinaUrl: 'lib/leaflet/dist/images/marker-icon-purple-2x.png',
                    iconSize: [25, 40],
                    iconAnchor: [12.5, 40],
                    popupAnchor: [0, -36],
                    shadowUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowRetinaUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowSize: [41, 41],
                    shadowAnchor: [12.5, 40]
                });

                var yellow_marker_icon = L.icon({
                    iconUrl: 'lib/leaflet/dist/images/marker-icon-yellow.png',
                    iconRetinaUrl: 'lib/leaflet/dist/images/marker-icon-yellow-2x.png',
                    iconSize: [25, 40],
                    iconAnchor: [12.5, 40],
                    popupAnchor: [0, -36],
                    shadowUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowRetinaUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowSize: [41, 41],
                    shadowAnchor: [12.5, 40]
                });

                var red_marker_icon = L.icon({
                    iconUrl: 'lib/leaflet/dist/images/marker-icon-red.png',
                    iconRetinaUrl: 'lib/leaflet/dist/images/marker-icon-red-2x.png',
                    iconSize: [25, 40],
                    iconAnchor: [12.5, 40],
                    popupAnchor: [0, -36],
                    shadowUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowRetinaUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowSize: [41, 41],
                    shadowAnchor: [12.5, 40]
                });

                // map options
                var sourceOptions = {
                    heatmapOptions: {
                        minOpacity: 0,
                        maxZoom: 30,
                        minZoom:5,
                        max: 0.0003,
                        radius: 18,
                        blur:20,
                        gradient: {
                            0:"#fff5eb",
                            0.2:"#fee6ce",
                            0.4: "#fdd0a2",
                            0.6:"#fdae6b",
                            0.8:"#fd8d3c",
                            1:"#e31a1c"
                        }
                    }
                };
                var sourceMapDat = constructMapDat(mapService.data.info, mapService.data.heatMap.source);
                mapService.data.map.source = sourceMapDat;
                var sourceMap = constructSourceMap(sourceMapDat, mapService.data.heatMap.source, sourceOptions);
                mapService.map.source = sourceMap;

                var destOptions = {
                    heatmapOptions: {
                        minOpacity: 0,
                        maxZoom: 30,
                        minZoom: 5,
                        max: 0.0002,
                        radius: 15, //18
                        blur:15,//20
                        gradient: {
                            0:"#fff5eb",
                            0.2:"#fee6ce",
                            0.4: "#fdd0a2",
                            0.6:"#fdae6b",
                            0.8:"#fd8d3c",
                            1:"#e31a1c"
                        }
                    }
                };
                var destMapDat = constructMapDat(mapService.data.info, mapService.data.heatMap.dest);
                mapService.data.map.dest = destMapDat;
                var destMap = constructDestMap(destMapDat, mapService.data.heatMap.dest, destOptions);
                mapService.map.dest = destMap;

                function updateCircles() {
                    if(mapService.map.source.pinnedNodes.length == 0 && mapService.map.dest.pinnedNodes.length == 0) {
                        drawCircles({}, {});
                    } else {
                        mapService.getCooccurrence(mapService.map.source.pinnedNodes, mapService.map.dest.pinnedNodes, function (data) {
                            var sourceCircles = data.sourceCircles;
                            var destCircles = data.destCircles;

                            drawCircles(sourceCircles, destCircles);
                        });
                    }
                }

                // draw coocurrence circles
                function drawCircles(sourceCircles, destCircles) {
                    mapService.filterSourceCircles = filterSourceCircles;
                    mapService.filterDestCircles = filterDestCircles;

                    mapService.data.circles.source = sourceCircles;
                    mapService.data.circles.dest = destCircles;


                    var sourceCirclesList = _.values(sourceCircles);
                    var destCirclesList = _.values(destCircles);

                    mapService.setDestCircleSize = setDestCircleSize;

                    var mapDat = mapService.data.info;

                    var sourceCirclesMaxR = _.max(sourceCirclesList, function(d) {
                        return d.r;
                    }).r;
                    if(sourceCirclesMaxR != 0) {
                        sourceCirclesList.forEach(function (d) {
                            d.r = d.r / sourceCirclesMaxR * maxCircleR;
                        });
                    }

                    var destCirclesMaxR = _.max(destCirclesList, function(d) {
                        return d.r;
                    }).r;
                    if(destCirclesMaxR != 0) {
                        destCirclesList.forEach(function (d) {
                            d.r = d.r / destCirclesMaxR * maxCircleR;
                        });
                    }

                    var overlaySelSource = mapService.data.d3Overlay.sel.source;
                    var overlaySelDest = mapService.data.d3Overlay.sel.dest;

                    var overlayProjSource = mapService.data.d3Overlay.proj.source;
                    var overlayProjDest = mapService.data.d3Overlay.proj.dest;

                    overlaySelSource.selectAll("circle").remove();
                    overlaySelDest.selectAll("circle").remove();

                    var co_nodes = overlaySelSource.selectAll("circle")
                        .data(sourceCirclesList)
                        .enter()
                        .append("circle")
                        .attr("class", "related_node")
                        .attr("r", function (d) {
                            return d.r;
                        })
                        .attr("transform", function (d) {
                            var point = overlayProjSource.latLngToLayerPoint([mapDat[d.id].gps.lat, mapDat[d.id].gps.lng]);

                            return "translate(" + point.x + "," + point.y + ")";
                        })
                        .attr("stroke", "grey")
                        .attr("stroke-width", 0.6)
                        .attr("stroke-opacity",0.6)
                        .attr("fill", "#ffffcc")
                        .attr("fill-opacity", "0.8");

                    var dest_nodes = overlaySelDest.selectAll("circle")
                        .data(destCirclesList)
                        .enter()
                        .append("circle")
                        .attr("class", "related_node")
                        .attr("r", function (d) {
                            return d.r;
                        })
                        .attr("transform", function (d) {
                            var point = overlayProjDest.latLngToLayerPoint([mapDat[d.id].gps.lat, mapDat[d.id].gps.lng]);

                            return "translate(" + point.x + "," + point.y + ")";
                        })
                        .attr("stroke", "grey")
                        .attr("stroke-width", 0.6)
                        .attr("stroke-opacity",0.6)
                        .attr("fill", "#ffffcc")
                        .attr("fill-opacity", "0.8");

                    var packerSource = sm.packer();
                    packerSource.elements(overlaySelSource.selectAll("circle")[0]).start();

                    var packerDest = sm.packer();
                    packerDest.elements(overlaySelDest.selectAll("circle")[0]).start();

                    function setDestCircleSize(index) {
                        destCircleSizeIndex = index;

                        for(var i in destCirclesList) {
                            destCirclesList[i].r = destCirclesList[i][index];
                        }

                        var destCirclesMaxR = _.max(destCirclesList, function(d) {
                            return d.r;
                        }).r;
                        if(destCirclesMaxR > 0) {
                            destCirclesList.forEach(function (d) {
                                d.r = d.r / destCirclesMaxR * maxCircleR;
                            });
                        }

                        mapService.data.d3Overlay.sel.dest
                            .selectAll("circle")
                            .attr("r", function(d) {
                                return d.r;
                            });

                        packerDest.start();
                    }

                    function filterSourceCircles(leastNum) {
                        mapService.data.d3Overlay.sel.source
                            .selectAll("circle")
                            .attr("display", function(d) {
                                return d.regionNum>=leastNum?'':'none';
                            });
                        packerSource.start();
                    }

                    function filterDestCircles(leastNum) {
                        mapService.data.d3Overlay.sel.dest
                            .selectAll("circle")
                            .attr("display", function(d) {
                                return d[mapService.destCircleSizeIndex]>=leastNum?'':'none';
                            });
                        packerDest.start();
                    }
                }

                function constructSourceMap(mapDat, heatMapDat, options) {
                    var containerText = "source_map";

                    var map = L.map(containerText, {
                        // Guang zhou [23.13692, 113.33064]
                        // Shang hai [31.22, 121.48]
                        center: [31.22, 121.48],
                        zoom: 11,
                        maxZoom:16,
                        minZoom:10,
                        doubleClickZoom: false
                    });
                    map.addGreenMarker = addGreenMarker;
                    map.addPurpleMarker = addPurpleMarker;
                    map.addBlueMarkerNid = addBlueMarkerNid;

                    function addGreenMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: green_marker_icon}).addTo(map);
                    }

                    function addPurpleMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: purple_marker_icon}).addTo(map);
                    }

                    function addBlueMarkerNid(nid) {
                        return L.marker([mapDat[nid].gps.lat, mapDat[nid].gps.lng]).addTo(map)
                            .bindPopup("Cell Id: " + nid + "</br>" +
                                "Latitude: " + mapDat[nid].gps.lat + "</br>" +
                                "Longitude: " + mapDat[nid].gps.lng);
                    }

                    // add an OpenStreetMap tile layer
                    var HERE_normalDay = L.tileLayer('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/maptile/{mapID}/normal.day/{z}/{x}/{y}/256/png8?app_id={app_id}&app_code={app_code}', {
                        attribution: 'Map &copy; 1987-2014',
                        subdomains: '1234',
                        mapID: 'newest',
                        app_id: 'Y8m9dK2brESDPGJPdrvs',
                        app_code: 'dq2MYIvjAotR8tHvY8Q_Dg',
                        base: 'base',
                        minZoom: 0,
                        maxZoom: 20
                    }).addTo(map);

                    var heatmapDataOfDay = generateHeatData(d3.range(24), mapDat, heatMapDat);
                    var heat = L.heatLayer(heatmapDataOfDay, options.heatmapOptions).addTo(map);


                    map.on('zoomend', function() {
                        var currentZoom = map.getZoom();
                        heat.setOptions({radius:(1.5*currentZoom),blur:((5/3)*currentZoom)});
                    });

                    var marker = undefined;
                    var isMarker = false;

                    var pinnedMarker = [];
                    map.pinnedNodes = [];

                    var d3Overlay = L.d3SvgOverlay(function(sel, proj) {
                        mapService.data.d3Overlay.sel.source = sel;
                        mapService.data.d3Overlay.proj.source = proj;
                    });
                    d3Overlay.addTo(map);

                    $("#source_marker_control").bind("click", setIsMarker);
                    $("#source_reset").bind("click", reset);

                    map.on({
                        // marker follows mouse
                        mousemove: function(e) {
                            if(!isMarker) {
                                return ;
                            }

                            if(marker) {
                                map.removeLayer(marker);
                                marker = undefined;
                            }

                            var min = 1e9;
                            var minID = -1;
                            for(var i in mapDat) {
                                if(map.pinnedNodes.length > 0 && mapService.data.circles.source && !mapService.data.circles.source.hasOwnProperty(i)) {
                                    continue ;
                                }

                                var dis = funcService.getDistance([e.latlng.lat, e.latlng.lng], [mapDat[i].gps.lat, mapDat[i].gps.lng]);
                                if(dis < min) {
                                    min = dis;
                                    minID = i;
                                }
                            }

                            if(minID != -1) {
                                marker = addBlueMarkerNid(minID);

                                marker.on({
                                    dblclick: function () {
                                        map.pinnedNodes.push(+minID);
                                        pinnedMarker.push(marker);
                                        marker = undefined;

                                        mapService.updateCircles();

                                        if(map.pinnedNodes.length >= 2) {
                                            var sources = map.pinnedNodes.slice(-2);
                                            pipService.emitFlowMapSourceChange(sources);
                                        }

                                        matrixService.setSelectedSource(map.pinnedNodes);
                                    }
                                });
                            }
                        }
                    });

                    function setIsMarker() {
                        isMarker = !isMarker;

                        if(!isMarker && marker) {
                            map.removeLayer(marker);
                            marker = undefined;
                        }
                    }

                    // reset
                    function reset() {
                        for(var i in pinnedMarker) {
                            var _marker = pinnedMarker[i];
                            map.removeLayer(_marker);
                        }

                        pinnedMarker = [];
                        map.pinnedNodes = [];

                        mapService.data.d3Overlay.sel.source.selectAll("circle").remove();
                        mapService.data.d3Overlay.sel.dest.selectAll("circle").remove();

                        pipService.emitFlowMapSourceChange(map.pinnedNodes);

                        matrixService.setSelectedSource(map.pinnedNodes);
                        destMap.reset();
                    }

                    map.setHours = setHours;
                    // set hours
                    function setHours(hours) {
                        if(heat != null) {
                            map.removeLayer(heat);
                            heat = null;
                        }

                        if(hours.length >= 24) {
                            heat = L.heatLayer(heatmapDataOfDay, options.heatmapOptions).addTo(map);

                            return ;
                        }

                        var heatmapData = generateHeatData(hours, mapDat, heatMapDat);
                        heat = L.heatLayer(heatmapData, options.heatmapOptions).addTo(map);
                    }

                    return map;
                }


                function constructDestMap(mapDat, heatMapDat, options) {
                    var containerText = "dest_map";

                    var map = L.map(containerText, {
                        // Guang zhou [23.13692, 113.33064]
                        // Shang hai [31.22, 121.48]
                        center: [31.22, 121.48],
                        zoom: 11,
                        maxZoom: 16,
                        minZoom: 10,
                        doubleClickZoom: false
                    });

                    map.addGreenMarker = addGreenMarker;
                    map.addGreenMarkerNid = addGreenMarkerNid;
                    map.addYellowMarker = addYellowMarker;
                    map.addRedMarker = addRedMarker;
                    map.addRedMarkerNid = addRedMarkerNid;
                    map.addBlueMarkerNid = addBlueMarkerNid;

                    function addGreenMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: green_marker_icon}).addTo(map);
                    }

                    function addGreenMarkerNid(nid) {
                        return L.marker([mapDat[nid].gps.lat, mapDat[nid].gps.lng], {icon: green_marker_icon}).addTo(map)
                            //.bindPopup(minID + " " + mapDat[minID].poi);
                            .bindPopup("Cell Id: " + nid + "</br>" +
                                "Latitude: " + mapDat[nid].gps.lat + "</br>" +
                                "Longitude: " + mapDat[nid].gps.lng //+ "</br>" +
                            //"Ref. POI: " + mapDat[minID].poi
                        );
                    }

                    function addYellowMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: yellow_marker_icon}).addTo(map);
                    }

                    function addRedMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: red_marker_icon}).addTo(map);
                    }

                    function addRedMarkerNid(nid) {
                        return L.marker([mapDat[nid].gps.lat, mapDat[nid].gps.lng], {icon: red_marker_icon}).addTo(map);
                    }

                    function addBlueMarkerNid(nid) {
                        return L.marker([mapDat[nid].gps.lat, mapDat[nid].gps.lng]).addTo(map)
                            //.bindPopup(minID + " " + mapDat[minID].poi);
                            .bindPopup("Cell Id: " + nid + "</br>" +
                                "Latitude: " + mapDat[nid].gps.lat + "</br>" +
                                "Longitude: " + mapDat[nid].gps.lng //+ "</br>" +
                            //"Ref. POI: " + mapDat[minID].poi
                        );
                    }

                    var HERE_normalDay = L.tileLayer('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/maptile/{mapID}/normal.day/{z}/{x}/{y}/256/png8?app_id={app_id}&app_code={app_code}', {
                        attribution: 'Map &copy; 1987-2014',
                        subdomains: '1234',
                        mapID: 'newest',
                        app_id: 'Y8m9dK2brESDPGJPdrvs',
                        app_code: 'dq2MYIvjAotR8tHvY8Q_Dg',
                        base: 'base',
                        minZoom: 0,
                        maxZoom: 20
                    }).addTo(map);

                    var heatmapDataOfDay = generateHeatData(d3.range(24), mapDat, heatMapDat);
                    var heat = L.heatLayer(heatmapDataOfDay, options.heatmapOptions).addTo(map);

                    map.on('zoomend', function() {
                        var currentZoom = map.getZoom();
                        //console.log(currentZoom);
                        heat.setOptions({radius:(1.5*currentZoom),blur:((5/3)*currentZoom)});
                    });

                    var marker = undefined;
                    var isMarker = false;
                    var icon = "default";

                    var state = null;

                    var pinnedMarker = [];
                    map.pinnedNodes = [];

                    var d3Overlay = L.d3SvgOverlay(function(sel, proj) {
                        mapService.data.d3Overlay.sel.dest = sel;
                        mapService.data.d3Overlay.proj.dest = proj;
                    });
                    d3Overlay.addTo(map);

                    $("#dest_marker_control").bind("click", setIsMarker);
                    $("#dest_radial_graph_control").bind("click", setIsSelectRadialGraph);
                    $("#dest_reset").bind("click", reset);

                    map.on({
                        mousemove: function(e) {
                            if(!isMarker) {
                                return ;
                            }

                            if(marker) {
                                map.removeLayer(marker);
                                marker = undefined;
                            }

                            var min = 1e9;
                            var minID = -1;

                            for(var i in mapDat) {
                                if(state == "pin" && sourceMap.pinnedNodes.length == 0) {
                                    continue ;
                                }

                                if(state == "pin" && sourceMap.pinnedNodes.length > 0 && mapService.data.circles.dest && !mapService.data.circles.dest.hasOwnProperty(i)) {
                                    continue ;
                                }

                                var dis = funcService.getDistance([e.latlng.lat, e.latlng.lng], [mapDat[i].gps.lat, mapDat[i].gps.lng]);
                                if(dis < min) {
                                    min = dis;
                                    minID = i;
                                }
                            }

                            if(minID != -1) {
                                if (icon == "default") {
                                    marker = addBlueMarkerNid(minID);
                                } else {
                                    marker = addGreenMarkerNid(minID);
                                }

                                marker.on({
                                    click: function () {
                                        if (state == "radial_graph") {
                                            radialGraphService.loadData(minID);
                                        }
                                    },
                                    dblclick: function () {
                                        if (state != "pin") {
                                            return;
                                        }

                                        addPinnedNodes(minID, marker);
                                        marker = undefined;
                                    }
                                });
                            }
                        }
                    });

                    map.addPinnedNodes = addPinnedNodes;
                    function addPinnedNodes(nid, marker) {
                        map.pinnedNodes.push(+nid);
                        pinnedMarker.push(marker);
                        mapService.updateCircles();

                        matrixService.setSelectedDest(map.pinnedNodes);
                    }

                    function setIsMarker() {
                        if(state != "radial_graph") {
                            isMarker = !isMarker;
                        }

                        if(marker) {
                            map.removeLayer(marker);
                            marker = undefined;
                        }

                        icon = "default";
                        if(isMarker) {
                            state = "pin";
                        } else {
                            state = null;
                        }
                    }

                    function setIsSelectRadialGraph() {
                        if(state != "pin") {
                            isMarker = !isMarker;
                        }

                        if(marker) {
                            map.removeLayer(marker);
                            marker = undefined;
                        }

                        icon = green_marker_icon;
                        if(isMarker) {
                            state = "radial_graph";
                        } else {
                            state = null;
                        }
                    }

                    map.reset = reset;
                    function reset() {
                        for(var i in pinnedMarker) {
                            var _marker = pinnedMarker[i];
                            map.removeLayer(_marker);
                        }

                        pinnedMarker = [];
                        map.pinnedNodes = [];

                        matrixService.setSelectedDest(map.pinnedNodes);

                        mapService.updateCircles();
                    }

                    map.setHours = setHours;
                    function setHours(hours) {
                        if(heat != null) {
                            map.removeLayer(heat);
                            heat = null;
                        }

                        if(hours.length >= 24) {
                            heat = L.heatLayer(heatmapDataOfDay, options.heatmapOptions).addTo(map);

                            return ;
                        }

                        var heatmapData = generateHeatData(hours, mapDat, heatMapDat);
                        heat = L.heatLayer(heatmapData, options.heatmapOptions).addTo(map);
                    }

                    return map;
                }

                // generate data of heat map
                function generateHeatData(hours, mapDat, heatMapDat) {
                    var data = [];
                    var max = 0;
                    for(var i in heatMapDat) {
                        var sourceList = [];

                        var node = heatMapDat[i];

                        for (var j in node) {
                            var sliceTime = j;

                            if (!_.contains(hours, +sliceTime)) {
                                continue;
                            }

                            sourceList = _.union(sourceList, node[j]);
                        }

                        var value = sourceList.length;
                        max = Math.max(max, value);

                        var datum = [
                            mapDat[i].gps.lat,
                            mapDat[i].gps.lng,
                            value
                        ];
                        data.push(datum);
                    }


                    if(max > 0) {
                        for (var i in data) {
                            var datum = data[i];
                            datum[2] = datum[2] / max;
                        }
                    }

                    return data;
                }

                function constructMapDat(allMapDat, heatMapDat) {
                    var mapDat = _.mapObject(heatMapDat, function(value, key) {
                        return allMapDat[key];
                    });

                    return mapDat;
                }
            }

        }
    }
}]);
'use strict';

angular.module('map').factory('mapService',
    function() {
        var mapService = {
            map: {},
            destCircleSizeIndex: 'regionNum'
        };

        return mapService;
    }
);
'use strict';

angular.module('matrix').controller('matrixController', ['$scope', 'matrixService', 'dataService', 'dbService', 'pipService',
    function($scope, matrixService, dataService, dbService, pipService) {
        $scope.refresh = refresh;
        $scope.switchIsLegend = switchIsLegend;

        matrixService.loadData = loadData;
        matrixService.loadDataHours = loadDataHours;
        matrixService.testCellsDataDeeply = testCellsDataDeeply;

        pipService.onDateListChange($scope, function() {
            loadData();
        });

        pipService.onHoursChange($scope, function() {
            loadDataHours();
        });

        function refresh() {
            matrixService.filterMatrixCell();
        }

        function switchIsLegend() {
            matrixService.switchIsLegend();
        }

        function loadData() {
            matrixService.clear();

            var dateList = dataService.getDateList();

            var name = '/matrix/day';
            var paras = {
                dateList: dateList
            };

            dbService.post(name, paras, function(data) {
                matrixService.data = data;

                matrixService.constructMatrixGraph(data);
            });
        }

        function loadDataHours() {
            matrixService.clear();

            var hours = dataService.getHours();

            if(hours.length >= 24) {
                loadData();

                return ;
            }

            var dateList = dataService.getDateList();

            var name = '/matrix/hour';
            var paras = {
                dateList: dateList,
                hours: hours
            };

            dbService.post(name, paras, function(data) {
                matrixService.data = data;

                matrixService.constructMatrixGraph(data);
            });
        }

        // test cell data deeply, i.e. test lineup data
        function testCellsDataDeeply(testList, selectedSourceList, selectedDestList, config) {
            if(testList.length <= 0) {
                return ;
            }

            var dateList = dataService.getDateList();

            testCellDataDeeply(0);

            function testCellDataDeeply(index) {
                var m = testList[index].m;
                var n = testList[index].n;
                var dom = testList[index].dom;

                var name = '/matrix/test';
                var paras = {
                    dateList: dateList,
                    m_n: m+'_'+n,
                    selectedSource: selectedSourceList,
                    selectedDest: selectedDestList
                };

                dbService.post(name, paras, function(res) {
                    if(res.flag) {
                        d3.select(dom).attr("fill", config.cell.color.normal);
                    } else {
                        d3.select(dom).attr("fill", config.cell.color.unavailable);
                    }

                    if(index+1 < testList.length) {
                        testCellDataDeeply(index+1);
                    }
                });
            }
        }
    }
]);
'use strict';

angular.module('matrix').directive('matrixDirective', ["matrixService", 'configService', 'pipService', function(matrixService, configService, pipService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            var config = configService.matrixGraph;

            var isLegend = true;

            var tooltip = drawTooltip();

            matrixService.switchIsLegend = switchIsLegend;
            matrixService.constructMatrixGraph = constructMatrixGraph;
            matrixService.clear = clear;

            matrixService.loadData();

            function switchIsLegend() {
                isLegend = !isLegend;
                d3
                    .select("#legend_matrix")
                    .style("display", isLegend ? "block" : "none");
            }

            function drawTooltip() {
                var tooltip = d3.select("body")
                    .append("div")
                    .attr("id", "tooltip")
                    .attr("class", "tooltip right")
                    .style("position", "absolute")
                    .style("z-index", "10")
                    .style("line-height", "20px")
                    .style("visibility", "hidden");

                tooltip
                    .append("div")
                    .attr("class", "tooltip-arrow");

                return tooltip;
            }

            function clear() {
                $("#matrix_graph").empty();
                $("#legend_matrix").empty();
            }

            function constructMatrixGraph(data) {
                matrixService.setSelectedSource = setSelectedSource;
                matrixService.setSelectedDest = setSelectedDest;
                matrixService.filterMatrixCell = filterMatrixCell;

                clear();
                var matrixSVG = d3.select("#matrix_graph").append("svg");

                var width = config.size.width;
                var height = config.size.height;

                var cellSize = Math.min(width/data.max_n, height/data.max_m);
                var cellGap = config.cell.gap;

                var selectedCell = {};
                var selectedSource = d3.set();
                var selectedDest = d3.set();

                var isClick = true;

                // x and y scale of matrix cells
                var x = d3.scale.linear()
                    .domain([1, data.max_n])
                    .range([0, (cellSize+cellGap)*(data.max_n-1)]);

                var y = d3.scale.linear()
                    .domain([1, data.max_m])
                    .range([0, (cellSize+cellGap)*(data.max_m-1)]);

                var zoom = d3.behavior.zoom()
                    .x(x)
                    .y(y)
                    .scaleExtent(config.zoomScaleExtent)
                    .on("zoom", zoomed);

                matrixSVG
                    .attr("width", width)
                    .attr("height", height)
                    .call(zoom);


                var contentMessage = tooltip
                    .append("div")
                    .attr("class", "tooltip-inner").
                    style("text-align", "left");

                var dataList = _.map(data.matrix, function(v, m_n) {
                    var m = +m_n.split('_')[0];
                    selectedCell[m] = d3.set();
                    var n = +m_n.split('_')[1];

                    return {
                        "m":m,
                        "n":n,
                        "frequency":v.m_n_num,
                        "source": v.source,
                        "destination": v.destination,
                        selected: false
                    };
                });

                var maxFrequency = d3.max(dataList, function(d) {
                    return d.frequency;
                });

                // opacity scale of cells
                var oScale = d3.scale.linear()
                    .domain([0, Math.log(maxFrequency)])
                    .range(config.cell.opacityRange);

                var matrixCellGroup = matrixSVG.append("g")
                    .attr("class", "matrix_cell_group");

                drawLegend();
                drawMatrix();

                function zoomed() {
                    var scale = zoom.scale();

                    matrixCellGroup.selectAll(".matrix_cell")
                        .attr("width", cellSize*scale)
                        .attr("height", cellSize*scale)
                        .attr("x", function (d) {
                            return x(d.n);
                        })
                        .attr("y", function (d) {
                            return y(d.m);
                        });
                }

                function drawLegend() {
                    var legendConfig = config.legend;

                    var margin = legendConfig.margin;
                    var legendWidth = d3.select("#legend_matrix")[0][0].offsetWidth;
                    var legendHeight = d3.select("#legend_matrix")[0][0].offsetHeight;
                    var legend = d3.select("#legend_matrix")
                        .append("svg")
                        .attr("id", "legendOfMatrix")
                        .attr("width", legendWidth)
                        .attr("height", legendHeight);

                    var gradient = legend.append("g").append("svg:defs")
                        .append("svg:linearGradient")
                        .attr("id", "gradient1")
                        .attr("x1", "0%")
                        .attr("y1", "0%")
                        .attr("x2", "100%")
                        .attr("y2", "0%")
                        .attr("spreadMethod", "pad");

                    gradient.append("svg:stop")
                        .attr("offset", "0%")
                        .attr("stop-color", config.cell.color.normal)
                        .attr("stop-opacity", legendConfig.startOpacity);

                    gradient.append("svg:stop")
                        .attr("offset", "100%")
                        .attr("stop-color", config.cell.color.normal)
                        .attr("stop-opacity", legendConfig.endOpacity);

                    legend
                        .append("g")
                        .append("rect")
                        .attr("width", legendWidth - margin.left - margin.right)
                        .attr("height", legendHeight - margin.top - margin.bottom)
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                        .attr("fill", "url(#gradient1)");

                    var textSize = legendConfig.textSize;

                    legend
                        .append("text")
                        .text("No. of biclusters")
                        .style("font-size", textSize + "px")
                        .style("font-weight", "bold")
                        .style("text-anchor", "start")
                        .attr("transform", "translate(" + margin.left + "," + (margin.top - textSize) + ")");

                    legend
                        .append("text")
                        .text("0")
                        .style("font-size", textSize + "px")
                        .style("text-anchor", "start")
                        .attr("transform", "translate(" + margin.left + "," + (legendHeight - margin.bottom + legendHeight - margin.top - margin.bottom) + ")");

                    legend
                        .append("text")
                        .text(maxFrequency)
                        .style("text-anchor", "end")
                        .style("font-size", textSize + "px")
                        .attr("transform", "translate(" + (legendWidth - margin.right) + "," + (legendHeight - margin.bottom + legendHeight - margin.top - margin.bottom) + ")");
                }

                function drawMatrix() {
                    var scale = zoom.scale();

                    matrixCellGroup.selectAll(".matrix_cell").remove();

                    matrixCellGroup.selectAll(".matrix_cell")
                        .data(dataList)
                        .enter()
                        .append("rect")
                        .attr("class", "matrix_cell")
                        .attr("width", cellSize*scale)
                        .attr("height", cellSize*scale)
                        .attr("x", function (d) {
                            return x(d.n);
                        })
                        .attr("y", function (d) {
                            return y(d.m);
                        })
                        .attr("fill", config.cell.color.normal)
                        .attr("opacity", function (d) {
                            return Math.max(config.cell.minOpacity, oScale(Math.log(d.frequency)));
                        })
                        .on("mouseover", mouseover)
                        .on("mouseout", mouseout)
                        .on("click", mousedown);
                }

                function mouseover(d) {
                    tooltip.style("visibility", "visible")
                        .style("left", (d3.event.pageX) + 30 + "px")
                        .style("top", (d3.event.pageY) - 30 + "px")
                        .transition()
                        .duration(200)
                        .style("opacity", config.tooltip.opacity);

                    var messageForTooltip = "m: "+ d.m.toString()+"</br>" + "n: "+ d.n.toString()+"</br>" + "No. of biclusters: "+ d.frequency.toString();
                    contentMessage.html(messageForTooltip);
                }

                function mouseout () {
                    tooltip.transition()
                        .duration(500)
                        .style("opacity", 0);
                }

                function mousedown(d) {
                    if(!isClick) {
                        return ;
                    }

                    var oldColor = this.attributes.fill.value;
                    if(oldColor == 'gray' || oldColor == 'orange') {
                        return ;
                    }

                    d.selected = !d.selected;
                    var color = config.cell.color.normal;
                    if(d.selected) {
                        color = config.cell.color.selected;
                        selectedCell[d.m].add(d.n);
                    } else {
                        color = config.cell.color.normal;
                        selectedCell[d.m].remove(d.n);
                    }
                    d3.select(this).attr("fill", color);

                    var mnList = selectedCellToMNList(selectedCell);

                    pipService.emitParallelCoordinatesDataChange({
                        mnList: mnList,
                        selectedSource: selectedSource,
                        selectedDest: selectedDest
                    })
                }

                function clearSelectedCell() {
                    for(var i in selectedCell) {
                        selectedCell[i] = d3.set();
                    }

                    matrixCellGroup
                        .selectAll(".matrix_cell")
                        .attr("fill", config.cell.color.normal);
                }

                // put selected cell to a list
                function selectedCellToMNList(selectedCell) {
                    var list = [];
                    for(var i in selectedCell) {
                        var values = selectedCell[i].values();
                        for(var j in values) {
                            list.push([+i, +values[j]]);
                        }
                    }

                    return list;
                }

                function setSelectedSource(_selectedSourceList) {
                    isClick = false;

                    var _selectedSource = d3.set();
                    for(var i in _selectedSourceList) {
                        _selectedSource.add(_selectedSourceList[i]);
                    }
                    selectedSource = _selectedSource;
                }

                function setSelectedDest(_selectedDestList) {
                    isClick = false;

                    var _selectedDest = d3.set();
                    for(var i in _selectedDestList) {
                        _selectedDest.add(_selectedDestList[i]);
                    }
                    selectedDest = _selectedDest;
                }

                function filterMatrixCell() {
                    clearSelectedCell();

                    var testList = [];

                    matrixCellGroup.selectAll(".matrix_cell")
                        .attr("fill", function(d) {
                            if(selectedSource.size() == 0 && selectedDest.size() == 0) {
                                return config.cell.color.normal;
                            }

                            var sourceCount = 0;
                            if(selectedSource.size() > 0) {
                                sourceCount = d.source.filter(function(v) {
                                    return selectedSource.has(v);
                                }).length;
                            }

                            var destCount = 0;
                            if(selectedDest.size() > 0) {
                                destCount = d.destination.filter(function(v) {
                                    return selectedDest.has(v);
                                }).length;
                            }

                            if(sourceCount == selectedSource.size() && destCount == selectedDest.size()) {
                                if(selectedSource.size() + selectedDest.size() == 1) {
                                    return config.cell.color.normal;
                                }

                                var m = d.m;
                                var n = d.n;

                                var thisDom = this;

                                testList.push({
                                    m: m,
                                    n: n,
                                    dom: thisDom
                                });

                                return config.cell.color.testing;
                            } else {
                                return config.cell.color.unavailable;
                            }
                        });

                    // deeply test cells
                    matrixService.testCellsDataDeeply(testList, selectedSource.values(), selectedDest.values(), config);

                    isClick = true;
                }
            }
        }
    }
}]);
'use strict';

angular.module('matrix').factory('matrixService',
    function() {
        var matrixService = {};

        return matrixService;
    }
);
'use strict';

angular.module('parallelCoordinates').controller('parallelCoordinatesController', ['$scope', 'parallelCoordinatesService', 'pipService', 'lineUpService',
    function($scope, parallelCoordinatesService, pipService, lineUpService) {

        $scope.changeParallelCoordinatesControlParameters = changeParallelCoordinatesControlParameters;
        $scope.switchIsLegend = switchIsLegend;
        $scope.switchIsHistogram = switchIsHistogram;
        $scope.reset = redrawGraph;

        parallelCoordinatesService.getDimensions = getDimensions;

        pipService.onParallelCoordinatesDataChange($scope, function(data) {
            parallelCoordinatesService.loadData(data.mnList, data.selectedSource, data.selectedDest);
        });

        function getDimensions() {
            var dims = [];

            var dimDoms = $(".parallelCoordinates_dimensions");
            for(var i in dimDoms) {
                if(dimDoms[i].checked) {
                    dims.push(dimDoms[i].value);
                }
            }

            return dims;
        }

        function changeParallelCoordinatesControlParameters() {
            var dimensions = getDimensions();

            parallelCoordinatesService.setDimensions(dimensions);
        }

        function switchIsLegend() {
            parallelCoordinatesService.switchIsLegend();
        }

        function switchIsHistogram() {
            parallelCoordinatesService.switchIsHistogram();
        }

        function redrawGraph() {
            lineUpService.lineUp.setData(parallelCoordinatesService.data);

            parallelCoordinatesService.drawGraph();
        }
    }
]);
'use strict';

angular.module('parallelCoordinates').directive('parallelCoordinatesDirective', ["parallelCoordinatesService", 'configService', 'funcService', 'dataService', 'pipService', 'lineUpService', function(parallelCoordinatesService, configService, funcService, dataService, pipService, lineUpService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            var config = configService.parallelCoordinates;

            var isLegend = true;
            parallelCoordinatesService.switchIsLegend = switchIsLegend;
            drawLegend();

            pipService.onDateListChange(scope, function() {
                parallelCoordinatesService.loadData([], [], []);
            });

            constructParallelCoordinates();

            function drawLegend() {
                d3.select("#legend_parallel_coordinates svg").remove();

                var legendMargin = config.legend.margin;

                var legendWidth = d3.select("#legend_parallel_coordinates")[0][0].offsetWidth;
                var legendHeight = d3.select("#legend_parallel_coordinates")[0][0].offsetHeight;
                var legend = d3
                    .select("#legend_parallel_coordinates")
                    .append("svg")
                    .attr("id", "legendOfPc")
                    .attr("width", legendWidth)
                    .attr("height", legendHeight);

                var gradient = legend.append("g").append("svg:defs")
                    .append("svg:linearGradient")
                    .attr("id", "gradient4")
                    .attr("x1", "0%")
                    .attr("y1", "0%")
                    .attr("x2", "0%")
                    .attr("y2", "100%")
                    .attr("spreadMethod", "pad");

                gradient.append("svg:stop").attr("offset", "0%").attr("stop-color", "rgb(0,104,55)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "10%").attr("stop-color", "rgb(26,152,80)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "20%").attr("stop-color", "rgb(102,189,99)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "30%").attr("stop-color", "rgb(166,217,106)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "40%").attr("stop-color", "rgb(217,239,139)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "50%").attr("stop-color", "rgb(255,255,191)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "60%").attr("stop-color", "rgb(254,224,139)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "70%").attr("stop-color", "rgb(253,174,97)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "80%").attr("stop-color", "rgb(244,109,67)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "90%").attr("stop-color", "rgb(215,48,39)").attr("stop-opacity", 1);
                gradient.append("svg:stop").attr("offset", "100%").attr("stop-color", "rgb(165,0,38)").attr("stop-opacity", 1);

                legend
                    .append("g")
                    .append("rect")
                    .attr("width", legendWidth - legendMargin.left - legendMargin.right)
                    .attr("height", legendHeight - legendMargin.top - legendMargin.bottom)
                    .attr("transform", "translate(" + legendMargin.left + "," + legendMargin.top + ")")
                    .attr("fill", "url(#gradient4)");

                legend
                    .append("text")
                    .text("Z-score")
                    .style("font-size", "8px")
                    .style("font-weight", "bold")
                    .style("text-anchor", "start")
                    .attr("transform", "translate(" + (legendMargin.left + (legendWidth - legendMargin.left - legendMargin.right + 10)) + "," + (legendMargin.top) + ")rotate(90)");

                legend.append("text")
                    .text("2")
                    .style("font-size", "8px")
                    .style("text-anchor", "middle")
                    .attr("transform", "translate(" + (legendMargin.left - 10) + "," + (legendMargin.top + 10) + ")");

                legend
                    .append("text")
                    .text("0")
                    .style("font-size", "8px")
                    .style("text-anchor", "middle")
                    .attr("transform", "translate(" + (legendMargin.left - 10) + "," + (legendHeight / 2 + legendMargin.top - 10) + ")");

                legend
                    .append("text")
                    .text("-2")
                    .style("text-anchor", "middle")
                    .style("font-size", "8px")
                    .attr("transform", "translate(" + (legendMargin.left - 10) + "," + (legendHeight + legendMargin.top - 30) + ")");
            }

            function switchIsLegend() {
                isLegend = !isLegend;
                d3
                    .select("#legend_parallel_coordinates")
                    .style("display", isLegend ? "block" : "none");
            }

            function constructParallelCoordinates() {
                parallelCoordinatesService.loadData = loadData;
                parallelCoordinatesService.setData = setData;
                parallelCoordinatesService.clearGraph = clearGraph;
                parallelCoordinatesService.drawGraph = drawGraph;
                parallelCoordinatesService.setDimensions = setDimensions;
                parallelCoordinatesService.switchIsHistogram = switchIsHistogram;

                var wholeData = [];
                var data = [];
                parallelCoordinatesService.wholeData = data;
                parallelCoordinatesService.data = data;

                var svg;
                var margin = config.margin;
                var width = config.size.width;
                var height = config.size.height;

                var sliceNum = config.histogram.slice.num;
                var isHistogram = false;

                var x = d3.scale.ordinal().rangePoints([0, width], 1);
                var y = {}; // for dimensions

                var dragging = {};
                var line = d3.svg.line();
                var axis = d3.svg.axis().orient("left");
                var background; // grey lines
                var foreground; // blue lines

                var dimensions = parallelCoordinatesService.getDimensions(); // init dimension list
                var newDimensions; // ordered dimensions
                var dimensionNameMap = config.dimensionNameMap;

                pipService.onHoursChange(scope, function() {
                    setData();
                });


                function clearGraph() {  // clear graph
                    $("#parallel_coordinates").empty();

                    y = {};
                    dragging = {};
                    svg = d3.select("#parallel_coordinates").append("svg")
                        .attr("width", width + margin.left + margin.right)
                        .attr("height", height + margin.top + margin.bottom)
                        .append("g")
                        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
                }

                function loadData(mnList, selectedSource, selectedDest) {
                    clearGraph();
                    lineUpService.lineUp.setData([]);

                    if(mnList.length == 0) {
                        wholeData = [];
                        data = [];
                        parallelCoordinatesService.wholeData = wholeData;
                        parallelCoordinatesService.data = data;

                        return ;
                    }

                    lineUpService.getDataListFiltered(mnList, selectedSource.values(), selectedDest.values(), function(dataList) {
                        wholeData = dataList.reduce(function(preV, curV) {
                            return preV.concat(curV);
                        });

                        parallelCoordinatesService.wholeData = wholeData;
                        setData();
                    });
                }

                function setData() {
                    var hours = dataService.getHours();

                    data = funcService.objectClone(wholeData);

                    if(hours.length < 24) {
                        filterData(hours);
                    }

                    parallelCoordinatesService.data = data;

                    lineUpService.lineUp.setData(data);
                    drawGraph();
                }

                function filterData(hours) {
                    for (var i in data) {
                        filterMNList(hours, data[i].value.m);
                        filterMNList(hours, data[i].value.n);

                        data[i].value.info.time_slice_sum = funcService.getPropertyNum(data[i].value.m[0].time_slice_persons);
                        data[i].value.info.persons_sum = data[i].value.m.reduce(function (preV, curV) {
                            return preV + curV.day_persons;
                        }, 0);
                    }
                    data = data.filter(function (d) {
                        return d.value.info.time_slice_sum;
                    });

                    function filterMNList(hours, list) {
                        for(var i in list) {
                            list[i].day_persons = 0;

                            for(var j in list[i].time_slice_persons) {
                                var sliceTime = j;
                                if (!_.contains(hours, parseInt(sliceTime/12))) {
                                    delete list[i].time_slice_persons[j];
                                    continue ;
                                }

                                list[i].day_persons += list[i].time_slice_persons[j]
                            }
                        }
                    }
                }

                function drawGraph() {
                    clearGraph();

                    if(isDataAvailable()) {
                        return ;
                    }

                    setY();

                    var statisticDims = getStatisticOfDimensions();

                    newDimensions = orderDimensions(statisticDims);
                    x.domain(newDimensions);

                    drawBackground();
                    drawForeground();

                    var g = drawDimensionGroups();
                    drawHistogram(g, statisticDims);
                    drawAxis(g);
                    drawBrushRect(g);
                }

                function isDataAvailable() {
                    return !data || data==null || data.length==0;
                }

                function setY() {
                    for (var i in dimensions) {
                        var d = dimensions[i];
                        y[d] = d3.scale.linear()
                            .domain(d3.extent(data, function (p) {
                                return +p.value.info[d];
                            }))
                            .range([height, 0]);
                    }
                }

                function getStatisticOfDimensions() {
                    var statisticDims = {};

                    // calculate slice list related statistic
                    for (var i in dimensions) {
                        statisticDims[dimensions[i]] = {};

                        var dList = data.map(function(v) {
                            return v.value.info[dimensions[i]]
                        });

                        var mean = d3.mean(dList);
                        var sigma = d3.deviation(dList);    // standard deviation

                        statisticDims[dimensions[i]].mean = mean;
                        statisticDims[dimensions[i]].sigma = sigma;

                        for (var j in dList) {
                            dList[j] = dList[j] - mean;
                        }
                        statisticDims[dimensions[i]].deviationList = dList;
                    }

                    // calculate per slice related statistic
                    for (var i in dimensions) {
                        var domain = y[dimensions[i]].domain();
                        var min = domain[0];
                        var max = domain[1];

                        statisticDims[dimensions[i]].min = min;
                        statisticDims[dimensions[i]].max = max;

                        var _sliceNum = sliceNum;
                        var step = (max - min) / _sliceNum;
                        if (dimensions[i] == "time_slice_sum") {
                            _sliceNum = max - min + 1;
                            step = 1;
                        }

                        var dataRecordNumPerSlice = new Array(_sliceNum);
                        var dataValuePerSlice = new Array(_sliceNum);
                        for (var j = 0; j < _sliceNum; ++j) {
                            dataRecordNumPerSlice[j] = 0;
                            dataValuePerSlice[j] = 0;
                        }

                        for (var j in data) {
                            var d = data[j].value.info[dimensions[i]];

                            var did = Math.floor((d - min) / step);
                            if (d == max) {
                                did = _sliceNum - 1;
                            }
                            ++dataRecordNumPerSlice[did];
                            dataValuePerSlice[did] = dataValuePerSlice[did] + d;
                        }

                        for (var j = 0; j < _sliceNum; ++j) {
                            if (dataRecordNumPerSlice[j] > 0) {
                                dataValuePerSlice[j] = dataValuePerSlice[j] / dataRecordNumPerSlice[j];
                            }
                        }

                        statisticDims[dimensions[i]].sliceNum = _sliceNum;

                        statisticDims[dimensions[i]].dataRecordNumPerSlice = dataRecordNumPerSlice;
                        statisticDims[dimensions[i]].maxDataRecordNumPerSlice = d3.max(dataRecordNumPerSlice);

                        statisticDims[dimensions[i]].dataValuePerSlice = dataValuePerSlice;
                    }

                    return statisticDims;
                }

                function calculateDimDis(vi, vj) {   // calculate cos value of angle of two vectors
                    var ij = numeric.dot(vi, vj);
                    ij = ij*ij;
                    var ii = numeric.dot(vi, vi);
                    var jj = numeric.dot(vj, vj);

                    var dis = 0;
                    if(ij == 0 || (ii == 0 && jj == 0)) {
                        dis = 0;
                    } else {
                        dis = Math.sqrt(ij/(ii*jj));
                    }

                    return dis;
                }

                function getDimensionDistanceMatrix(statisticDims) {  // distance of every two dims
                    var dimN = dimensions.length;

                    // init array
                    var dimDisMatrix= new Array(dimN);
                    for(var i = 0; i < dimN; ++i) {
                        dimDisMatrix[i] = new Array(dimN);
                    }

                    for(var i = 0; i < dimN; ++i) {
                        dimDisMatrix[i][i] = 1;

                        var dListI = statisticDims[dimensions[i]].deviationList;
                        for(var j = i+1; j < dimN; ++j) {
                            var dListJ = statisticDims[dimensions[j]].deviationList;

                            dimDisMatrix[i][j] = calculateDimDis(dListI, dListJ);
                            dimDisMatrix[j][i] = dimDisMatrix[i][j];
                        }
                    }

                    return dimDisMatrix;
                }



                function orderDimensions(statisticDims) {
                    var dimN = dimensions.length;
                    var dimDisMatrix = getDimensionDistanceMatrix(statisticDims);

                    var max = -1;
                    var maxI = -1;
                    var maxJ = -1;
                    for(var i = 0; i < dimN; ++i) {
                        for(var j = i+1; j < dimN; ++j) {
                            if(dimDisMatrix[i][j] > max) {
                                max = dimDisMatrix[i][j];
                                maxI = i;
                                maxJ = j;
                            }
                        }
                    }

                    var dimsIndexs = [maxI, maxJ];
                    var dimSet = d3.set(d3.range(dimN));
                    dimSet.remove(maxI);
                    dimSet.remove(maxJ);

                    var lr = "L";   // Left or Right
                    var relatedIndex = 0;

                    while(dimSet.size() > 0) {
                        var max = -1;
                        var maxIndex = -1;

                        var values = dimSet.values();
                        for(var i in values) {
                            if(dimDisMatrix[dimsIndexs[relatedIndex]][values[i]] > max) {
                                max = dimDisMatrix[dimsIndexs[relatedIndex]][values[i]];
                                maxIndex = values[i];
                            }
                        }
                        maxIndex = +maxIndex;

                        if(lr == "L") {
                            dimsIndexs = [maxIndex].concat(dimsIndexs);
                            lr = "R";
                            relatedIndex = dimsIndexs.length-1;
                        } else {
                            dimsIndexs.push(maxIndex);
                            lr = "L";
                            relatedIndex = 0;
                        }

                        dimSet.remove(maxIndex);
                    }

                    var newDims = dimsIndexs.map(function(v) {
                        return dimensions[v];
                    });

                    return newDims;
                }

                function drawBackground() { // Add grey background lines for context.
                    background = svg.append("g")
                        .attr("class", "background")
                        .selectAll("path")
                        .data(data)
                        .enter().append("path")
                        .attr("opacity", 0.1)
                        .attr("d", path);

                    background.style("display", isHistogram ? "none" : "");
                }

                function drawForeground() { // Add blue foreground lines for focus.
                    var forGroundOpacity = 0.1;
                    if(data.length < 50){
                        forGroundOpacity = 0.7;
                    } else if(data.length < 150){
                        forGroundOpacity = 0.5;
                    }

                    foreground = svg.append("g")
                        .attr("class", "foreground")
                        .selectAll("path")
                        .data(data)
                        .enter().append("path")
                        .attr("opacity", forGroundOpacity)
                        .attr("d", path);

                    foreground.style("display", isHistogram ? "none" : "");
                }

                function drawDimensionGroups() {    // Add a group element for each dimension and return these groups
                    var g = svg.selectAll(".dimension")
                        .data(newDimensions)
                        .enter().append("g")
                        .attr("class", "dimension")
                        .attr("transform", function (d) {
                            return "translate(" + x(d) + ")";
                        })
                        .call(d3.behavior.drag()
                            .origin(function (d) {
                                return {
                                    x: x(d)
                                };
                            })
                            .on("dragstart", function (d) {
                                dragging[d] = x(d);
                                background.attr("visibility", "hidden");
                            })
                            .on("drag", function (d) {
                                dragging[d] = Math.min(width, Math.max(0, d3.event.x));
                                foreground.attr("d", path);
                                newDimensions.sort(function (a, b) {
                                    return position(a) - position(b);
                                });
                                x.domain(newDimensions);

                                g.attr("transform", function (d) {
                                    return "translate(" + position(d) + ")";
                                })
                            })
                            .on("dragend", function (d) {
                                delete dragging[d];

                                transition(d3.select(this)).attr("transform", "translate(" + x(d) + ")");
                                transition(foreground).attr("d", path);

                                background
                                    .attr("d", path)
                                    .transition()
                                    .delay(500)
                                    .duration(0)
                                    .attr("visibility", null);
                            }));

                    return g;
                }

                function calculateZScore(value, mean, sigma) {
                    var zScore = 0;
                    if (sigma > 0) {
                        zScore = (value - mean) / sigma;
                    }

                    return zScore;
                }

                function drawHistogram(container, statisticDims) {
                    var histogramConfig = config.histogram;

                    var maxSliceLen = width / dimensions.length - histogramConfig.slice.gap;

                    var zScoreColorScale = d3.scale.linear()
                        .domain(histogramConfig.colorScale.domain)
                        .range(histogramConfig.colorScale.range)
                        .interpolate(d3.interpolateLab);

                    container.append("g")
                        .style("display", isHistogram ? "" : "none")
                        .selectAll(".slices")
                        .data(function (d) {
                            return statisticDims[d].dataRecordNumPerSlice;
                        })
                        .enter()
                        .append("rect")
                        .attr("class", "slices")
                        .attr("transform", function (d, i) {
                            var dim = d3.select(this.parentNode).datum();
                            var sta = statisticDims[dim];
                            var value = sta.dataValuePerSlice[i];

                            return "translate(" + (-0.5 * d / sta.maxDataRecordNumPerSlice * maxSliceLen) + ", " + (y[dim](value) - 0.5 * height / sta.sliceNum) + ")";
                        })
                        .attr("width", function (d) {
                            var dim = d3.select(this.parentNode).datum();
                            var sta = statisticDims[dim];

                            return d / sta.maxDataRecordNumPerSlice * maxSliceLen;
                        })
                        .attr("height", function () {
                            var dim = d3.select(this.parentNode).datum();
                            var sta = statisticDims[dim];

                            return height / sta.sliceNum;
                        })
                        .attr("fill", function (d, i) {
                            var dim = d3.select(this.parentNode).datum();
                            var sta = statisticDims[dim];
                            var value = sta.dataValuePerSlice[i];

                            var zScore = calculateZScore(value, sta.mean, sta.sigma);

                            return zScoreColorScale(zScore);
                        })
                        .attr("opacity", histogramConfig.opacity);
                }

                function drawAxis(container) {  // Add an axis and title.
                    var axisConfig = config.axis;

                    container.append("g")
                        .attr("class", "axis")
                        .each(function (d) {
                            d3
                                .select(this)
                                .call(axis.scale(y[d]));
                        })
                        .append("text")
                        .style("text-anchor", axisConfig.textAnchor)
                        .attr("y", axisConfig.y)
                        .text(function (d) {
                            return dimensionNameMap[d];
                        });
                }

                function drawBrushRect(container) {  // Add and store a brush for each axis.
                    var brushConfig = config.brush;

                    container.append("g")
                        .attr("class", "brush")
                        .each(function(d) {
                            d3
                                .select(this)
                                .call(y[d].brush = d3.svg.brush().y(y[d]).on("brushstart", brushstart).on("brush", brush));
                        })
                        .selectAll("rect")
                        .attr("x", brushConfig.x)
                        .attr("width", brushConfig.width);
                }


                function position(d) {
                    var v = dragging[d];
                    return v == null ? x(d) : v;
                }

                function transition(g) {
                    return g.transition().duration(500);
                }

                function path(d) {  // Returns the path for a given data point.
                    return line(newDimensions.map(function(p) {
                        return [position(p), y[p](d.value.info[p])];
                    }));
                }

                function brushstart() {
                    d3.event.sourceEvent.stopPropagation();
                }

                function brush() {  // Handles a brush event, toggling the display of foreground lines.
                    var actives = dimensions.filter(function(p) {
                            return !y[p].brush.empty();
                        });
                    var extents = actives.map(function(p) {
                            return y[p].brush.extent();
                        });

                    if(isHistogram) {
                        if (actives.length == 0) {
                            background.style("display", "none");
                            foreground.style("display", "none");

                            return;
                        } else {
                            background.style("display", "");
                            foreground.style("display", "");
                        }
                    }

                    var filteredData = [];
                    foreground
                        .style("display", function(d) {
                            var flag = actives.every(function(p, i) {
                                return extents[i][0] <= d.value.info[p] && d.value.info[p] <= extents[i][1];
                            });

                            if(flag) {
                                filteredData.push(d);
                            }

                            return flag ? "" : "none";
                        });

                    lineUpService.lineUp.setData(filteredData);
                }

                function setDimensions(_dimensions) {
                    dimensions = _dimensions;
                    drawGraph();
                }

                function switchIsHistogram() {
                    isHistogram = !isHistogram;
                    drawGraph();
                }
            }
        }
    }
}]);
'use strict';

angular.module('parallelCoordinates').factory('parallelCoordinatesService',
    function() {
        var parallelCoordinatesService = {};

        return parallelCoordinatesService;
    }
);
'use strict';

angular.module('radialGraph').controller('radialGraphController', ['$scope', 'radialGraphService', 'dataService', 'dbService', 'pipService',
    function($scope, radialGraphService, dataService, dbService, pipService) {


        $scope.switchIsLegend = switchIsLegend;
        $scope.changeControlParameters = changeControlParameters;

        radialGraphService.loadData = loadData;

        function switchIsLegend() {
            radialGraphService.switchIsLegend();
        }

        function changeControlParameters() {
            var radialGraph_hoursPerSector = $(".radialGraph_hoursPerSector")[0];
            radialGraphService.setRadialGraphHoursPerSector(radialGraph_hoursPerSector.value);

            var radialGraph_sortTypeDoms = $(".radialGraph_sortType");
            for(var i in radialGraph_sortTypeDoms) {
                if(radialGraph_sortTypeDoms[i].checked) {
                    radialGraphService.setRadialGraph_sortType(radialGraph_sortTypeDoms[i].value);

                    break;
                }
            }

            var radialGraph_maxDistance = $(".radialGraph_maxDistance")[0];
            radialGraphService.setRadialGraphMaxDistance(parseFloat(radialGraph_maxDistance.value));

            var radialGraph_sigma = $(".radialGraph_sigma")[0];
            radialGraphService.setRadialGraphSigmaOfGaussianSmooth(parseInt(radialGraph_sigma.value));

            radialGraphService.constructRadialGraph();
        }

        function loadData(id) {
            var dateList = dataService.getDateList();

            var name = '/radialgraph';
            var paras = {
                stationID: id,
                dateList: dateList
            };

            dbService.post(name, paras, function(data) {
                radialGraphService.data = data;

                pipService.emitRadialGraphDataChange(data);
            });
        }
    }
]);
'use strict';

angular.module('radialGraph').directive('radialGraphDirective', ["radialGraphService", 'funcService', 'pipService', 'mapService', function(radialGraphService, funcService, pipService, mapService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            var isLegend = true;

            var rawData = null;

            var sortType = "distance";
            var maxDistance = 20;
            var sigmaNum = 2;

            var startTime = 0;
            var endTime = 23;

            var hoursPerSector = 2;

            pipService.onDateListChange(scope, function() {
                rawData = null;
                clear();
            });

            pipService.onHourRangeChange(scope, function(hourRange) {
                radialGraphService.setRadialGraphStartAndEndTime(hourRange.start, hourRange.end);
                radialGraphService.constructRadialGraph();
            });


            radialGraphService.switchIsLegend = switchIsLegend;

            radialGraphService.constructRadialGraph = constructRadialGraph;

            radialGraphService.setRadialGraphStartAndEndTime = setRadialGraphStartAndEndTime;
            radialGraphService.setRadialGraphHoursPerSector = setRadialGraphHoursPerSector;
            radialGraphService.setRadialGraph_sortType = setRadialGraph_sortType;
            radialGraphService.setRadialGraphMaxDistance = setRadialGraphMaxDistance;
            radialGraphService.setRadialGraphSigmaOfGaussianSmooth = setRadialGraphSigmaOfGaussianSmooth;

            function switchIsLegend() {
                isLegend = !isLegend;
                d3
                    .select("#legend_tree_map")
                    .style("display", isLegend ? "block" : "none");
            }

            function drawLegend() {
                d3.select("#legend_tree_map svg").remove();

                var margin = {left: 15, right: 10, top: 20, bottom: 15};

                var legendWidth = d3.select("#legend_tree_map")[0][0].offsetWidth;
                var legendHeight = d3.select("#legend_tree_map")[0][0].offsetHeight;
                var legend = d3.select("#legend_tree_map").append("svg").attr("id", "legendOfTreemap")
                    .attr("width", legendWidth).attr("height", legendHeight);

                var gradient = legend.append("g").append("svg:defs")
                    .append("svg:linearGradient")
                    .attr("id", "gradient3")
                    .attr("x1", "0%")
                    .attr("y1", "0%")
                    .attr("x2", "100%")
                    .attr("y2", "0%")
                    .attr("spreadMethod", "pad");

                gradient.append("svg:stop")
                    .attr("offset", "0%")
                    .attr("stop-color", "rgb(247,252,245)")
                    .attr("stop-opacity", 1);

                gradient.append("svg:stop")
                    .attr("offset", "90%")
                    .attr("stop-color", "#41ab5d")
                    .attr("stop-opacity", 1);

                gradient.append("svg:stop")
                    .attr("offset", "100%")
                    .attr("stop-color", "#41ab5d")
                    .attr("stop-opacity", 1);

                legend.append("g").append("rect").attr("width", legendWidth - margin.left - margin.right).attr("height", legendHeight - margin.top - margin.bottom).attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                    .attr("fill", "url(#gradient3)");

                legend.append("text").text("Distance (km)").style("font-size", "6px").style("font-weight", "bold").style("text-anchor", "start").attr("transform", "translate(" + margin.left + "," + (margin.top - 5) + ")");

                legend.append("text").text("0").style("font-size", "6px").style("text-anchor", "start").attr("transform", "translate(" + margin.left + "," + (legendHeight - margin.bottom + legendHeight - margin.top - margin.bottom) + ")");

                legend.append("text").text(maxDistance).style("text-anchor", "end").style("font-size", "6px").attr("transform", "translate(" + (legendWidth - margin.right) + "," + (legendHeight - margin.bottom + legendHeight - margin.top - margin.bottom) + ")");
            }

            function setRadialGraphStartAndEndTime(_startTime, _endTime) {
                if(_startTime < 0) {
                    _startTime = 0;
                }
                if(_endTime > 23) {
                    _endTime = 23;
                }

                if(startTime == _startTime && endTime == _endTime) {
                    return ;
                }

                startTime = _startTime;
                endTime = _endTime;
            }


            function setRadialGraphHoursPerSector(_hoursPerSector) {
                if(hoursPerSector == _hoursPerSector) {
                    return ;
                }

                hoursPerSector = _hoursPerSector;
            }

            function setRadialGraph_sortType(_sortType) {
                if(sortType == _sortType) {
                    return ;
                }

                sortType = _sortType;
            }

            function setRadialGraphMaxDistance(_maxDistance) {
                if(maxDistance == _maxDistance) {
                    return ;
                }

                maxDistance = _maxDistance;
            }

            function setRadialGraphSigmaOfGaussianSmooth(_sigmaNum) {
                if(sigmaNum == _sigmaNum) {
                    return ;
                }

                sigmaNum = _sigmaNum;
            }


            pipService.onRadialGraphDataChange(scope, function(data) {
                rawData = data;

                constructRadialGraph();
            });


            function isDataAvailable() {
                if(!rawData || rawData == null) {
                    return false;
                }

                return true;
            }

            function clear() {
                d3.select("#radial_graph_svg").remove();
            }

            function constructRadialGraph() {
                clear();

                if(!isDataAvailable()) {
                    return ;
                }

                var loyaltySliceNum = 1;    // adjust loyalty
                var timeSliceNum = 288;
                var directionSliceNum = (endTime-startTime+1) * (288/24); // adjust direction slice (time slice)

                var marker = null;
                var selected_nid = -1;

                var radialGraph = d3
                    .select("#radial_graph")
                    .append("svg")
                    .attr("id", "radial_graph_svg");

                var radialGraph_g = radialGraph
                    .append("g")
                    .attr("transform", "translate(" + 200 + "," + 300+")");

                var areaType = "persons";

                var nodes = getNodeList();

                var realMaxDistance = d3.max(nodes, function(d) {   // the max distance of data, sometimes we could encode using this
                    return d.distance;
                });

                var totalPersonsOfGraph = nodes.reduce(function(preV, curV) {
                    return preV + curV.totalPersons;
                }, 0);

                setCid("loyalty" , 1, loyaltySliceNum, nodes);
                for(var i = 0; i < nodes.length; ++i) { // reverse loyalty layer
                    nodes[i].loyalty_id = loyaltySliceNum - 1 - nodes[i].loyalty_id;
                }

                var grids = constructGrids();

                gaussianSmooth(grids, loyaltySliceNum, directionSliceNum);
                calculateGridScale(grids, loyaltySliceNum, directionSliceNum);

                drawRadialGraph(grids, loyaltySliceNum, directionSliceNum);

                function getNodeList() {
                    var nodes = [];
                    for(var i in rawData) {
                        var nid = i;
                        var node = rawData[nid];
                        node.nid = nid;

                        for(var j in node.value) {
                            node[j] = node.value[j];
                        }
                        node.value = node[areaType];

                        node.personsOfHours = [];
                        for(var j = 0; j < 24; ++j) {
                            node.personsOfHours.push(0);
                        }
                        var step = timeSliceNum / 24;
                        var totalPersons = 0;
                        for(var j in node.persons) {
                            var _j = Math.floor(j / step);

                            node.personsOfHours[_j] = node.personsOfHours[_j] + node.persons[j];
                            totalPersons = totalPersons + node.persons[j];
                        }
                        node.totalPersons = totalPersons;

                        if(node.distance <= 0.4) {    // filter out distance == 0.4
                            continue ;
                        }

                        nodes.push(node);
                    }

                    return nodes;
                }

                function setCid(propertyName , total, sliceNum, nodes) {

                    var step = total / sliceNum;

                    for(var i = 0; i < nodes.length; ++i) {
                        var value = nodes[i][propertyName];
                        var id = Math.floor(value / step);
                        if(id >= sliceNum) {
                            id = sliceNum - 1;
                        }

                        nodes[i][propertyName+"_id"] = id;
                    }
                }

                function constructGrids() {
                    var grids = new Array(loyaltySliceNum);
                    for (var i = 0; i < loyaltySliceNum; ++i) {
                        grids[i] = new Array(directionSliceNum);
                        for (var j = 0; j < directionSliceNum; ++j) {
                            grids[i][j] = [];
                            grids[i][j].value = 0;
                        }
                    }

                    for(var i in nodes) {
                        var node = nodes[i];
                        for(var j in node.persons) {
                            var step = timeSliceNum / 24;
                            var hour = Math.floor(j / step);
                            if(hour >= startTime && hour <= endTime) {
                                var _j = j - startTime * (timeSliceNum / 24);

                                grids[node.loyalty_id][_j].value = grids[node.loyalty_id][_j].value + node.persons[j];
                            }
                        }
                    }

                    return grids;
                }

                function calculateGaussianValue(d, sigma) {
                    return Math.exp(-d*d/2/sigma/sigma)/Math.sqrt(2*Math.PI)/sigma;
                }

                function gaussianSmooth(grids, loyaltySliceNum, directionSliceNum) {
                    for(var i = 0; i < loyaltySliceNum; ++i) {
                        for(var j = 0; j < directionSliceNum; ++j) {
                            var grid = grids[i][j];
                            grid.gaussianValue = 0;

                            for(var k = 0; k < directionSliceNum; ++k) {
                                var _grid = grids[i][k];

                                var d1 = k-j;
                                if(d1 < 0) {
                                    d1 = d1 + directionSliceNum;
                                }
                                var d2 = j-k;
                                if(d2 < 0) {
                                    d2 = d2 + directionSliceNum;
                                }
                                var dis = Math.min(d1, d2);

                                grid.gaussianValue = grid.gaussianValue + _grid.value * calculateGaussianValue(dis, sigmaNum);
                            }
                        }
                    }

                }

                function calculateGridScale(grids, loyaltySliceNum, directionSliceNum) {    // scale = sqrt(gaussianValue)
                    for(var i = 0; i < loyaltySliceNum; ++i) {
                        for(var j = 0; j < directionSliceNum; ++j) {
                            var grid = grids[i][j];
                            grid.scale = Math.sqrt(grid.gaussianValue);
                        }
                    }
                }

                function drawRadialGraph(grids, loyaltySliceNum, directionSliceNum) {
                    var maxLength = 150;

                    var maxScale = 0;
                    for(var i = 0; i < loyaltySliceNum; ++i) {
                        if (nodes.length == 0) {
                            continue;
                        }

                        // calculate angles
                        var clus = constructClusters(nodes);
                        var clusAngles = generateClusAngles(i, clus);

                        maxScale = d3.max(clusAngles, function (d) {
                            return d3.max(d, function (_d) {
                                return _d.scaleCeil;
                            })
                        });

                        // draw treemap
                        var roots = [];
                        for(var j = 0; j < clus.length; ++j) {
                            var root = {
                                elements: clus[j],
                                edges: clusAngles[j]
                            };

                            constructTree(root, clus[j].timeSlice);
                            roots.push(root);
                        }

                        for(var j = 0; j < roots.length; ++j) {
                            drawTreemap(roots[j]);
                        }
                    }

                    drawClockDial();

                    function constructClusters(nodes) {
                        var clusLen = Math.ceil((endTime-startTime+1) / hoursPerSector);

                        var clus = [];
                        var clusSet = [];   // make sure uniqueness of nodes in one hour sector

                        // init clusters
                        for(var i = 0; i < clusLen; ++i) {
                            var clu = [];
                            clu.persons = 0;
                            clu.timeSlice = i;

                            clus.push(clu);
                            clusSet.push(d3.set());
                        }

                        for(var i in nodes) {
                            for(var j in nodes[i].personsOfHours) {
                                if(j<startTime || j>endTime) {  // get cluster of certain hours
                                    continue ;
                                }

                                var _j = Math.floor((j-startTime) / hoursPerSector);

                                if(nodes[i].personsOfHours[j] > 0) {
                                    clus[_j].persons = clus[_j].persons + nodes[i].personsOfHours[j];

                                    if(!clusSet[_j].has(i)) {
                                        clus[_j].push(nodes[i]);
                                        clusSet[_j].add(i);
                                    }
                                }
                            }
                        }

                        generateMinMaxAngleOfClusters(clus);

                        return clus;
                    }

                    function generateMinMaxAngleOfClusters(clus) {
                        clus[0].minAngle = 0;
                        clus[0].maxAngle = 360;
                        if(hoursPerSector<=(endTime-startTime+1)) {
                            clus[0].maxAngle = hoursPerSector/(endTime-startTime+1)*360;
                        }

                        for(var i = 1; i < clus.length-1; ++i) {
                            clus[i].minAngle = clus[i-1].maxAngle;
                            clus[i].maxAngle = clus[i].minAngle + hoursPerSector/(endTime-startTime+1)*360;
                        }
                        if(clus.length > 1) {
                            clus[clus.length-1].minAngle = clus[clus.length-2].maxAngle;
                            clus[clus.length-1].maxAngle = 360;
                        }
                    }

                    function generateClusAngles(loyaltyLevel, clus) {
                        var clusAngles = [];

                        for(var j = 0; j < clus.length; ++j) {
                            var clusAngle = [];
                            clusAngle.startAngle = clus[j].minAngle;
                            clusAngle.endAngle = clus[j].maxAngle;

                            clusAngles.push(clusAngle);
                        }

                        // If grids are on the range of clusAngle, then simply add them.
                        for (var j = 0; j < clus.length; ++j) {
                            transformGridsToCluAngles(loyaltyLevel, clusAngles[j]);
                        }

                        // head and tail are normally not on a certain grid, so we need to calculate their info
                        generateClusAnglesHeadAndTail(clusAngles);

                        return clusAngles;
                    }

                    function transformGridsToCluAngles(loyaltyLevel ,cluAngles) {
                        var step = 360 / directionSliceNum;

                        cluAngles.length = 0;
                        var startAngle = cluAngles.startAngle;
                        var endAngle = cluAngles.endAngle;

                        var offset = 0;
                        if (endAngle <= startAngle) {
                            offset = 360;
                        }

                        var n = Math.floor((startAngle - step / 2) / step);

                        if (!funcService.isInRangeInclude(n * step + step / 2, startAngle, endAngle + offset)) {
                            n = n + 1;
                        }

                        while (funcService.isInRangeInclude(n * step + step / 2, startAngle, endAngle + offset)) {
                            var _n = n;
                            var angle = n * step + step / 2;
                            if (angle >= 360) {
                                angle = angle - 360;
                                _n = _n - directionSliceNum;
                            }

                            cluAngles.push({
                                angle: angle,
                                scaleFloor: loyaltyLevel - 1 < 0 ? 0 : grids[loyaltyLevel - 1][_n].scale,
                                scaleCeil: grids[i][_n].scale
                            });

                            n = n + 1;
                        }
                    }

                    function generateClusAnglesHeadAndTail(clusAngles) {
                        for(var j = 0; j < clus.length; ++j) {
                            var cluAngles = clusAngles[j];

                            var cluAngleHead = generateCluAngle(cluAngles.startAngle);
                            var cluAngleTail = generateCluAngle(cluAngles.endAngle);

                            // cluAngleHead is the head of cluAngles and cluAngleTail is the tail.
                            cluAngles.splice(0, 0, cluAngleHead);
                            cluAngles.push(cluAngleTail);
                        }
                    }

                    function generateCluAngle(gama) {   // calculate CluAngle, which the angle is not on certain grid
                        var step = 360 / directionSliceNum;

                        var cluAngle = {
                            angle: gama,
                            scaleFloor: 0,
                            scaleCeil: 0
                        };

                        var _gama = gama-step/2;
                        if(_gama<0) {
                            _gama = _gama+360;
                        }
                        if(_gama >= 360) {
                            _gama = _gama-360;
                        }

                        var pre_gridIndex = Math.floor(_gama / step);

                        if(pre_gridIndex*step == _gama) {
                            cluAngle.scaleFloor = i-1<0?0:grids[i-1][pre_gridIndex].scale;
                            cluAngle.scaleCeil = grids[i][pre_gridIndex].scale;

                        } else {
                            var succ_gridIndex = pre_gridIndex+1;

                            var alpha = step/2 + pre_gridIndex*step;
                            var beta = step/2 + succ_gridIndex*step;

                            if(succ_gridIndex >= directionSliceNum) {
                                succ_gridIndex = succ_gridIndex - directionSliceNum;

                                if(beta < alpha) {
                                    beta = beta + 360;
                                }
                                if(gama < alpha) {
                                    gama = gama + 360;
                                }
                            }

                            cluAngle.scaleFloor = 0;
                            if(i-1>=0) {
                                cluAngle.scaleFloor = getScaleInGivenLine(gama, alpha, grids[i-1][pre_gridIndex].scale, beta, grids[i-1][succ_gridIndex].scale);
                            }
                            cluAngle.scaleCeil = getScaleInGivenLine(gama, alpha, grids[i][pre_gridIndex].scale, beta, grids[i][succ_gridIndex].scale);
                        }

                        return cluAngle;
                    }


                    function getScaleInGivenLine(angle, angle1, scale1, angle2, scale2) {

                        var radian = angle/180*Math.PI;

                        var p1 = [scale1*Math.cos(angle1/180*Math.PI), scale1*Math.sin(angle1/180*Math.PI)];
                        var p2 = [scale2*Math.cos(angle2/180*Math.PI), scale2*Math.sin(angle2/180*Math.PI)];

                        var scale = 0;

                        if(p2[0]-p1[0] != 0) {
                            var k = (p2[1] - p1[1]) / (p2[0] - p1[0]);
                            var b = p1[1] - p1[0]*k;

                            scale = b/(Math.sin(radian)-k*Math.cos(radian));
                        } else {

                            scale = p1[0] / Math.cos(radian);
                        }

                        return scale;
                    }

                    function getAngleInGivenLine(scale, angle1, scale1, angle2, scale2) {   // precision of acos is too bad, sad~~~
                        var e = 1e-6;

                        if(Math.abs(scale-scale1) < e) {
                            return angle1;
                        }
                        if(Math.abs(scale-scale2) < e) {
                            return angle2;
                        }

                        var p1 = [scale1*Math.cos(angle1/180*Math.PI), scale1*Math.sin(angle1/180*Math.PI)];
                        var p2 = [scale2*Math.cos(angle2/180*Math.PI), scale2*Math.sin(angle2/180*Math.PI)];

                        if(p2[0]-p1[0] != 0) {
                            var k = (p2[1] - p1[1]) / (p2[0] - p1[0]);
                            var b = p1[1] - p1[0]*k;

                            var delta2 = b*b*k*k-(k*k+1)*(b*b-scale*scale);
                            var delta = Math.sqrt(delta2);

                            var cosAngle_1 = (-b*k+delta)/(k*k+1)/scale;
                            var sinAngle_1 = (k*scale*cosAngle_1+b)/scale;
                            var angle_1 = Math.acos(cosAngle_1);

                            if(Math.abs(Math.sin(angle_1) - sinAngle_1) > e) {
                                angle_1 = 2*Math.PI - angle_1;
                            }

                            angle_1 = angle_1/Math.PI*180;
                            if(angle_1<0) {
                                angle_1 = angle_1 + 360;
                            }
                            if(angle_1>=360) {
                                angle_1 = angle_1 - 360;
                            }

                            if(isInclude(angle_1, angle1, angle2)) {
                                return angle_1;
                            }

                            var cosAngle_2 = (-b*k-delta)/(k*k+1)/scale;
                            var sinAngle_2 = (k*scale*cosAngle_2+b)/scale;
                            var angle_2 = Math.acos(cosAngle_2);

                            if(Math.abs(Math.sin(angle_2) - sinAngle_2) > e) {
                                angle_2 = 2*Math.PI - angle_2;
                            }

                            angle_2 = angle_2/Math.PI*180;
                            if(angle_2<0) {
                                angle_2 = angle_2 + 360;
                            }
                            if(angle_2>=360) {
                                angle_2 = angle_2 - 360;
                            }

                            if(isInclude(angle_2, angle1, angle2)) {
                                return angle_2;
                            }

                        } else {
                            var cosAngle = p1[0] / scale;
                            var angle = Math.acos(cosAngle_1);
                            angle = angle/Math.PI*180;
                            if(isInclude(angle, angle1, angle2)) {
                                return angle;
                            } else {
                                angle = angle + 180;
                                if(isInclude(angle, angle1, angle2)) {
                                    return angle;
                                }
                            }
                        }

                        function isInclude(angle, angle1, angle2) {
                            var delta = angle-angle1;
                            if(delta < 0) {
                                delta = delta + 360;
                            }
                            var _delta = angle2-angle1;
                            if(_delta < 0) {
                                _delta = _delta + 360;
                            }

                            if(_delta >= delta) {
                                return true;
                            }

                            return false;
                        }

                        console.log("getAngleInGivenLine is Null !");
                        return null;
                    }

                    function constructTree(node, timeSlice) {
                        if(node.elements.length <= 1) {
                            return ;
                        }

                        var cutResult_vertical = verticalCut(node, timeSlice);

                        var cutResult_horozontal = horizontalCut(node, timeSlice);

                        node.children = cutResult_vertical;
                        if(cutResult_horozontal != null) {
                            var L_W_ratio_vertical = calculateLenthWidthRatioOfCutResult(cutResult_vertical);
                            var L_W_ratio_horizontal = calculateLenthWidthRatioOfCutResult(cutResult_horozontal);

                            if(L_W_ratio_horizontal <= L_W_ratio_vertical) {
                                node.children = cutResult_horozontal;
                            }
                        }

                        constructTree(node.children.left, timeSlice);
                        constructTree(node.children.right, timeSlice);
                    }

                    function calculateLenthWidthRatioOfCutResult(cutResult) {
                        var lwRatioLeft = calculateLenthWidthRatioOfNode(cutResult.left);
                        var lwRatioRight = calculateLenthWidthRatioOfNode(cutResult.right);

                        return (lwRatioLeft+lwRatioRight)/2;
                    }

                    function calculateLenthWidthRatioOfNode(node) {
                        var edges = node.edges;

                        var totalWidth = 0;
                        for(var i = 0; i < edges.length; ++i) {
                            var w1 = scaleToLength(edges[i].scaleFloor);
                            var w2 = scaleToLength(edges[i].scaleCeil);

                            totalWidth = totalWidth + w2 - w1;
                        }
                        var width = totalWidth / edges.length;

                        var totalUpLenth = 0;
                        var totalDownLenth = 0;
                        for(var i = 1; i < edges.length; ++i) {
                            var pre_up_p = getPoint_angle(edges[i-1].angle, scaleToLength(edges[i-1].scaleCeil));
                            var up_p = getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleCeil));
                            totalUpLenth = totalUpLenth + funcService.getDistance(pre_up_p, up_p);

                            var pre_down_p = getPoint_angle(edges[i-1].angle, scaleToLength(edges[i-1].scaleFloor));
                            var down_p = getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleFloor));
                            totalDownLenth = totalDownLenth + funcService.getDistance(pre_down_p, down_p);
                        }
                        var length = (totalUpLenth+totalDownLenth)/2;
                        var ratio = 1e9;
                        if(width > 0) {
                            ratio = length / width;
                        }
                        if(ratio <= 0) {
                            ratio = 1e9;
                        }
                        if(ratio < 1) {
                            ratio = 1 / ratio;
                        }

                        return ratio;
                    }

                    function verticalCut(node, timeSlice) {
                        return cut(node, sortType, "angle", timeSlice);
                    }

                    function horizontalCut(node, timeSlice) {
                        return cut(node, sortType, "scale", timeSlice);
                    }

                    function cut(node, elePropertyType, edgePropertyType, timeSlice) {

                        var elements = node.elements;
                        var edges = funcService.objectClone(node.edges);

                        elements.sort(function(a, b) {
                            if(elePropertyType == "distance") {
                                return b[elePropertyType] - a[elePropertyType];
                            } else {
                                return a[elePropertyType] - b[elePropertyType];
                            }
                        });

                        var totalPersons = 0;
                        for(var i = 0; i < elements.length; ++i) {
                            totalPersons = totalPersons + getPersonsOfTimeslice(i, timeSlice);
                        }

                        var accuPersons = 0;
                        var index = -1;
                        for(var i = 0; i < elements.length; ++i) {
                            if(accuPersons+getPersonsOfTimeslice(i, timeSlice) > totalPersons/2) {
                                break ;
                            } else {
                                accuPersons = accuPersons + getPersonsOfTimeslice(i, timeSlice);
                                index = i;
                            }
                        }
                        if(index == -1) {
                            index = 0;
                            accuPersons = accuPersons + getPersonsOfTimeslice(0, timeSlice);
                        }

                        function getPersonsOfTimeslice(eleID ,timeSlice) {
                            var persons = 0;

                            var timeSliceEnd = Math.min(endTime, startTime+(timeSlice+1)*hoursPerSector-1);

                            for(var j = startTime+timeSlice*hoursPerSector; j <= timeSliceEnd; ++j) {
                                persons = persons + elements[eleID].personsOfHours[j];
                            }

                            return persons;
                        }

                        var ratio = accuPersons/totalPersons;
                        var bisectResult = bisect(edges, ratio, edgePropertyType);
                        if(bisectResult == null) {
                            return null;
                        }

                        var leftChild = {
                            elements: [],
                            edges: []
                        };
                        var rightChild = {
                            elements: [],
                            edges: []
                        };

                        for (var i = 0; i <= index; ++i) {
                            leftChild.elements.push(elements[i]);
                        }
                        for (var i = index + 1; i < node.elements.length; ++i) {
                            rightChild.elements.push(elements[i]);
                        }

                        if(edgePropertyType == "angle") {
                            edges = node.edges;

                            leftChild.edges = calculateLeftEdges(edges, bisectResult.edgeIndex, bisectResult.edge);
                            rightChild.edges = calculateRightEdges(edges, bisectResult.edgeIndex, bisectResult.edge);
                        }

                        if(edgePropertyType == "scale") {
                            edges = node.edges;

                            leftChild.edges = calculateUpEdges(edges, bisectResult.scale, bisectResult.property, bisectResult.startIndex, bisectResult.endIndex);
                            rightChild.edges = calculateDownEdges(edges, bisectResult.scale, bisectResult.property, bisectResult.startIndex, bisectResult.endIndex);
                        }

                        return {
                            left: leftChild,
                            right: rightChild
                        };

                        function calculateLeftEdges(edges, bisectEdgeIndex, edge) {
                            var result = [];

                            for (var i = 0; i <= bisectEdgeIndex; ++i) {
                                result.push(edges[i]);
                            }
                            result.push(edge);

                            result.startAngle = edges.startAngle;
                            result.endAngle = edge.angle;

                            return result;
                        }

                        function calculateRightEdges(edges, bisectEdgeIndex, edge) {
                            var result = [];

                            result.push(edge);
                            for (var i = bisectEdgeIndex + 1; i < edges.length; ++i) {
                                result.push(edges[i]);
                            }

                            result.startAngle = edge.angle;
                            result.endAngle = edges.endAngle;

                            return result;
                        }

                        function calculateDownEdges(edges, scale, property, startIndex, endIndex) {
                            var result = [];
                            result.startAngle = edges.startAngle;
                            result.endAngle = edges.endAngle;

                            if(property == "scaleCeil") {
                                for(var i =0; i<startIndex; ++i) {
                                    result.push(edges[i]);
                                }
                            }

                            if(startIndex != 0) {
                                var startEdge = {
                                    angle: 0,
                                    scaleFloor: scale,
                                    scaleCeil: scale
                                };

                                var angle = getAngleInGivenLine(scale, edges[startIndex-1].angle, edges[startIndex-1][property], edges[startIndex].angle, edges[startIndex][property]);
                                startEdge.angle = angle;

                                if(property == "scaleCeil") {
                                    startEdge.scaleFloor = getScaleInGivenLine(angle, edges[startIndex-1].angle, edges[startIndex-1]["scaleFloor"], edges[startIndex].angle, edges[startIndex]["scaleFloor"]);
                                } else {
                                    result.startAngle = angle;
                                }

                                result.push(startEdge);
                            }

                            for(var i = startIndex; i <= endIndex; ++i) {
                                var edge = {
                                    angle: edges[i].angle,
                                    scaleFloor: edges[i].scaleFloor,
                                    scaleCeil: scale
                                };

                                result.push(edge);
                            }

                            if(endIndex != edges.length-1) {
                                var endEdge = {
                                    angle: 0,
                                    scaleFloor: scale,
                                    scaleCeil: scale
                                };


                                var angle = getAngleInGivenLine(scale, edges[endIndex].angle, edges[endIndex][property], edges[endIndex+1].angle, edges[endIndex+1][property]);
                                endEdge.angle = angle;

                                if(property == "scaleCeil") {
                                    endEdge.scaleFloor = getScaleInGivenLine(angle, edges[endIndex].angle, edges[endIndex]["scaleFloor"], edges[endIndex+1].angle, edges[endIndex+1]["scaleFloor"]);
                                }  else {
                                    result.endAngle = angle;
                                }

                                result.push(endEdge);
                            }

                            if(property == "scaleCeil") {
                                for(var i = endIndex+1; i<edges.length; ++i) {
                                    result.push(edges[i]);
                                }
                            }


                            return result;
                        }

                        function calculateUpEdges(edges, scale, property, startIndex, endIndex) {
                            var result = [];
                            result.startAngle = edges.startAngle;
                            result.endAngle = edges.endAngle;

                            if(property == "scaleFloor") {
                                for(var i =0; i<startIndex; ++i) {
                                    result.push(edges[i]);
                                }
                            }

                            if(startIndex != 0) {
                                var startEdge = {
                                    angle: 0,
                                    scaleFloor: scale,
                                    scaleCeil: scale
                                };

                                var angle = getAngleInGivenLine(scale, edges[startIndex-1].angle, edges[startIndex-1][property], edges[startIndex].angle, edges[startIndex][property]);
                                startEdge.angle = angle;

                                if(property == "scaleFloor") {
                                    startEdge.scaleFloor = getScaleInGivenLine(angle, edges[startIndex-1].angle, edges[startIndex-1]["scaleCeil"], edges[startIndex].angle, edges[startIndex]["scaleCeil"]);
                                } else {
                                    result.startAngle = angle;
                                }

                                result.push(startEdge);
                            }

                            for(var i = startIndex; i <= endIndex; ++i) {
                                var edge = {
                                    angle: edges[i].angle,
                                    scaleFloor: scale,
                                    scaleCeil: edges[i].scaleCeil
                                };

                                result.push(edge);
                            }

                            if(endIndex != edges.length-1) {
                                var endEdge = {
                                    angle: 0,
                                    scaleFloor: scale,
                                    scaleCeil: scale
                                };

                                var angle = getAngleInGivenLine(scale, edges[endIndex].angle, edges[endIndex][property], edges[endIndex+1].angle, edges[endIndex+1][property]);
                                endEdge.angle = angle;

                                if(property == "scaleFloor") {
                                    endEdge.scaleFloor = getScaleInGivenLine(angle, edges[endIndex].angle, edges[endIndex]["scaleCeil"], edges[endIndex+1].angle, edges[endIndex+1]["scaleCeil"]);
                                }  else {
                                    result.endAngle = angle;
                                }

                                result.push(endEdge);
                            }

                            if(property == "scaleFloor") {
                                for(var i = endIndex+1; i<edges.length; ++i) {
                                    result.push(edges[i]);
                                }
                            }

                            return result;
                        }

                        function bisect(edges, ratio, edgePropertyType) {

                            var polygon = constructPolygonOfEdges(edges);
                            var polygonFloor = constructPolygonOfEdgesOfFloor(edges);
                            var polygonCeil = constructPolygonOfEdgesOfCeil(edges);
                            var isConcave = {
                                general: funcService.isConcave(polygon),
                                floor: funcService.isConcave(polygonFloor),
                                ceil: funcService.isConcave(polygonCeil)
                            };
//                        isConcave = true;   // set true to avoid convex case

                            if(edgePropertyType == "scale") {
                                ratio = 1 - ratio;
                            }

                            var min_max = getMinMax(edges, edgePropertyType, isConcave);
                            var left = min_max.min;
                            var right = min_max.max;

                            if(left > right) {
                                return null;
                            }

                            var totalArea = funcService.calculateArea(polygon);

                            var bisectResult_left = calculateBisectResult(edges, left, totalArea, edgePropertyType);
                            var bisectResult_right = calculateBisectResult(edges, right, totalArea, edgePropertyType);

                            if(isFound(bisectResult_left, ratio)) {
                                return bisectResult_left;
                            }

                            if(isFound(bisectResult_right, ratio)) {
                                return bisectResult_right;
                            }

                            if((ratio-bisectResult_left.ratio)*(ratio-bisectResult_right.ratio) > 0) {
                                return null;
                            }

                            do {
                                var m = (left+right)/2;

                                var biserctResult = calculateBisectResult(edges, m, totalArea, edgePropertyType);

                                if(isFound(biserctResult, ratio)) {
                                    return biserctResult;
                                }

                                if(biserctResult.ratio < ratio) {
                                    left = m;
                                } else {
                                    right = m;
                                }

                            } while(true);


                            function getMinMax(edges, edgePropertyType, isConcave) {

                                var min = 1e9;
                                var max = -1e9;

                                var edgeP_1 = edgePropertyType;
                                var edgeP_2 = edgePropertyType;

                                if(edgePropertyType == "scale") {
                                    if(isConcave.floor) {
                                        min = -1e9;
                                    }
                                    if(isConcave.ceil) {
                                        max = 1e9;
                                    }
                                }

                                if(edgePropertyType == "scale") {
                                    edgeP_1 = edgePropertyType + "Floor";
                                    edgeP_2 = edgePropertyType + "Ceil";
                                }

                                for(var i = 0; i < edges.length; ++i) {
                                    if(edgePropertyType == "scale" && isConcave.floor) {
                                        if (edges[i][edgeP_1] > min) {
                                            min = edges[i][edgeP_1];
                                        }
                                    } else {
                                        if (edges[i][edgeP_1] < min) {
                                            min = edges[i][edgeP_1];
                                        }
                                    }

                                    if(edgePropertyType == "scale" && isConcave.ceil) {
                                        if(edges[i][edgeP_2] < max) {
                                            max = edges[i][edgeP_2];
                                        }
                                    } else {
                                        if(edges[i][edgeP_2] > max) {
                                            max = edges[i][edgeP_2];
                                        }
                                    }
                                }

                                return {
                                    min: min,
                                    max: max
                                }
                            }

                            function calculateBisectResult(edges, m, totalArea, edgePropertyType) {

                                if(edgePropertyType == "angle") {
                                    return calculateBisectResult_vertical(edges, m, totalArea);
                                }

                                if(edgePropertyType == "scale") {
                                    return calculateBisectResult_horizontal(edges, m, totalArea);
                                }
                            }

                            function calculateBisectResult_vertical(edges, m, totalArea) {
                                var edge = {
                                    angle: m,
                                    scaleFloor: 0,
                                    scaleCeil: 0
                                };
                                if(m > 360) {
                                    edge.angle = edge.angle - 360;
                                }

                                var index = -1;
                                for(var i = 0; i < edges.length-1; ++i) {
                                    index = i;

                                    if(edges[i].angle<=m && edges[i+1].angle>=m) {
                                        break;
                                    }
                                }

                                var alpha = edges[index].angle;
                                var beta = edges[index+1].angle;
                                var gama = m;

                                edge.scaleFloor = getScaleInGivenLine(gama, alpha, edges[index].scaleFloor, beta, edges[index+1].scaleFloor);
                                edge.scaleCeil = getScaleInGivenLine(gama, alpha, edges[index].scaleCeil, beta, edges[index+1].scaleCeil);


                                var leftEdges = calculateLeftEdges(edges, index, edge);
                                var polygon = constructPolygonOfEdges(leftEdges);

                                var leftArea = funcService.calculateArea(polygon);

                                var _ratio = leftArea/totalArea;

                                return {
                                    ratio: _ratio,
                                    edge: edge,
                                    edgeIndex: index
                                }
                            }

                            function calculateBisectResult_horizontal(edges, m, totalArea) {
                                var scale = m;

                                var property = "scaleCeil";
                                if(m < edges[0].scaleFloor || m < edges[edges.length-1].scaleFloor) {
                                    property = "scaleFloor";
                                }

                                for(var startIndex = 0; startIndex < edges.length; ++startIndex) {
                                    if(edges[startIndex].scaleFloor <= m && edges[startIndex].scaleCeil >=m) {
                                        break ;
                                    }
                                }

                                for(var endIndex = edges.length-1; endIndex >= 0; --endIndex) {
                                    if(edges[endIndex].scaleFloor <= m && edges[endIndex].scaleCeil >=m) {
                                        break ;
                                    }
                                }

                                var downEdges = calculateDownEdges(edges, scale, property, startIndex, endIndex);
                                var polygon = constructPolygonOfEdges(downEdges);
                                var downArea = funcService.calculateArea(polygon);

                                var _ratio = downArea/totalArea;

                                return {
                                    ratio: _ratio,
                                    scale: scale,
                                    property: property,
                                    startIndex: startIndex,
                                    endIndex: endIndex
                                }
                            }

                            function isFound(biserctResult, ratio) {
                                var e = 1e-6;

                                if(Math.abs(biserctResult.ratio-ratio)<e) {
                                    return true;
                                }

                                return false;

                            }
                        }
                    }

                    function drawTreemap(node) {

                        var polygon = constructPolygonOfEdges(node.edges);

                        var fill = "none";
                        var opa = 1;


                        if(node.elements.length == 1) {
                            fill = "green";
                            opa = node.elements[0].distance;

                            var fillColor = d3.scale.linear()
                                .range(['rgb(247,252,245)','rgb(229,245,224)','rgb(199,233,192)','rgb(161,217,155)','rgb(116,196,118)','rgb(65,171,93)','rgb(65,171,93)'])
                                .domain([0.2,0.05*maxDistance,0.1*maxDistance,0.5*maxDistance,0.7*maxDistance, 0.9*maxDistance, maxDistance]);

                            drawLegend();

                            var nid = node.elements[0].nid;
                            var nodeData = mapService.data.map.source[nid];

                            var _marker = null;

                            radialGraph_g
                                .append("path")
                                .attr("class", "treemap_node_"+nid)
                                .attr("d", funcService.polygon(polygon))
                                .attr("fill", fillColor((opa)))
                                .attr("fill-opacity", 1)
                                .attr("stroke", "black")
                                .attr("stroke-width", 0.3)
                                .attr("stroke-opacity", 0.8)
                                .on("mouseover", function() {
                                    d3.selectAll(".treemap_node_"+nid)
                                        .attr("stroke", "red")
                                        .attr("stroke-width", 2.5)
                                        .attr("stroke-opacity", 0.7);

                                    if(selected_nid == -1) {
                                        mapService.map.source.setView([nodeData.gps.lat, nodeData.gps.lng]);
                                    }

//                                    if(mapService.data.d3Overlay.sel.source) {
//                                        mapService.data.d3Overlay.sel.source
//                                            .selectAll("circle")
//                                            .attr("display", "none");
//                                    }

                                    _marker = mapService.map.source.addGreenMarker(nodeData.gps.lat, nodeData.gps.lng);
                                })
                                .on("mouseout", function() {

                                    if(_marker != null) {
                                        mapService.map.source.removeLayer(_marker);
                                        _marker = null;
                                    }

                                    if(selected_nid != nid) {
                                        d3.selectAll(".treemap_node_" + nid)
                                            .attr("stroke", "black")
                                            .attr("stroke-width", 1)
                                            .attr("stroke-opacity", 0.3);
                                    }

//                                    if(mapService.data.d3Overlay.sel.source) {
//                                        mapService.data.d3Overlay.sel.source
//                                            .selectAll("circle")
//                                            .attr("display", "");
//                                    }

                                })
                                .on("click", function() {
                                    if(marker != null) {
                                        mapService.map.source.removeLayer(marker);
                                        marker = null;

                                        d3.selectAll(".treemap_node_"+selected_nid)
                                            .attr("stroke", "black")
                                            .attr("stroke-width", 1)
                                            .attr("stroke-opacity", 0.3);
                                    }

                                    if(selected_nid == nid) {
                                        selected_nid = -1;

                                        return ;
                                    }

                                    if(mapService.map.source) {
                                        mapService.map.source.setView([nodeData.gps.lat, nodeData.gps.lng]);
                                    }

                                    selected_nid = nid;
                                    marker = _marker;
                                    _marker = null;
                                });
                        }

                        if(node.elements.length > 1) {
                            drawTreemap(node.children.left);
                            drawTreemap(node.children.right);
                        }
                    }

                    function constructPolygonOfEdges(edges) {
                        var polygon = [];
                        for(var i = 0; i < edges.length; ++i) {
                            polygon.push(getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleFloor)));
                        }
                        for(var i = edges.length-1; i >= 0; --i) {
                            polygon.push(getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleCeil)));
                        }

                        return polygon;
                    }

                    function constructPolygonOfEdgesOfFloor(edges) {
                        var polygon = [];
                        for(var i = 0; i < edges.length; ++i) {
                            polygon.push(getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleFloor)));
                        }

                        return polygon;
                    }

                    function constructPolygonOfEdgesOfCeil(edges) {
                        var polygon = [];
                        for(var i = edges.length-1; i >= 0; --i) {
                            polygon.push(getPoint_angle(edges[i].angle, scaleToLength(edges[i].scaleCeil)));
                        }

                        return polygon;
                    }

                    function scaleToLength(scale) {
                        var len = 0;
                        if(maxScale > 0) {
                            len = scale / maxScale * maxLength;
                        }

                        return len;
                    }

                    function getPoint(_radian, len) {
                        var radian = _radian - Math.PI/2;

                        var point = [len*Math.cos(radian), len*Math.sin(radian)];
                        return point;
                    }

                    function getPoint_angle(_angle, len) {
                        var angle = _angle - 90;

                        var radian = angle/180*Math.PI;

                        var point = [len*Math.cos(radian), len*Math.sin(radian)];
                        return point;
                    }

                    function drawClockDial() {
                        var radius = scaleToLength(maxScale) + 5;
                        var outer_circle = radialGraph_g.append("circle")
                            .attr("r", function () {
                                return radius;
                            })
                            .attr("cx", 0)
                            .attr("cy", 0)
                            .attr("fill", "none")
                            .attr("stroke", "black")
                            .attr("stroke-width", 1)
                            .attr("stroke-opacity", 1);

                        var outer_circle2 = radialGraph_g.append("circle")
                            .attr("r", function () {
                                return radius + 7;
                            })
                            .attr("cx", 0)
                            .attr("cy", 0)
                            .attr("fill", "none")
                            .attr("stroke", "dodgerblue")
                            .attr("stroke-width", 5)
                            .attr("stroke-opacity", 1);

                        var step = 360 * hoursPerSector / (endTime - startTime + 1);
                        var i = 0;
                        while (i * hoursPerSector + startTime <= endTime) {
                            var point = getPoint_angle(step * i, radius);
                            radialGraph_g.append("line")
                                .attr("x1", 0)
                                .attr("y1", 0)
                                .attr("x2", point[0])
                                .attr("y2", point[1])
                                .attr("stroke", "#f46d43")
                                .attr("stroke-width", 2)
                                .attr("stroke-opacity", 0.8)
                                .style("stroke-dasharray", 3);

                            point = getPoint_angle(step * i, radius + 22);
                            radialGraph_g.append("text")
                                .attr("transform", "translate(" + (point[0] - 6) + "," + (point[1] + 6) + ")")
                                .text(function () {
                                    return i * hoursPerSector + startTime;
                                }).style("font-weight", "bold");

                            i = i + 1;
                        }

                        radialGraph_g.append("text")
                            .text("Visitors: " + totalPersonsOfGraph)
                            .attr("transform", "translate(100, -180)");
                    }
                }
            }
        }
    }
}]);
'use strict';

angular.module('radialGraph').factory('radialGraphService',
    function() {
        var radialGraphService = {};

        return radialGraphService;
    }
);