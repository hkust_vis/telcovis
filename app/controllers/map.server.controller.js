'use strict';

var mongoose = require('mongoose');
var _ = require('lodash');
var func = require('./func.server.controller.js');

exports.getMapInfo = getMapInfo;

exports.getHeatMapSource = getHeatMapSource;
exports.getHeatMapDest = getHeatMapDest;

exports.getCooccurrence = getCooccurrenceSource;

function getMapInfo(req, res) {
    var mongoPara = {
        modelName: 'mapInfo',
        collectionName: 'map_info'
    };

    func.getDataMongo(mongoPara, function(data) {

        res.send(data[0].value);
        res.end();
    });
}

// get heatmap of flow-out map
function getHeatMapSource(req, res) {
    getHeatMap(req, res, 'source');
}

// get heatmap of flow-in map
function getHeatMapDest(req, res) {
    getHeatMap(req, res, 'destination');
}

// general get heatmap data function
function getHeatMap(req, res, type) {
    var paras = req.body;

    var dateList = paras.dateList;

    var mongoParas = generateMongoParas();

    func.getDataListMongo(mongoParas, function(dataList) {
        var result = combineHeatMapDataList(dataList);

        res.send(result);
        res.end();
    });

    function generateMongoParas() {
        var modelNames = [];
        var collectionNames = [];
        var findParas = [];

        for(var i = 0; i < dateList.length; ++i) {
            modelNames.push('heatMap');
            collectionNames.push('heatmap_' + type);
            findParas.push({
                date: dateList[i]
            });
        }

        return {
            modelNames: modelNames,
            collectionNames: collectionNames,
            findParas: findParas
        };
    }
}

// combine heatmap data of different dates
function combineHeatMapDataList(dataList) {
    var data = dataList[0][0].value;

    for(var i = 1; i < dataList.length; ++i) {
        var value = dataList[i][0].value;
        for(var j in value) {
            if(data.hasOwnProperty(j)) {
                combineHeatMapDataItem(data[j], value[j]);
            } else {
                data[j] = value[j];
            }
        }
    }

    return data;

    function combineHeatMapDataItem(itemA, itemB) {
        for(var i in itemB) {
            if(itemA.hasOwnProperty(i)) {
                itemA[i] = _.union(itemA[i], itemB[i]);
            } else {
                itemA[i] = itemB[i];
            }
        }
    }
}

// get cooccurrence of flow-out map
function getCooccurrenceSource(req, res) {
    var paras = req.body;

    var dateList = paras.dateList;
    for(var i in dateList) {
        dateList[i] = +dateList[i];
    }
    var hours = paras.hours;

    var selectedSource = paras.selectedSource;
    var selectedDest = paras.selectedDest;

    var mongoParas = generateMongoParas();

    func.getDataListMongo(mongoParas, function(sourceDataList) {
        var hourSet = {};
        var destSet = {};

        for(var i in sourceDataList) {
            for(var j in sourceDataList[i]){
                var node = sourceDataList[i][j];

                var nodeID = node.gpsID;
                var nodeHour = node.hour;
                var nodeSource = node.value.source;
                var nodeDest = node.value.destination;

                if (isMN(nodeSource, nodeDest, selectedSource, selectedDest)) {
                    if (hourSet.hasOwnProperty(nodeID)) {
                        hourSet[nodeID].push(nodeHour);
                    } else {
                        hourSet[nodeID] = [nodeHour];
                    }

                    if (destSet.hasOwnProperty(nodeID)) {
                        destSet[nodeID] = _.union(destSet[nodeID], nodeDest);
                    } else {
                        destSet[nodeID] = nodeDest;
                    }
                }
            }
        }

        getCooccurrenceDest(req, res, selectedSource, selectedDest, dateList, hourSet, destSet);
    });

    function generateMongoParas() {
        var modelNames = [];
        var collectionNames = [];
        var findParas = [];

        for(var i = 0; i < dateList.length; ++i) {
            modelNames.push('cooccurrenceSource');
            collectionNames.push('cooccurrence_source');
            findParas.push({
                date: +dateList[i],
                hour: {
                    '$in': hours
                },
                gpsID: {
                    '$in': selectedSource
                }
            });
        }

        return {
            modelNames: modelNames,
            collectionNames: collectionNames,
            findParas: findParas
        };
    }
}

// is m and n lists contain all elements in selected source and destination
function isMN(mList, nList, selectedSource, selectedDest) {
    if (_.intersection(mList, selectedSource).length == selectedSource.length && _.intersection(nList, selectedDest).length == selectedDest.length) {
        return true;
    }

    return false;
}

// get cooccurrence of flow-in map
function getCooccurrenceDest(req, res, selectedSource, selectedDest, dateList, hourSet, destSet) {
    var mongoParas = generateMongoParas();

    func.getDataListMongo(mongoParas, function(destDataList) {
        var personsSet = {};
        var combinations = [];
        for(var i in destDataList) {
            for(var j in destDataList[i]) {
                var node = destDataList[i][j];

                var gpsID = node.gpsID;
                var stationID = node.stationID;
                var personsNum = node.value.persons_num;

                if(personsSet.hasOwnProperty(stationID)) {
                    personsSet[stationID] += personsNum;
                } else {
                    personsSet[stationID] = personsNum;
                }

                for(var k in destDataList[i][j].value.m_n) {
                    var m_n = destDataList[i][j].value.m_n[k];
                    if(isMN(m_n.m, m_n.n, selectedSource, selectedDest)) {
                        combinations.push(m_n);
                    }
                }
            }
        }

        var sourceCircles = generateSourceCircles(combinations, selectedDest);
        var destCircles = generateDestCircles(combinations, selectedSource, personsSet);

        res.send({
            sourceCircles: sourceCircles,
            destCircles: destCircles
        });
        res.end();
    });

    function generateMongoParas() {
        var modelNames = [];
        var collectionNames = [];
        var findParas = [];

        for(var i = 0; i < selectedSource.length; ++i) {
            var nid = +selectedSource[i];

            modelNames.push('cooccurrenceDest');
            collectionNames.push('cooccurrence_destination');
            findParas.push({
                date: {
                    '$in': dateList
                },
                hour: {
                    '$in': hourSet[nid]
                },
                gpsID: nid,
                stationID: {
                    '$in': destSet[nid]
                }
            });
        }

        return {
            modelNames: modelNames,
            collectionNames: collectionNames,
            findParas: findParas
        };
    }

    // generate information of source circles, e.g. region number
    function generateSourceCircles(combinations, selectedDest) {
        var sourceSet = {};
        for(var i in combinations) {
            var mn = combinations[i];

            for(var j in mn.m) {
                var m = mn.m[j];
                if(sourceSet.hasOwnProperty(m)) {
                    sourceSet[m] = _.union(sourceSet[m], mn.n);
                } else {
                    sourceSet[m] = mn.n;
                }
            }
        }

        for(var i in sourceSet) {
            sourceSet[i] = {
                id: +i,
                regionNum: sourceSet[i].length,
                r: sourceSet[i].length
            };
        }

        return sourceSet;
    }

    // generate information of destination circles, e.g. region number and person number
    function generateDestCircles(combinations, selectedSource, personsSet) {
        var destSet = {};
        for(var i in combinations) {
            var mn = combinations[i];

            for(var j in mn.n) {
                var n = mn.n[j];
                if(destSet.hasOwnProperty(n)) {
                    destSet[n] = _.union(destSet[n], mn.m);
                } else {
                    destSet[n] = mn.m;
                }
            }
        }

        for(var i in destSet) {
            destSet[i] = {
                id: +i,
                regionNum: destSet[i].length,
                personNum: personsSet[i],
                r: destSet[i].length
            };
        }

        return destSet;
    }
}

