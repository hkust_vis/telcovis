'use strict';

angular.module('map').directive('mapDirective', ["mapService", 'dataService', 'funcService', 'pipService', 'radialGraphService', 'matrixService', function(mapService, dataService, funcService, pipService, radialGraphService, matrixService) {
    return {
        restrict: 'A',

        scope: false,

        link: function (scope, element, attributes) {
            mapService.clear = clear;
            setHourRangeSlider();
            mapService.loadData();

            pipService.onMapDataChange(scope, function() {
                setMap();
            });

            pipService.onHoursChange(scope, function(hours) {
                $("#source_slider-range").slider({
                    values: [hours[0], hours[hours.length-1]]
                });

                $("#dest_slider-range").slider({
                    values: [hours[0], hours[hours.length-1]]
                });

                mapService.map.source.setHours(hours);
                mapService.map.dest.setHours(hours);

                mapService.updateCircles();
            });

            // clear map
            function clear() {
                if(mapService.map.source) {
                    mapService.map.source.remove();
                }
                if(mapService.map.dest) {
                    mapService.map.dest.remove();
                }

                $('#source_map').empty();
                $('#dest_map').empty();
            }

            // init hour range slider
            function setHourRangeSlider() {
                $(function () {
                    $("#source_slider-range").slider({
                        range: true,
                        min: 0,
                        max: 23,
                        values: [ 0, 23 ],
                        step: 1,
                        slide: function (event, ui) {
                            var start = ui.values[ 0 ];
                            var end  = ui.values[ 1 ];

                            $("#source_hour_range").val(start + " - " + end);

                            var hours = d3.range(start, end+1, 1);
                            dataService.setHours(hours);
                        }
                    });
                    $("#source_hour_range")
                        .val($("#source_slider-range").slider("values", 0) +
                        " - " + $("#source_slider-range").slider("values", 1));
                });

                $(function () {
                    $("#dest_slider-range").slider({
                        range: true,
                        min: 0,
                        max: 23,
                        values: [ 0, 23 ],
                        step: 1,
                        slide: function (event, ui) {
                            var start = ui.values[ 0 ];
                            var end  = ui.values[ 1 ];

                            $("#dest_hour_range").val(ui.values[ 0 ] + " - " + ui.values[ 1 ]);

                            var hours = d3.range(start, end+1, 1);
                            dataService.setHours(hours);
                        }
                    });
                    $("#dest_hour_range").val($("#dest_slider-range").slider("values", 0) +
                        " - " + $("#dest_slider-range").slider("values", 1));
                });
            }

            // set map
            function setMap() {
                mapService.updateCircles = updateCircles;

                var maxCircleR = 12;
                var destCircleSizeIndex = "regionNum";

                var green_marker_icon = L.icon({
                    iconUrl: 'lib/leaflet/dist/images/marker-icon-green.png',
                    iconRetinaUrl: 'lib/leaflet/dist/images/marker-icon-green-2x.png',
                    iconSize: [25, 40],
                    iconAnchor: [12.5, 35],
                    popupAnchor: [0, -32],
                    shadowUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowRetinaUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowSize: [41, 36],
                    shadowAnchor: [12.5, 35]
                });

                var purple_marker_icon = L.icon({
                    iconUrl: 'lib/leaflet/dist/images/marker-icon-purple.png',
                    iconRetinaUrl: 'lib/leaflet/dist/images/marker-icon-purple-2x.png',
                    iconSize: [25, 40],
                    iconAnchor: [12.5, 40],
                    popupAnchor: [0, -36],
                    shadowUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowRetinaUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowSize: [41, 41],
                    shadowAnchor: [12.5, 40]
                });

                var yellow_marker_icon = L.icon({
                    iconUrl: 'lib/leaflet/dist/images/marker-icon-yellow.png',
                    iconRetinaUrl: 'lib/leaflet/dist/images/marker-icon-yellow-2x.png',
                    iconSize: [25, 40],
                    iconAnchor: [12.5, 40],
                    popupAnchor: [0, -36],
                    shadowUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowRetinaUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowSize: [41, 41],
                    shadowAnchor: [12.5, 40]
                });

                var red_marker_icon = L.icon({
                    iconUrl: 'lib/leaflet/dist/images/marker-icon-red.png',
                    iconRetinaUrl: 'lib/leaflet/dist/images/marker-icon-red-2x.png',
                    iconSize: [25, 40],
                    iconAnchor: [12.5, 40],
                    popupAnchor: [0, -36],
                    shadowUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowRetinaUrl: 'lib/leaflet/dist/images/marker-shadow.png',
                    shadowSize: [41, 41],
                    shadowAnchor: [12.5, 40]
                });

                // map options
                var sourceOptions = {
                    heatmapOptions: {
                        minOpacity: 0,
                        maxZoom: 30,
                        minZoom:5,
                        max: 0.0003,
                        radius: 18,
                        blur:20,
                        gradient: {
                            0:"#fff5eb",
                            0.2:"#fee6ce",
                            0.4: "#fdd0a2",
                            0.6:"#fdae6b",
                            0.8:"#fd8d3c",
                            1:"#e31a1c"
                        }
                    }
                };
                var sourceMapDat = constructMapDat(mapService.data.info, mapService.data.heatMap.source);
                mapService.data.map.source = sourceMapDat;
                var sourceMap = constructSourceMap(sourceMapDat, mapService.data.heatMap.source, sourceOptions);
                mapService.map.source = sourceMap;

                var destOptions = {
                    heatmapOptions: {
                        minOpacity: 0,
                        maxZoom: 30,
                        minZoom: 5,
                        max: 0.0002,
                        radius: 15, //18
                        blur:15,//20
                        gradient: {
                            0:"#fff5eb",
                            0.2:"#fee6ce",
                            0.4: "#fdd0a2",
                            0.6:"#fdae6b",
                            0.8:"#fd8d3c",
                            1:"#e31a1c"
                        }
                    }
                };
                var destMapDat = constructMapDat(mapService.data.info, mapService.data.heatMap.dest);
                mapService.data.map.dest = destMapDat;
                var destMap = constructDestMap(destMapDat, mapService.data.heatMap.dest, destOptions);
                mapService.map.dest = destMap;

                function updateCircles() {
                    if(mapService.map.source.pinnedNodes.length == 0 && mapService.map.dest.pinnedNodes.length == 0) {
                        drawCircles({}, {});
                    } else {
                        mapService.getCooccurrence(mapService.map.source.pinnedNodes, mapService.map.dest.pinnedNodes, function (data) {
                            var sourceCircles = data.sourceCircles;
                            var destCircles = data.destCircles;

                            drawCircles(sourceCircles, destCircles);
                        });
                    }
                }

                // draw coocurrence circles
                function drawCircles(sourceCircles, destCircles) {
                    mapService.filterSourceCircles = filterSourceCircles;
                    mapService.filterDestCircles = filterDestCircles;

                    mapService.data.circles.source = sourceCircles;
                    mapService.data.circles.dest = destCircles;


                    var sourceCirclesList = _.values(sourceCircles);
                    var destCirclesList = _.values(destCircles);

                    mapService.setDestCircleSize = setDestCircleSize;

                    var mapDat = mapService.data.info;

                    var sourceCirclesMaxR = _.max(sourceCirclesList, function(d) {
                        return d.r;
                    }).r;
                    if(sourceCirclesMaxR != 0) {
                        sourceCirclesList.forEach(function (d) {
                            d.r = d.r / sourceCirclesMaxR * maxCircleR;
                        });
                    }

                    var destCirclesMaxR = _.max(destCirclesList, function(d) {
                        return d.r;
                    }).r;
                    if(destCirclesMaxR != 0) {
                        destCirclesList.forEach(function (d) {
                            d.r = d.r / destCirclesMaxR * maxCircleR;
                        });
                    }

                    var overlaySelSource = mapService.data.d3Overlay.sel.source;
                    var overlaySelDest = mapService.data.d3Overlay.sel.dest;

                    var overlayProjSource = mapService.data.d3Overlay.proj.source;
                    var overlayProjDest = mapService.data.d3Overlay.proj.dest;

                    overlaySelSource.selectAll("circle").remove();
                    overlaySelDest.selectAll("circle").remove();

                    var co_nodes = overlaySelSource.selectAll("circle")
                        .data(sourceCirclesList)
                        .enter()
                        .append("circle")
                        .attr("class", "related_node")
                        .attr("r", function (d) {
                            return d.r;
                        })
                        .attr("transform", function (d) {
                            var point = overlayProjSource.latLngToLayerPoint([mapDat[d.id].gps.lat, mapDat[d.id].gps.lng]);

                            return "translate(" + point.x + "," + point.y + ")";
                        })
                        .attr("stroke", "grey")
                        .attr("stroke-width", 0.6)
                        .attr("stroke-opacity",0.6)
                        .attr("fill", "#ffffcc")
                        .attr("fill-opacity", "0.8");

                    var dest_nodes = overlaySelDest.selectAll("circle")
                        .data(destCirclesList)
                        .enter()
                        .append("circle")
                        .attr("class", "related_node")
                        .attr("r", function (d) {
                            return d.r;
                        })
                        .attr("transform", function (d) {
                            var point = overlayProjDest.latLngToLayerPoint([mapDat[d.id].gps.lat, mapDat[d.id].gps.lng]);

                            return "translate(" + point.x + "," + point.y + ")";
                        })
                        .attr("stroke", "grey")
                        .attr("stroke-width", 0.6)
                        .attr("stroke-opacity",0.6)
                        .attr("fill", "#ffffcc")
                        .attr("fill-opacity", "0.8");

                    var packerSource = sm.packer();
                    packerSource.elements(overlaySelSource.selectAll("circle")[0]).start();

                    var packerDest = sm.packer();
                    packerDest.elements(overlaySelDest.selectAll("circle")[0]).start();

                    function setDestCircleSize(index) {
                        destCircleSizeIndex = index;

                        for(var i in destCirclesList) {
                            destCirclesList[i].r = destCirclesList[i][index];
                        }

                        var destCirclesMaxR = _.max(destCirclesList, function(d) {
                            return d.r;
                        }).r;
                        if(destCirclesMaxR > 0) {
                            destCirclesList.forEach(function (d) {
                                d.r = d.r / destCirclesMaxR * maxCircleR;
                            });
                        }

                        mapService.data.d3Overlay.sel.dest
                            .selectAll("circle")
                            .attr("r", function(d) {
                                return d.r;
                            });

                        packerDest.start();
                    }

                    function filterSourceCircles(leastNum) {
                        mapService.data.d3Overlay.sel.source
                            .selectAll("circle")
                            .attr("display", function(d) {
                                return d.regionNum>=leastNum?'':'none';
                            });
                        packerSource.start();
                    }

                    function filterDestCircles(leastNum) {
                        mapService.data.d3Overlay.sel.dest
                            .selectAll("circle")
                            .attr("display", function(d) {
                                return d[mapService.destCircleSizeIndex]>=leastNum?'':'none';
                            });
                        packerDest.start();
                    }
                }

                function constructSourceMap(mapDat, heatMapDat, options) {
                    var containerText = "source_map";

                    var map = L.map(containerText, {
                        // Guang zhou [23.13692, 113.33064]
                        // Shang hai [31.22, 121.48]
                        center: [31.22, 121.48],
                        zoom: 11,
                        maxZoom:16,
                        minZoom:10,
                        doubleClickZoom: false
                    });
                    map.addGreenMarker = addGreenMarker;
                    map.addPurpleMarker = addPurpleMarker;
                    map.addBlueMarkerNid = addBlueMarkerNid;

                    function addGreenMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: green_marker_icon}).addTo(map);
                    }

                    function addPurpleMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: purple_marker_icon}).addTo(map);
                    }

                    function addBlueMarkerNid(nid) {
                        return L.marker([mapDat[nid].gps.lat, mapDat[nid].gps.lng]).addTo(map)
                            .bindPopup("Cell Id: " + nid + "</br>" +
                                "Latitude: " + mapDat[nid].gps.lat + "</br>" +
                                "Longitude: " + mapDat[nid].gps.lng);
                    }

                    // add an OpenStreetMap tile layer
                    var HERE_normalDay = L.tileLayer('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/maptile/{mapID}/normal.day/{z}/{x}/{y}/256/png8?app_id={app_id}&app_code={app_code}', {
                        attribution: 'Map &copy; 1987-2014',
                        subdomains: '1234',
                        mapID: 'newest',
                        app_id: 'Y8m9dK2brESDPGJPdrvs',
                        app_code: 'dq2MYIvjAotR8tHvY8Q_Dg',
                        base: 'base',
                        minZoom: 0,
                        maxZoom: 20
                    }).addTo(map);

                    var heatmapDataOfDay = generateHeatData(d3.range(24), mapDat, heatMapDat);
                    var heat = L.heatLayer(heatmapDataOfDay, options.heatmapOptions).addTo(map);


                    map.on('zoomend', function() {
                        var currentZoom = map.getZoom();
                        heat.setOptions({radius:(1.5*currentZoom),blur:((5/3)*currentZoom)});
                    });

                    var marker = undefined;
                    var isMarker = false;

                    var pinnedMarker = [];
                    map.pinnedNodes = [];

                    var d3Overlay = L.d3SvgOverlay(function(sel, proj) {
                        mapService.data.d3Overlay.sel.source = sel;
                        mapService.data.d3Overlay.proj.source = proj;
                    });
                    d3Overlay.addTo(map);

                    $("#source_marker_control").bind("click", setIsMarker);
                    $("#source_reset").bind("click", reset);

                    map.on({
                        // marker follows mouse
                        mousemove: function(e) {
                            if(!isMarker) {
                                return ;
                            }

                            if(marker) {
                                map.removeLayer(marker);
                                marker = undefined;
                            }

                            var min = 1e9;
                            var minID = -1;
                            for(var i in mapDat) {
                                if(map.pinnedNodes.length > 0 && mapService.data.circles.source && !mapService.data.circles.source.hasOwnProperty(i)) {
                                    continue ;
                                }

                                var dis = funcService.getDistance([e.latlng.lat, e.latlng.lng], [mapDat[i].gps.lat, mapDat[i].gps.lng]);
                                if(dis < min) {
                                    min = dis;
                                    minID = i;
                                }
                            }

                            if(minID != -1) {
                                marker = addBlueMarkerNid(minID);

                                marker.on({
                                    dblclick: function () {
                                        map.pinnedNodes.push(+minID);
                                        pinnedMarker.push(marker);
                                        marker = undefined;

                                        mapService.updateCircles();

                                        if(map.pinnedNodes.length >= 2) {
                                            var sources = map.pinnedNodes.slice(-2);
                                            pipService.emitFlowMapSourceChange(sources);
                                        }

                                        matrixService.setSelectedSource(map.pinnedNodes);
                                    }
                                });
                            }
                        }
                    });

                    function setIsMarker() {
                        isMarker = !isMarker;

                        if(!isMarker && marker) {
                            map.removeLayer(marker);
                            marker = undefined;
                        }
                    }

                    // reset
                    function reset() {
                        for(var i in pinnedMarker) {
                            var _marker = pinnedMarker[i];
                            map.removeLayer(_marker);
                        }

                        pinnedMarker = [];
                        map.pinnedNodes = [];

                        mapService.data.d3Overlay.sel.source.selectAll("circle").remove();
                        mapService.data.d3Overlay.sel.dest.selectAll("circle").remove();

                        pipService.emitFlowMapSourceChange(map.pinnedNodes);

                        matrixService.setSelectedSource(map.pinnedNodes);
                        destMap.reset();
                    }

                    map.setHours = setHours;
                    // set hours
                    function setHours(hours) {
                        if(heat != null) {
                            map.removeLayer(heat);
                            heat = null;
                        }

                        if(hours.length >= 24) {
                            heat = L.heatLayer(heatmapDataOfDay, options.heatmapOptions).addTo(map);

                            return ;
                        }

                        var heatmapData = generateHeatData(hours, mapDat, heatMapDat);
                        heat = L.heatLayer(heatmapData, options.heatmapOptions).addTo(map);
                    }

                    return map;
                }


                function constructDestMap(mapDat, heatMapDat, options) {
                    var containerText = "dest_map";

                    var map = L.map(containerText, {
                        // Guang zhou [23.13692, 113.33064]
                        // Shang hai [31.22, 121.48]
                        center: [31.22, 121.48],
                        zoom: 11,
                        maxZoom: 16,
                        minZoom: 10,
                        doubleClickZoom: false
                    });

                    map.addGreenMarker = addGreenMarker;
                    map.addGreenMarkerNid = addGreenMarkerNid;
                    map.addYellowMarker = addYellowMarker;
                    map.addRedMarker = addRedMarker;
                    map.addRedMarkerNid = addRedMarkerNid;
                    map.addBlueMarkerNid = addBlueMarkerNid;

                    function addGreenMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: green_marker_icon}).addTo(map);
                    }

                    function addGreenMarkerNid(nid) {
                        return L.marker([mapDat[nid].gps.lat, mapDat[nid].gps.lng], {icon: green_marker_icon}).addTo(map)
                            //.bindPopup(minID + " " + mapDat[minID].poi);
                            .bindPopup("Cell Id: " + nid + "</br>" +
                                "Latitude: " + mapDat[nid].gps.lat + "</br>" +
                                "Longitude: " + mapDat[nid].gps.lng //+ "</br>" +
                            //"Ref. POI: " + mapDat[minID].poi
                        );
                    }

                    function addYellowMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: yellow_marker_icon}).addTo(map);
                    }

                    function addRedMarker(lat, lng) {
                        return L.marker([lat, lng], {icon: red_marker_icon}).addTo(map);
                    }

                    function addRedMarkerNid(nid) {
                        return L.marker([mapDat[nid].gps.lat, mapDat[nid].gps.lng], {icon: red_marker_icon}).addTo(map);
                    }

                    function addBlueMarkerNid(nid) {
                        return L.marker([mapDat[nid].gps.lat, mapDat[nid].gps.lng]).addTo(map)
                            //.bindPopup(minID + " " + mapDat[minID].poi);
                            .bindPopup("Cell Id: " + nid + "</br>" +
                                "Latitude: " + mapDat[nid].gps.lat + "</br>" +
                                "Longitude: " + mapDat[nid].gps.lng //+ "</br>" +
                            //"Ref. POI: " + mapDat[minID].poi
                        );
                    }

                    var HERE_normalDay = L.tileLayer('http://{s}.{base}.maps.cit.api.here.com/maptile/2.1/maptile/{mapID}/normal.day/{z}/{x}/{y}/256/png8?app_id={app_id}&app_code={app_code}', {
                        attribution: 'Map &copy; 1987-2014',
                        subdomains: '1234',
                        mapID: 'newest',
                        app_id: 'Y8m9dK2brESDPGJPdrvs',
                        app_code: 'dq2MYIvjAotR8tHvY8Q_Dg',
                        base: 'base',
                        minZoom: 0,
                        maxZoom: 20
                    }).addTo(map);

                    var heatmapDataOfDay = generateHeatData(d3.range(24), mapDat, heatMapDat);
                    var heat = L.heatLayer(heatmapDataOfDay, options.heatmapOptions).addTo(map);

                    map.on('zoomend', function() {
                        var currentZoom = map.getZoom();
                        //console.log(currentZoom);
                        heat.setOptions({radius:(1.5*currentZoom),blur:((5/3)*currentZoom)});
                    });

                    var marker = undefined;
                    var isMarker = false;
                    var icon = "default";

                    var state = null;

                    var pinnedMarker = [];
                    map.pinnedNodes = [];

                    var d3Overlay = L.d3SvgOverlay(function(sel, proj) {
                        mapService.data.d3Overlay.sel.dest = sel;
                        mapService.data.d3Overlay.proj.dest = proj;
                    });
                    d3Overlay.addTo(map);

                    $("#dest_marker_control").bind("click", setIsMarker);
                    $("#dest_radial_graph_control").bind("click", setIsSelectRadialGraph);
                    $("#dest_reset").bind("click", reset);

                    map.on({
                        mousemove: function(e) {
                            if(!isMarker) {
                                return ;
                            }

                            if(marker) {
                                map.removeLayer(marker);
                                marker = undefined;
                            }

                            var min = 1e9;
                            var minID = -1;

                            for(var i in mapDat) {
                                if(state == "pin" && sourceMap.pinnedNodes.length == 0) {
                                    continue ;
                                }

                                if(state == "pin" && sourceMap.pinnedNodes.length > 0 && mapService.data.circles.dest && !mapService.data.circles.dest.hasOwnProperty(i)) {
                                    continue ;
                                }

                                var dis = funcService.getDistance([e.latlng.lat, e.latlng.lng], [mapDat[i].gps.lat, mapDat[i].gps.lng]);
                                if(dis < min) {
                                    min = dis;
                                    minID = i;
                                }
                            }

                            if(minID != -1) {
                                if (icon == "default") {
                                    marker = addBlueMarkerNid(minID);
                                } else {
                                    marker = addGreenMarkerNid(minID);
                                }

                                marker.on({
                                    click: function () {
                                        if (state == "radial_graph") {
                                            radialGraphService.loadData(minID);
                                        }
                                    },
                                    dblclick: function () {
                                        if (state != "pin") {
                                            return;
                                        }

                                        addPinnedNodes(minID, marker);
                                        marker = undefined;
                                    }
                                });
                            }
                        }
                    });

                    map.addPinnedNodes = addPinnedNodes;
                    function addPinnedNodes(nid, marker) {
                        map.pinnedNodes.push(+nid);
                        pinnedMarker.push(marker);
                        mapService.updateCircles();

                        matrixService.setSelectedDest(map.pinnedNodes);
                    }

                    function setIsMarker() {
                        if(state != "radial_graph") {
                            isMarker = !isMarker;
                        }

                        if(marker) {
                            map.removeLayer(marker);
                            marker = undefined;
                        }

                        icon = "default";
                        if(isMarker) {
                            state = "pin";
                        } else {
                            state = null;
                        }
                    }

                    function setIsSelectRadialGraph() {
                        if(state != "pin") {
                            isMarker = !isMarker;
                        }

                        if(marker) {
                            map.removeLayer(marker);
                            marker = undefined;
                        }

                        icon = green_marker_icon;
                        if(isMarker) {
                            state = "radial_graph";
                        } else {
                            state = null;
                        }
                    }

                    map.reset = reset;
                    function reset() {
                        for(var i in pinnedMarker) {
                            var _marker = pinnedMarker[i];
                            map.removeLayer(_marker);
                        }

                        pinnedMarker = [];
                        map.pinnedNodes = [];

                        matrixService.setSelectedDest(map.pinnedNodes);

                        mapService.updateCircles();
                    }

                    map.setHours = setHours;
                    function setHours(hours) {
                        if(heat != null) {
                            map.removeLayer(heat);
                            heat = null;
                        }

                        if(hours.length >= 24) {
                            heat = L.heatLayer(heatmapDataOfDay, options.heatmapOptions).addTo(map);

                            return ;
                        }

                        var heatmapData = generateHeatData(hours, mapDat, heatMapDat);
                        heat = L.heatLayer(heatmapData, options.heatmapOptions).addTo(map);
                    }

                    return map;
                }

                // generate data of heat map
                function generateHeatData(hours, mapDat, heatMapDat) {
                    var data = [];
                    var max = 0;
                    for(var i in heatMapDat) {
                        var sourceList = [];

                        var node = heatMapDat[i];

                        for (var j in node) {
                            var sliceTime = j;

                            if (!_.contains(hours, +sliceTime)) {
                                continue;
                            }

                            sourceList = _.union(sourceList, node[j]);
                        }

                        var value = sourceList.length;
                        max = Math.max(max, value);

                        var datum = [
                            mapDat[i].gps.lat,
                            mapDat[i].gps.lng,
                            value
                        ];
                        data.push(datum);
                    }


                    if(max > 0) {
                        for (var i in data) {
                            var datum = data[i];
                            datum[2] = datum[2] / max;
                        }
                    }

                    return data;
                }

                function constructMapDat(allMapDat, heatMapDat) {
                    var mapDat = _.mapObject(heatMapDat, function(value, key) {
                        return allMapDat[key];
                    });

                    return mapDat;
                }
            }

        }
    }
}]);