'use strict';

angular.module('core').controller('homeController', ['$scope', 'dataService',
	function($scope, dataService) {
        $scope.changeControlParameters = changeControlParameters;

        function changeControlParameters() {
            changeDate();
            changeHourRange();
        }

        function changeDate() {
            var wholeDateList = dataService.getWholeDateList();

            var dateList = [];
            var startDateIndex = $("#date-slider-range").slider("values", 0);
            var endDateIndex = $("#date-slider-range").slider("values", 1);

            for(var i = startDateIndex; i <= endDateIndex; ++i ) {
                dateList.push(wholeDateList[i]);
            }

            dataService.setDateList(dateList);
        }

        function changeHourRange() {
            var start = $("#hour-slider-range").slider("values", 0);
            var end = $("#hour-slider-range").slider("values", 1);

            var hours = d3.range(start, end+1, 1);
            dataService.setHours(hours);
        }
	}
]);